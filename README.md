# PetriNet Editor
## Advanced Software Engineering
### Authors

* Anil Acharya
* Eddie Davis
* Iker Vazquez

This project is a Java implementation of a [Petri net](*https://en.wikipedia.org/wiki/Petri_net*)
editor. The program can be launched by executing the launch script from the
command prompt at the project root directory:

*$ ./launch.sh*


The project is organized as follows:


1. **doc** contains the documentation.
2. **nets** contains the Petri net XML files.
3. **src** contains Java source code files.
4. **target** contains compiled Java class files.
5. *launch.sh* is a bash script to launch the program.
6. *pom.xml* is the *Maven* build file.
7. *wd.xml* is the default Petri net loaded at run time.
8. *Makefile*, running *make* rebuilds code and executes unit tests.
9. *README.md* is this README file.


The final project paper can be found at *doc/final/team-orange-final.pdf*

Thank you for reading...
