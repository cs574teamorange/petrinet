package junit;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.model.Firable;
import logic.model.LocationCoordinates;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.model.Place;
import logic.model.Tokens;
import logic.model.Transition;
import logic.utilities.PetriNetInferencer;
import logic.utilities.exception.InvalidPetriNetStateException;

import static org.junit.Assert.*;

/**
 * Created by anil on Sep 23, 2017
 * 
 */
public class TestPetriNetInferencer {

	private Set<PetriNetState> petriNetStates;

	@Before
	public void  initialiseData()
	{
		Place place1= new Place(1, new Tokens(1));
		Place place2= new Place(2, new Tokens(0));
		Place place3= new Place(3, new Tokens(1));
		Place place4= new Place(4, new Tokens(0));
		Set<Place> places= new TreeSet<>();
		places.add(place1);
		places.add(place2);
		places.add(place3);
		places.add(place4);
		PetriNetState petriNetState= new PetriNetState(1, places, MarkingState.NEW, null,null);
		petriNetStates=new TreeSet<>();
		PetriNetState petriNetState2=new PetriNetState(2, new int[]{1,1000,1,0}, MarkingState.NEW,petriNetState,null);
		petriNetStates.add(petriNetState);
		petriNetStates.add(petriNetState2);
	}
	@Test
	public void testIfPetriNetStatesIsEmpty()
	{
		Assert.assertFalse(petriNetStates.isEmpty());

	}
	@Test
	public void testBoundednessWorkability() throws InvalidPetriNetStateException
	{
		PetriNetInferencer petriNetInferencer= new PetriNetInferencer(petriNetStates,2);
		Assert.assertFalse(petriNetInferencer.isPetriNetBounded());

	}
	@Test
	public void testSafenessWorkability() throws InvalidPetriNetStateException
	{
		PetriNetInferencer petriNetInferencer= new PetriNetInferencer(petriNetStates,2);
		Assert.assertFalse(petriNetInferencer.isPetriNetSafe());
	}
	

	@Test
	public void testExceptionIsThrownForBounded() 
	{
		petriNetStates.clear();
		boolean thrown=false;
		PetriNetState petriNetState2=new PetriNetState(56, new int[]{1,100,-1,0}, MarkingState.NEW,null,null);
		petriNetStates.add(petriNetState2);
		PetriNetInferencer petriNetInferencer= new PetriNetInferencer(petriNetStates,2);
		try {
			petriNetInferencer.isPetriNetBounded();
		} catch (InvalidPetriNetStateException e) {
			thrown=true;
		}

		assertTrue(thrown);
	}
	
	@Test
	public void testExceptionIsThrownForSafe() 
	{
		boolean thrown=false;
		petriNetStates.clear();
		PetriNetState petriNetState2=new PetriNetState(56, new int[]{1,10,-1,0}, MarkingState.NEW,null,null);
		petriNetStates.add(petriNetState2);
		PetriNetInferencer petriNetInferencer= new PetriNetInferencer(petriNetStates,2);
		try {
			petriNetInferencer.isPetriNetSafe();
		} catch (InvalidPetriNetStateException e) {
			thrown=true;	
		}
		assertTrue(thrown);
	}
	
	@Test
	public void testForSafePetriNet() throws InvalidPetriNetStateException
	{
		petriNetStates.clear();
		PetriNetState petriNetState2=new PetriNetState(56, new int[]{1,1,1,0}, MarkingState.NEW,null,null);
		petriNetStates.add(petriNetState2);
		PetriNetInferencer petriNetInferencer= new PetriNetInferencer(petriNetStates,2);
			petriNetInferencer.isPetriNetSafe();		
	}

	@Test
	public void testPetriNetStateCopy()
	{
		PetriNetState petriNetState=new PetriNetState(0, new int[]{1,1,1,0}, MarkingState.NEW,null,null);
		Transition transition= (Transition)ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		transition.set_locationCoordinates(new LocationCoordinates(34,34));
		PetriNetState petriNetState2=new PetriNetState(1, new int[]{1,5,1,0}, MarkingState.NEW,petriNetState,transition);
		petriNetState2.set_locationCoordinates(new LocationCoordinates(45, 12));
//		PetriNetState newCopiedPetriNet= new PetriNetState(petriNetState2);
//		assertTrue(petriNetState2.equalsLogical(newCopiedPetriNet.getTokenCount()));
		
	}

}
