package junit;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;

import logic.model.DestinationArc;
import logic.model.Firable;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Tokens;
import logic.model.Transition;
import logic.utilities.CoverabilityTree;
import logic.utilities.DefaultArcManager;
import logic.utilities.DefaultMarkingStateManager;
import logic.utilities.DefaultTransitionFiringManager;
import logic.utilities.MarkingStateManager;
import logic.utilities.PetriNetInferencer;
import logic.utilities.TransitionFiringManager;
import logic.utilities.exception.InvalidPetriNetStateException;

/**
 * Created by anil on Sep 23, 2017
 *
 */
public class TestCoverabilityTree1 extends DemoPetriNet {
	@Test
	public void testNonNullCoverabilityTreeWithDisplay()
	{
		Assert.assertNotNull(petriNetStates);
	}
	@Test
	public void testIsPetriNetBounded() throws InvalidPetriNetStateException
	{
		PetriNetInferencer petriNetInferencer=new PetriNetInferencer(petriNetStates,getPetriNet().get_transitions().size());
		Assert.assertTrue(petriNetInferencer.isPetriNetBounded());
	}
	@Test
	public void testIfPetriNetSafe() throws InvalidPetriNetStateException
	{
		PetriNetInferencer petriNetInferencer= new PetriNetInferencer(petriNetStates,getPetriNet().get_transitions().size());
		Assert.assertFalse(petriNetInferencer.isPetriNetSafe());
	}

	@Override
	public PetriNet getPetriNet() {
		Place place1=new Place(1, new Tokens(3));
		Place place2= new Place(2, new Tokens(1));
		Place place3= new Place(3, new Tokens(2));
		Set<Place> places= new TreeSet<>();
		places.add(place1);
		places.add(place2);
		places.add(place3);
		SourceArc sourceArc1_1=new SourceArc(1, place1, 1);
		DestinationArc destinationArc1_1=new DestinationArc(2, place2,1);
		DestinationArc destinationArc1_2=new DestinationArc(3, place3,1);
		Set<SourceArc> sourcesArcs= new TreeSet<>();
		sourcesArcs.add(sourceArc1_1);
		Set<DestinationArc> destintionArcs=new TreeSet<>();
		destintionArcs.add(destinationArc1_1);
		destintionArcs.add(destinationArc1_2);
		Transition transition0= new Transition(0, sourcesArcs,destintionArcs,"T1");
		SourceArc sourceArc2_1= new SourceArc(45, place1, 1);
		SourceArc sourceArc2_2=new SourceArc(46, place3, 1);
		DestinationArc destinationArc2_1= new DestinationArc(47, place2, 1);
		Set<SourceArc> sourceArcs1_1=new TreeSet<>();
		sourceArcs1_1.add(sourceArc2_1);
		sourceArcs1_1.add(sourceArc2_2);
		Set<DestinationArc> destinationArcs12=new TreeSet<>();
		destinationArcs12.add(destinationArc2_1);
		Transition transition1= new Transition(1, sourceArcs1_1,destinationArcs12,"T2");
		Set<Firable> transitions= new TreeSet<>();
		transitions.add(transition0);
		transitions.add(transition1);
		return new PetriNet(1,places,transitions);
	}
}
