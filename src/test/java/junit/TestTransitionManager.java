package junit;

import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.PetriNetManager;
import logic.model.SourceArc;
import logic.model.Transition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
/**
 * Created by anil on Oct 13, 2017
 *
 */
public class TestTransitionManager {

	@Before
	public void clearTraces()
	{


	}

	@Test
	public void testIfComponentIsInstantinated()
	{
		Transition transition= (Transition)ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		assertNotNull(transition);	
	}

	@Test
	public void testIfNonItemHasDefaultValueAtInitialization()
	{
		Transition transition= (Transition)ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		assertTrue(transition.getSourceArcs().isEmpty() ||transition.getDestinationArcs().isEmpty());	
	}
	@Test
	public void testComponentAdditionBehaviour()
	{
		Transition transition= (Transition)ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		SourceArc sourceArc= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.addComponent(sourceArc, transition.get_id());
		assertTrue(!transition.getSourceArcs().isEmpty());		
	}
	@Test
	public void testCopyBehaviour()
	{
		Transition transition= (Transition)ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		SourceArc sourceArc= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.addComponent(sourceArc, transition.get_id());
		Transition copied= (Transition) ComponentManagerFactory.copyComponent(transition);
		assertFalse(copied.getSourceArcs().contains(sourceArc));

	}
	@Test
	public void testIfComponentAttachMentSucceed()
	{
		Transition transition= (Transition)ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 1);
		assertTrue(PetriNetManager.initialize().getComponent().get_transitions().contains(transition));
	}
}
