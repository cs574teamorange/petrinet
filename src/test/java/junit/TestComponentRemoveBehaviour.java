package junit;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.PetriNetManager;
import logic.model.ArcValue;
import logic.model.DestinationArc;
import logic.model.Firable;
import logic.model.LocationCoordinates;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Tokens;
import logic.model.Transition;

/**
 * Created by anil on Oct 13, 2017
 *
 */
public class TestComponentRemoveBehaviour {
	

	PetriNetManager petriNetManger=PetriNetManager.initialize();
	
	@Before
	public void clearAll()
	{
	petriNetManger.clear();
	}
	
	@Test
	public void testPlaceRemoval()
	{
		Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(45, 45),new Tokens(5),null); //update the component //null is because place has nothing to attach any component
		ComponentManagerFactory.addComponent(place1, null); //add the component to the petriNet //null because place has no parent
		Place place2= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(place2,new LocationCoordinates(45, 45),new Tokens(5),null);
		ComponentManagerFactory.addComponent(place2, null);
		Transition transition1= (Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.updateComponentMetaData(transition1, new LocationCoordinates(45, 45), null, null);
		ComponentManagerFactory.addComponent(transition1, null);
		SourceArc sourceArc= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(4),place1); // arc is attach to place 
		ComponentManagerFactory.addComponent(sourceArc, transition1.get_id());// transition is parent to arc
		SourceArc sourceArc12= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc12, new LocationCoordinates(45, 45), new ArcValue(6),place1);
		ComponentManagerFactory.addComponent(sourceArc12, transition1.get_id());
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(5),place2);
		ComponentManagerFactory.addComponent(destinationArc, transition1.get_id());
		DestinationArc destinationArc12= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc12, new LocationCoordinates(45, 45), new ArcValue(7),place1);
		ComponentManagerFactory.addComponent(destinationArc12, transition1.get_id());
		ComponentManagerFactory.removeComponent(place1, null);
		assertFalse(petriNetManger.getComponent().get_places().contains(place1));
	}
	
	@Test
	public void testTransitionRemoval()
	{
		Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(45, 45),new Tokens(5),null); //update the component //null is because place has nothing to attach any component
		ComponentManagerFactory.addComponent(place1, null); //add the component to the petriNet //null because place has no parent
		Place place2= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(place2,new LocationCoordinates(45, 45),new Tokens(5),null);
		ComponentManagerFactory.addComponent(place2, null);
		Transition transition1= (Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.updateComponentMetaData(transition1, new LocationCoordinates(45, 45), null, null);
		ComponentManagerFactory.addComponent(transition1, null);
		SourceArc sourceArc= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(4),place1); // arc is attach to place 
		ComponentManagerFactory.addComponent(sourceArc, transition1.get_id());// transition is parent to arc
		SourceArc sourceArc12= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc12, new LocationCoordinates(45, 45), new ArcValue(6),place1);
		ComponentManagerFactory.addComponent(sourceArc12, transition1.get_id());
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(5),place2);
		ComponentManagerFactory.addComponent(destinationArc, transition1.get_id());
		DestinationArc destinationArc12= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc12, new LocationCoordinates(45, 45), new ArcValue(7),place1);
		ComponentManagerFactory.addComponent(destinationArc12, transition1.get_id());
		ComponentManagerFactory.removeComponent(transition1, null);
		assertFalse(petriNetManger.getComponent().get_transitions().contains(transition1));	
	}
	
	@Test
	public void testSourceArcRemoval()
	{
		Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(45, 45),new Tokens(5),null); //update the component //null is because place has nothing to attach any component
		ComponentManagerFactory.addComponent(place1, null); //add the component to the petriNet //null because place has no parent
		Place place2= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(place2,new LocationCoordinates(45, 45),new Tokens(5),null);
		ComponentManagerFactory.addComponent(place2, null);
		Transition transition1= (Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.updateComponentMetaData(transition1, new LocationCoordinates(45, 45), null, null);
		ComponentManagerFactory.addComponent(transition1, null);
		SourceArc sourceArc= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(4),place1); // arc is attach to place 
		ComponentManagerFactory.addComponent(sourceArc, transition1.get_id());// transition is parent to arc
		SourceArc sourceArc12= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc12, new LocationCoordinates(45, 45), new ArcValue(6),place1);
		ComponentManagerFactory.addComponent(sourceArc12, transition1.get_id());
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(5),place2);
		ComponentManagerFactory.addComponent(destinationArc, transition1.get_id());
		DestinationArc destinationArc12= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc12, new LocationCoordinates(45, 45), new ArcValue(7),place1);
		ComponentManagerFactory.addComponent(destinationArc12, transition1.get_id());
		ComponentManagerFactory.removeComponent(sourceArc12, transition1.get_id());
		
		Transition matching=null;
		List<Firable> transitions=new ArrayList<Firable>(petriNetManger.getComponent().get_transitions());
		for(Firable transiton: transitions)
		{
			Transition tr=(Transition) transiton;
			if(transiton.equals(tr))
				matching=tr;
		}
		assertFalse(matching.getSourceArcs().contains(sourceArc12));	
	}
	@Test
	public void testDestinationRemoval()
	{
		Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(45, 45),new Tokens(5),null); //update the component //null is because place has nothing to attach any component
		ComponentManagerFactory.addComponent(place1, null); //add the component to the petriNet //null because place has no parent
		Place place2= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(place2,new LocationCoordinates(45, 45),new Tokens(5),null);
		ComponentManagerFactory.addComponent(place2, null);
		Transition transition1= (Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.updateComponentMetaData(transition1, new LocationCoordinates(45, 45), null, null);
		ComponentManagerFactory.addComponent(transition1, null);
		SourceArc sourceArc= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(4),place1); // arc is attach to place 
		ComponentManagerFactory.addComponent(sourceArc, transition1.get_id());// transition is parent to arc
		SourceArc sourceArc12= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc12, new LocationCoordinates(45, 45), new ArcValue(6),place1);
		ComponentManagerFactory.addComponent(sourceArc12, transition1.get_id());
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(5),place2);
		ComponentManagerFactory.addComponent(destinationArc, transition1.get_id());
		DestinationArc destinationArc12= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc12, new LocationCoordinates(45, 45), new ArcValue(7),place1);
		ComponentManagerFactory.addComponent(destinationArc12, transition1.get_id());
		ComponentManagerFactory.removeComponent(destinationArc12, transition1.get_id());
		
		Transition matching=null;
		List<Firable> transitions=new ArrayList<Firable>(petriNetManger.getComponent().get_transitions());
		for(Firable transiton: transitions)
		{
			Transition tr=(Transition) transiton;
			if(transiton.equals(tr))
				matching=tr;
		}
		assertFalse(matching.getDestinationArcs().contains(destinationArc12));	
		
	}
}
