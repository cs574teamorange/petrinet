package junit;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import logic.model.ArcValue;
import logic.model.LocationCoordinates;

/**
 * Created by anil on Oct 14, 2017
 *
 */
public class TestGetterSetter {
	
	@Test
	public void testGetterXOfLocationCoordinates()
	{
		LocationCoordinates locationCoordinates= new LocationCoordinates(12, 12);
		locationCoordinates.setxPoint(25);
		assertEquals(25, locationCoordinates.getxPoint());
		
	}
	@Test
	public void testGetterYOfLocationCoordinates()
	{
		LocationCoordinates locationCoordinates= new LocationCoordinates(12, 12);
		locationCoordinates.setyPoint(25);
		assertEquals(25, locationCoordinates.getyPoint());
		
	}
	@Test
	public void testSetterOfAbstractMetaData()
	{
		ArcValue arc= new ArcValue(34);
		arc.setIntCount(34);
		assertEquals(34, arc.getIntCount());	
	}
}
