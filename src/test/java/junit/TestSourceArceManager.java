package junit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.DestinationArcManager;
import logic.componentManager.PetriNetManager;
import logic.componentManager.SourceArcManager;
import logic.componentManager.TransitionManager;
import logic.model.ArcValue;
import logic.model.DestinationArc;
import logic.model.LocationCoordinates;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Transition;

/**
 * Created by anil on Oct 13, 2017
 *
 */
public class TestSourceArceManager {
	
	@Test
	public void clear()
	{
		PetriNetManager.initialize().getComponent().get_transitions().clear();
	}	
	@Test
	public void isSourceArcCreated()
	{
		assertNotNull(ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW));
	}
	@Test
	public void isComponentCreatedSourceArc()
	{
		assertTrue(ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW).getClass()==SourceArc.class);
	}
	@Test
	public void isComponentAddedToActualTransition()
	{
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition  transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);;
		ComponentManagerFactory.addComponent(transition, 0);
		SourceArc sourceArc= (SourceArc)ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(sourceArc, transition.get_id());
		Transition transition12=TransitionManager.getCurrentInstance().findComponent(transition.get_id());
		assertTrue(transition12.getSourceArcs().contains(sourceArc));	
	}
	@Test
	public void isComponentNotAddedToAnotherTransition()
	{
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition  transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		Transition  transition34=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition34, 1);
		ComponentManagerFactory.addComponent(transition, 0);
		SourceArc sourceArc= (SourceArc)ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(sourceArc, transition.get_id());
		Transition transition12=TransitionManager.getCurrentInstance().findComponent(transition34.get_id());
		assertFalse(transition12.getSourceArcs().contains(sourceArc));	
	}	
	@Test
	public void isComponentEditSuccess()
	{
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition  transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		SourceArc sourceArc= (SourceArc)ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(sourceArc, transition.get_id());
		Place p2=(Place)ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(45), p2);
		SourceArc sourceArc12=SourceArcManager.getCurrentInstance().findComponent(sourceArc.get_id());
		assertEquals(p2, sourceArc12.get_place());
	}
	
	@Test
	public void testCopyComponentSuccessBehaviour()
	{
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition  transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		SourceArc sourceArc= (SourceArc)ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(sourceArc, transition.get_id());
		SourceArc copied= (SourceArc) ComponentManagerFactory.copyComponent(sourceArc);
		assertNull(null,copied.get_place());	
	}
	@Test
	public void testnullComponentReturnForNotAddedComponent()
	{
		Transition  transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		assertNull(TransitionManager.getCurrentInstance().findComponent(490));
	}
	@Test
	public void testIfSourceArcIsDuplicateOrNot()
	{
		SourceArcManager.getCurrentInstance().clear();
		SourceArc sourceArc=  (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(sourceArc, transition.get_id());
		assertTrue(SourceArcManager.getCurrentInstance().isComponentDuplicate(place, transition));
	}
	
	@Test
	public void testIfSourceArcIsDuplicateOrNot2()
	{
		SourceArcManager.getCurrentInstance().clear();
		SourceArc sourceArc=  (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(sourceArc, transition.get_id());
		assertFalse(SourceArcManager.getCurrentInstance().isComponentDuplicate(null, null));
	}
	@Test
	public void testIfSourceArcIsDuplicateOrNot3()
	{
		SourceArcManager.getCurrentInstance().clear();
		SourceArc sourceArc=  (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(sourceArc, transition.get_id());
		assertFalse(SourceArcManager.getCurrentInstance().isComponentDuplicate((Place)ComponentManagerFactory.getComponent(ComponentType.PLACE),(Transition)ComponentManagerFactory.getComponent(ComponentType.TRANSITION)));
	}
	
	@Test
	public void testIfDestinationIsDuplicateOrNot()
	{
		DestinationArcManager.getCurrentInstance().clear();
		DestinationArc destinationArc=  (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(destinationArc, transition.get_id());
		
		assertTrue(DestinationArcManager.getCurrentInstance().isComponentDuplicate(place,transition));
	}
	
	@Test
	public void testIfDestinationIsDuplicateOrNot2()
	{
		DestinationArcManager.getCurrentInstance().clear();
		DestinationArc destinationArc=  (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(destinationArc, transition.get_id());
		assertFalse(DestinationArcManager.getCurrentInstance().isComponentDuplicate(null,null));
	}
	@Test
	public void testIfDestinationIsDuplicateOrNot267()
	{
		DestinationArcManager.getCurrentInstance().clear();
		DestinationArc destinationArc=  (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(destinationArc, transition.get_id());
		assertFalse(DestinationArcManager.getCurrentInstance().isComponentDuplicate((Place) ComponentManagerFactory.getComponent(ComponentType.PLACE),null));
	}
	@Test
	public void testIfDestinationIsDuplicateOrNot23()
	{
		DestinationArcManager.getCurrentInstance().clear();
		DestinationArc destinationArc=  (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(destinationArc, transition.get_id());
		assertFalse(DestinationArcManager.getCurrentInstance().isComponentDuplicate(null,(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION)));
	}
	
	@Test
	public void testIfDestinationIsDuplicateOrNot34()
	{
		DestinationArcManager.getCurrentInstance().clear();
		DestinationArc destinationArc=  (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(destinationArc, transition.get_id());
		assertFalse(DestinationArcManager.getCurrentInstance().isComponentDuplicate((Place) ComponentManagerFactory.getComponent(ComponentType.PLACE),null));
	}
	
	@Test
	public void testIfDestinationIsDuplicateOrNot345()
	{
		DestinationArcManager.getCurrentInstance().clear();
		DestinationArc destinationArc=  (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(destinationArc, transition.get_id());
		assertFalse(DestinationArcManager.getCurrentInstance().isComponentDuplicate(null,(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION)));
	}
	
	@Test
	public void testIfDestinationIsDuplicateOrNot3()
	{
		DestinationArcManager.getCurrentInstance().clear();
		DestinationArc destinationArc=  (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		Place place=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(45), place);
		ComponentManagerFactory.addComponent(destinationArc, transition.get_id());
		assertFalse(DestinationArcManager.getCurrentInstance().isComponentDuplicate((Place)ComponentManagerFactory.getComponent(ComponentType.PLACE),(Transition)ComponentManagerFactory.getComponent(ComponentType.TRANSITION)));

	}
	
}
