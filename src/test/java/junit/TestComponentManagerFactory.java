package junit;

import org.junit.Test;

import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.PetriNetIO;
import logic.componentManager.PlaceManager;
import logic.componentManager.TransitionManager;
import logic.model.Place;
import logic.model.Transition;

import static org.junit.Assert.*;

public class TestComponentManagerFactory {
	
	
	@Test
	public void testInstanceCreation()
	{
		assertNotNull(new ComponentManagerFactory());
	}
	@Test
	public void testInstanceCreationPetriNetIO()
	{
		assertNotNull(new PetriNetIO());
		
	}
	
	@Test
	public void testIfNameUniqueIsInsured()
	{
		PlaceManager placemanger=PlaceManager.getCurrentInstance();
		Place place=placemanger.getComponent();
		assertTrue(placemanger.isNameUsed(place.getPlaceName()));
		assertFalse(placemanger.isNameUsed(place.getPlaceName()+"anil acharya"));
	Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
	assertTrue(transition.isTransitionDead());
	assertTrue(transition.getTransitionName()==transition.getCompoentName());
	}

}
