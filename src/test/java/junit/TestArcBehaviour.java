package junit;

import logic.model.DestinationArc;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Tokens;
import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
/**
 * Created by anil on Sep 23, 2017
 */
public class TestArcBehaviour {

	private SourceArc sourceArc1;
	private SourceArc sourceArc2;
	private DestinationArc destinationArc1;
	private DestinationArc destinationArc2;
	@Before
	public void initialiseArcsValue()
	{
		Place place= new Place(0, new Tokens(10));
		sourceArc1=new SourceArc(1, place, 5);
		Place place1=new Place(2, new Tokens(15));
		sourceArc2=new SourceArc(3, place1, 20);
		Place place4=new Place(4, new Tokens(10));
		destinationArc1= new DestinationArc(5, place4, 5);
		Place place5=new Place(6, new Tokens(0));
		destinationArc2= new DestinationArc(7, place5, 5);
	}
	@Test
	public void testSourceArc1FirableBehaviour()
	{
		Assert.assertTrue(sourceArc1.isArcActive());
	}
	@Test
	public void testSourceArc2FirableBehaviour()
	{
		Assert.assertFalse(sourceArc2.isArcActive());
	}
	@Test
	public void testSourceArc1FiredBehaviour()
	{
		int arcCount=sourceArc1.get_arcValue();
		int placetoken= sourceArc1.get_place().get_tokens().getIntCount();
		sourceArc1.activateArc();
		Assert.assertEquals(sourceArc1.get_place().get_tokens().getIntCount(),placetoken-arcCount);
	}
	@Test
	public void testDestinationArcFiredBehaviour()
	{
		int arcCount=destinationArc1.get_arcValue();
		int placetoken= destinationArc1.get_place().get_tokens().getIntCount();
		destinationArc1.activateArc();
		Assert.assertEquals(destinationArc1.get_place().get_tokens().getIntCount(),placetoken+arcCount);

	}
	@Test
	public void testDestinationArc2FiredBehaviour()
	{
		int arcCount=destinationArc2.get_arcValue();
		int placetoken= destinationArc2.get_place().get_tokens().getIntCount();
		destinationArc2.activateArc();
		Assert.assertEquals(destinationArc2.get_place().get_tokens().getIntCount(),placetoken+arcCount);
	}




}
