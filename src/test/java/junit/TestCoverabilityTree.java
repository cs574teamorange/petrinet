package junit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import io.PetriSerializerException;
import logic.componentManager.PetriNetIO;
import logic.componentManager.PetriNetManager;
import logic.model.DestinationArc;
import logic.model.Firable;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Tokens;
import logic.model.Transition;
import logic.utilities.CoverabilityTree;
import logic.utilities.DefaultArcManager;
import logic.utilities.DefaultMarkingStateManager;
import logic.utilities.DefaultTransitionFiringManager;
import logic.utilities.MarkingStateManager;
import logic.utilities.PetriNetInferencer;
import logic.utilities.TransitionFiringManager;
import logic.utilities.exception.InvalidPetriNetStateException;

/**
 * Created by anil on Sep 23, 2017
 *
 */
public class TestCoverabilityTree extends DemoPetriNet {

	
	@Before
	public void init()
	{
		PetriNetManager.initialize().clear();
		
	}
	@Test
	public void testIfTheGivenValidPetriNetHasEmptyStates()
	{
		Assert.assertNotNull(petriNetStates);
	}

	@Test
	public void testIfTheGivenPetriNetIsBounded() throws InvalidPetriNetStateException
	{
		PetriNetInferencer petriNetInferencer=new PetriNetInferencer(petriNetStates,getPetriNet().get_transitions().size());
		Assert.assertFalse(petriNetInferencer.isPetriNetBounded());
	}
	@Test
	public void testIfGivenNetIsLive() throws InvalidPetriNetStateException
	{
		PetriNetInferencer petriNetInferencer=new PetriNetInferencer(petriNetStates,getPetriNet().get_transitions().size());
		Assert.assertFalse(petriNetInferencer.isPetriNetSafe());
	}
	@Override
	public PetriNet getPetriNet() {
		Place place1=new Place(1, new Tokens(1));
		Place place2= new Place(2, new Tokens(0));
		Place place3= new Place(3, new Tokens(0));
		Set<Place> places= new TreeSet<>();
		places.add(place1);
		places.add(place2);
		places.add(place3);
		SourceArc sourceArc1=new SourceArc(1, place1, 1);
		SourceArc sourceArc2=new SourceArc(2, place3, 1);
		Set<SourceArc> sourcesArcs= new TreeSet<>();
		sourcesArcs.add(sourceArc1);
		sourcesArcs.add(sourceArc2);
		Transition transition0= new Transition(0, sourcesArcs,new TreeSet<DestinationArc>(),"T1");
		SourceArc sourceArc3=new SourceArc(3, place1, 1);
		DestinationArc destinationArc=new DestinationArc(4, place3, 1);
		sourcesArcs= new TreeSet<>();
		sourcesArcs.add(sourceArc3);
		Set<DestinationArc> destinationArcs=new TreeSet<DestinationArc>();
		destinationArcs.add(destinationArc);
		Transition transition1= new Transition(1, sourcesArcs,destinationArcs,"T2");
		SourceArc sourceArc4=new SourceArc(5, place1, 1);
		DestinationArc destinationArc1=new DestinationArc(6, place1, 1);
		DestinationArc destinationArc2=new DestinationArc(7, place2, 1);
		sourcesArcs= new TreeSet<>();
		sourcesArcs.add(sourceArc4);
		destinationArcs=new TreeSet<>();
		destinationArcs.add(destinationArc1);
		destinationArcs.add(destinationArc2);
		Transition transition2= new Transition(2, sourcesArcs,destinationArcs,"T3");

		SourceArc sourceArc5=new SourceArc(8, place2, 1);
		SourceArc sourceArc6=new SourceArc(9, place3, 1);
		DestinationArc destinationArc45=new DestinationArc(10, place3, 1);
		sourcesArcs= new TreeSet<>();
		sourcesArcs.add(sourceArc5);
		sourcesArcs.add(sourceArc6);
		destinationArcs=new TreeSet<DestinationArc>();
		destinationArcs.add(destinationArc45);
		Transition transition3= new Transition(3, sourcesArcs,destinationArcs,"T4");
		Set<Firable> transitions= new TreeSet<>();
		transitions.add(transition1);
		transitions.add(transition2);
		transitions.add(transition3);
		transitions.add(transition0);
		return  new PetriNet(1,places,transitions);
	}
	

	@Test
	public void testIfPetriNetIsL1Live() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet=PetriNetIO.readPetriNet("./src/test/resources/HW1PN1.xml");
		TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
		int[] currentMarkingState=markingStateManager.getcurrentStateofPetriNet(petriNet.get_places());
		PetriNetState initialState= new PetriNetState(1,currentMarkingState,MarkingState.NEW,null,null);
		petriNet.set_petriNetState(initialState);		
		Set<PetriNetState> petriNetStates=coverabilityTree.generateConverabilityTree(petriNet);
		PetriNetInferencer petriNetInferencer=new PetriNetInferencer(petriNetStates,petriNet.get_transitions().size());
		assertTrue(petriNetInferencer.isPetriNetL1Live());
		
	}

	@Test
	public void testIfPetriNetIsL0Live() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet=PetriNetIO.readPetriNet("./src/test/resources/HW1PN1.xml");
		TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
		int[] currentMarkingState=markingStateManager.getcurrentStateofPetriNet(petriNet.get_places());
		PetriNetState initialState= new PetriNetState(1,currentMarkingState,MarkingState.NEW,null,null);
		petriNet.set_petriNetState(initialState);		
		Set<PetriNetState> petriNetStates=coverabilityTree.generateConverabilityTree(petriNet);
		PetriNetInferencer petriNetInferencer=new PetriNetInferencer(petriNetStates,petriNet.get_transitions().size());
		assertFalse(petriNetInferencer.isPetriNetL0Live());
		
	}
	@Test
	public void testIfMarkingStateIsReachableOrNot() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet=PetriNetIO.readPetriNet("./src/test/resources/HW1PN1.xml");
		TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
		int[] currentMarkingState=markingStateManager.getcurrentStateofPetriNet(petriNet.get_places());
		PetriNetState initialState= new PetriNetState(1,currentMarkingState,MarkingState.NEW,null,null);
		petriNet.set_petriNetState(initialState);		
		Set<PetriNetState> petriNetStates=coverabilityTree.generateConverabilityTree(petriNet);
		PetriNetInferencer petriNetInferencer=new PetriNetInferencer(petriNetStates,petriNet.get_transitions().size());
		assertTrue(petriNetInferencer.isMarkingStateReachable(new int[] {0, 0, 0, 1, 0, 0, 1, 0}));
	}
	  
	@Test
	public void testIfMarkingStateIsNotReachableOrNot() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet=PetriNetIO.readPetriNet("./src/test/resources/HW1PN1.xml");
		TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
		int[] currentMarkingState=markingStateManager.getcurrentStateofPetriNet(petriNet.get_places());
		PetriNetState initialState= new PetriNetState(1,currentMarkingState,MarkingState.NEW,null,null);
		petriNet.set_petriNetState(initialState);		
		Set<PetriNetState> petriNetStates=coverabilityTree.generateConverabilityTree(petriNet);
		PetriNetInferencer petriNetInferencer=new PetriNetInferencer(petriNetStates,petriNet.get_transitions().size());
		assertFalse(petriNetInferencer.isMarkingStateReachable(new int[] {3,0,4,5,0,0,0,0}));
	}
	
	
}
