package junit;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.xml.sax.SAXException;

import io.PetriNetSerializer;
import io.PetriSerializerException;
import junit.framework.AssertionFailedError;
import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.PetriNetIO;
import logic.componentManager.PetriNetManager;
import logic.model.AbstractComponent;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.model.Shapes;
import logic.utilities.DefaultMarkingStateManager;
import logic.utilities.MarkingStateManager;
/**
 * Created by anil on Oct 13, 2017
 *
 */
public class TestComponentTypeAndShape {
	@Test
	public void testShapeOfPlace()
	{
		AbstractComponent abstractComponent= ComponentManagerFactory.getComponent(ComponentType.PLACE);
		assertEquals(Shapes.CIRCLE, abstractComponent.getShape());
		
	}
	@Test
	public void testTypeOfPlace()
	{
		AbstractComponent abstractComponent= ComponentManagerFactory.getComponent(ComponentType.PLACE);
		assertEquals(ComponentType.PLACE, abstractComponent.getComponentType());
	}
	
	@Test
	public void testShapeOfTransition()
	{
		AbstractComponent abstractComponent= ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		assertEquals(Shapes.RECTANGLE, abstractComponent.getShape());
		
	}
	@Test
	public void testTypeOfTransition()
	{
		AbstractComponent abstractComponent= ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		assertEquals(ComponentType.TRANSITION, abstractComponent.getComponentType());
	}
	
	@Test
	public void testShapeOfSourceArc()
	{
		AbstractComponent abstractComponent= ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		assertEquals(Shapes.SOURCE_ARROW, abstractComponent.getShape());
		
	}
	@Test
	public void testTypeOfSourceArc()
	{
		AbstractComponent abstractComponent= ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		assertEquals(ComponentType.SOURCE_ARROW, abstractComponent.getComponentType());
	}
	
	@Test
	public void testShapeOfDestinationArc()
	{
		AbstractComponent abstractComponent= ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		assertEquals(Shapes.DESTINATION_ARROW, abstractComponent.getShape());
		
	}
	@Test
	public void testTypeOfDestinationArc()
	{
		AbstractComponent abstractComponent= ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		assertEquals(ComponentType.DESTINATION_ARROW, abstractComponent.getComponentType());
	}
	
	@Test
	public void testTypeOfPetriNet()
	{
		assertEquals(ComponentType.PETRINET, PetriNetManager.initialize().getComponent().getComponentType());
	}
	
	@Test
	public void testShapeOfPetriNet()
	{
		assertEquals(Shapes.DEFAULT, PetriNetManager.initialize().getComponent().getShape());
	}
	
	
	
	@Test
	public void testIfTwoSameCompoentWithDifferntIdReturnDifferent()
	{
		AbstractComponent abstractComponent= ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		AbstractComponent abstractComponent12= ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		assertTrue(abstractComponent.equals(abstractComponent));
	}
	
	
	
	@Test
	public void testComponentTypeofPetriNetState() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet= PetriNetIO.readPetriNet("./src/test/resources/VendingMachine.xml");
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		PetriNetState initialState= new PetriNetState(1,markingStateManager.getcurrentStateofPetriNet(petriNet.get_places()),MarkingState.NEW,null,null);
		assertEquals(ComponentType.COVERABILITY_GRAPH, initialState.getComponentType());
	}
	
	@Test
	public void testShapeofPetriNetState() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet= PetriNetIO.readPetriNet("./src/test/resources/VendingMachine.xml");
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		PetriNetState initialState= new PetriNetState(1,markingStateManager.getcurrentStateofPetriNet(petriNet.get_places()),MarkingState.NEW,null,null);
		assertEquals(Shapes.DIRECTED_GRAPH, initialState.getShape());
	}
	
	@Test
	public void TestWhichTrasitionResultsTheState() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet= PetriNetIO.readPetriNet("./src/test/resources/VendingMachine.xml");
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		PetriNetState initialState= new PetriNetState(1,markingStateManager.getcurrentStateofPetriNet(petriNet.get_places()),MarkingState.NEW,null,null);
		assertEquals(null, initialState.get_transitionFired());
	}
	
	@Test
	public void testHashCodeEquals() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException{
		
		PetriNet petriNet= PetriNetIO.readPetriNet("./src/test/resources/VendingMachine.xml");
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		PetriNetState initialState= new PetriNetState(1,markingStateManager.getcurrentStateofPetriNet(petriNet.get_places()),MarkingState.NEW,null,null);
		PetriNetState initialState23= new PetriNetState(1,markingStateManager.getcurrentStateofPetriNet(petriNet.get_places()),MarkingState.NEW,null,null);
		assertEquals(initialState.hashCode(),initialState23.hashCode());
	}
}
