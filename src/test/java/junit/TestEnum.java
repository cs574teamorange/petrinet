package junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import logic.componentManager.ComponentType;
import logic.model.MarkingState;
import logic.model.Shapes;
public class TestEnum {
	
	@Test
	public void testShapeEnum()
	{
		List<Shapes> shapes= new ArrayList<>();
		shapes.add(Shapes.CIRCLE);
		shapes.add(Shapes.SOURCE_ARROW);
		shapes.add(Shapes.DESTINATION_ARROW);

		shapes.add(Shapes.RECTANGLE);
		shapes.add(Shapes.DEFAULT);
		shapes.add(Shapes.DIRECTED_GRAPH);
		assertArrayEquals(Shapes.values(),shapes.toArray() );	
	}
	@Test
	public void testComponentType()
	{
		List<ComponentType> componentTypes=new ArrayList<>();		
		componentTypes.add(ComponentType.PETRINET);
		componentTypes.add(ComponentType.PLACE);
		componentTypes.add(ComponentType.TRANSITION);
		componentTypes.add(ComponentType.SOURCE_ARROW);
		componentTypes.add(ComponentType.DESTINATION_ARROW);
		componentTypes.add(ComponentType.COVERABILITY_GRAPH);
		assertArrayEquals(ComponentType.values(),componentTypes.toArray());
	}
	@Test
	public  void testMarkingState()
	{
		

		List<MarkingState> markingState=new ArrayList<>();
		markingState.add(MarkingState.NEW);
		markingState.add(MarkingState.OLD);
		markingState.add(MarkingState.DEADLOCK);
		markingState.add(MarkingState.VISITED);
		assertArrayEquals(MarkingState.values(),markingState.toArray());
		
	}

}
