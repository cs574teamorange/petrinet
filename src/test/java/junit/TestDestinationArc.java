package junit;

import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.TransitionManager;
import logic.model.DestinationArc;
import logic.model.LocationCoordinates;
import logic.model.Tokens;
import logic.model.Transition;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 * Created by anil on Oct 13, 2017
 *
 */
public class TestDestinationArc {

	@Test
	public void testIfemptyIsReturn()
	{
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		assertNotNull(destinationArc);
	}

	@Test
	public void testIfCopyWorks()
	{
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(34, 54), new Tokens(45),null);
		DestinationArc copied= (DestinationArc) ComponentManagerFactory.copyComponent(destinationArc);
		 assertNotSame(copied, destinationArc);
	}

	@Test
	public void testIfCopyWorksWithComponents()
	{
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(34, 54), new Tokens(45),null);
		DestinationArc copied= (DestinationArc) ComponentManagerFactory.copyComponent(destinationArc);
		 assertNotSame(copied.get_arcValueObj(), destinationArc.get_arcValueObj());
	}
	
	@Test
	public void testIfCopyWorksWithComponentsactualValueIsSame()
	{
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(34, 54), new Tokens(45),null);
		DestinationArc copied= (DestinationArc) ComponentManagerFactory.copyComponent(destinationArc);
		 assertSame(copied.get_arcValueObj().getIntCount(), destinationArc.get_arcValueObj().getIntCount());
	}
	
	@Test
	public void testIfComponentIsAttachedtoParent()
	{
		Transition transition=(Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.addComponent(transition, 0);
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.addComponent(destinationArc, transition.get_id());
		assertTrue(TransitionManager.getCurrentInstance().findComponent(transition.get_id()).getDestinationArcs().contains(destinationArc));
	}



}
