package junit;

import logic.model.Place;
import logic.model.Tokens;
import static org.junit.Assert.*;

import org.junit.Test;
/**
 * Created by anil on Oct 13, 2017
 *
 */
public class TestEqualsMethod {
	
	
	
	@Test
	public void testIfNullReturnsEqual()
	{
		Place place= new Place(1, new Tokens(5));
		assertFalse(place.equals(null));
	}
	
	@Test
	public void testIfNotEqual()
	{
		Place place= new Place(1, new Tokens(5));
		Place place2= new Place(2, new Tokens(45));
		assertFalse(place.equals(place2));
	}
	
	@Test
	public void testIfEqualsmethodWorksFine()
	{
		Place place= new Place(1, new Tokens(5));
		assertTrue(place.equals(place));
	}
	
	@Test
	public void testToStringMethod()
	{
		Place place= new Place(1, new Tokens(5));
		assertTrue(place.toString().compareToIgnoreCase(place.get_id()+"")==0);
	}


}
