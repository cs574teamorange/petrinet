package junit;

import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.PetriNetManager;
import logic.model.DestinationArc;
import logic.model.LocationCoordinates;
import logic.model.Place;
import logic.model.Tokens;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.instanceOf;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by anil on Oct 13, 2017
 *
 */
public class TestPlaceManager {


	@Before
	public void clearPreviousState()
	{
		PetriNetManager.initialize().getComponent().get_places().clear();
	}
	@Test
	public void testIfEmptyInstanceIsReturn()
	{
		assertNotNull(ComponentManagerFactory.getComponent(ComponentType.PLACE));
	}

	@Test
	public void testIfInstanceIsActualdesiredType()
	{
		assertThat(ComponentManagerFactory.getComponent(ComponentType.PLACE), instanceOf(Place.class));	
	}
	@Test
	public void testNotdesiredType()
	{
		assertFalse(ComponentManagerFactory.getComponent(ComponentType.PLACE).getClass()==DestinationArc.class);	
	}

	@Test
	public void testIfComponentIsNotAddedOnComponentCreation()
	{
		ComponentManagerFactory.getComponent(ComponentType.PLACE);
		assertEquals(0, PetriNetManager.initialize().getComponent().get_places().size());
	}

	@Test
	public void testIfMultipleComponentIsAddedOrNot()
	{
		Place place1=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		Place place2=(Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.addComponent(place1, 1);
		ComponentManagerFactory.addComponent(place2, 1);
		assertEquals(2, PetriNetManager.initialize().getComponent().get_places().size());
	}
	@Test
	public void testComponentMetaModelInitialised()
	{
		Place place=(Place)ComponentManagerFactory.getComponent(ComponentType.PLACE);
		LocationCoordinates locationCoordinates= new LocationCoordinates(12, 13);
		place.set_locationCoordinates(locationCoordinates);
		place.set_tokens(new Tokens(10));
		ComponentManagerFactory.addComponent(place, 0);
		List<Place> places= new ArrayList<>(PetriNetManager.initialize().getComponent().get_places());
		Place place12= places.get(0);
		assertNotNull(place12.get_tokens());
	}
	@Test
	public void testIfNotLocationInitialised()
	{
		Place place=(Place)ComponentManagerFactory.getComponent(ComponentType.PLACE);
		place.set_tokens(new Tokens(10));
		ComponentManagerFactory.addComponent(place, 0);
		List<Place> places= new ArrayList<>(PetriNetManager.initialize().getComponent().get_places());
		Place place12= places.get(0);
		assertNull(place12.get_locationCoordinates());
	}

	@Test
	public void testIfLocationInitialised()
	{
		Place place=(Place)ComponentManagerFactory.getComponent(ComponentType.PLACE);
		LocationCoordinates locationCoordinates= new LocationCoordinates(12, 13);
		place.set_locationCoordinates(locationCoordinates);
		place.set_tokens(new Tokens(10));
		ComponentManagerFactory.addComponent(place, 0);
		List<Place> places= new ArrayList<>(PetriNetManager.initialize().getComponent().get_places());
		Place p=null;
		for( int i=0;i<places.size();i++)
		{
			if(places.get(i).get_id()==place.get_id())
			{
				p=places.get(i);
				break;
			}
		}
		assertNotNull(p.get_locationCoordinates());
	}
	@Test
	public void testIfLocationUpdate()
	{
		Place place=(Place)ComponentManagerFactory.getComponent(ComponentType.PLACE);
		LocationCoordinates locationCoordinates= new LocationCoordinates(12, 13);
		place.set_locationCoordinates(locationCoordinates);
		ComponentManagerFactory.addComponent(place, 0);
		locationCoordinates= new LocationCoordinates(45, 45);
		ComponentManagerFactory.updateComponentMetaData(place, locationCoordinates, null, null);
		List<Place> places= new ArrayList<>(PetriNetManager.initialize().getComponent().get_places());
		Place p=null;
		for( int i=0;i<places.size();i++)
		{
			if(places.get(i).get_id()==place.get_id())
			{
				p=places.get(i);
				break;
			}
		}
		assertEquals(45,p.get_locationCoordinates().getxPoint());
	}
	@Test
	public void MakeCopyOfTheplaceTestForLocation()
	{
		Place place=(Place)ComponentManagerFactory.getComponent(ComponentType.PLACE);
		LocationCoordinates locationCoordinates= new LocationCoordinates(12, 13);
		place.set_locationCoordinates(locationCoordinates);
		Tokens tokens= new Tokens(34);
		place.set_tokens(tokens);
		Place placeCopied=(Place)ComponentManagerFactory.copyComponent(place);
		assertEquals(place.get_locationCoordinates().getxPoint(),placeCopied.get_locationCoordinates().getxPoint());
	}
	
	@Test
	public void MakeCopyOfTheplaceTestForToken()
	{
		Place place=(Place)ComponentManagerFactory.getComponent(ComponentType.PLACE);
		LocationCoordinates locationCoordinates= new LocationCoordinates(12, 13);
		place.set_locationCoordinates(locationCoordinates);
		Tokens tokens= new Tokens(34);
		place.set_tokens(tokens);
		Place placeCopied=(Place)ComponentManagerFactory.copyComponent(place);
		assertEquals(place.get_tokens().getIntCount(),placeCopied.get_tokens().getIntCount());
	}


}
