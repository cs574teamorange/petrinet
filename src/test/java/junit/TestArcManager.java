package junit;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import logic.model.ActiveArc;
import logic.model.DestinationArc;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Tokens;
import logic.utilities.ArcManager;
import logic.utilities.DefaultArcManager;

/**
 * Created by anil on Sep 23, 2017
 *
 */
public class TestArcManager {

	private SourceArc sourceArc1;
	private SourceArc sourceArc2;
	private DestinationArc destinationArc1;
	private DestinationArc destinationArc2;
	Set<ActiveArc> activeArcs;
	int sourArc1Val=5;
	int sourceArc2Val=20;
	int destinationArc1Val=5;
	int destinationArc2Val=5;
	int place0initialToken=10;
	int place1initialToken=15;
	int place4initialToken=10;
	int place5initialToken=0;

	ArcManager arcManager= new DefaultArcManager();
	@Before
	public void initialiseArcsValue()
	{
		Place place0= new Place(0, new Tokens(place0initialToken));
		sourceArc1=new SourceArc(1, place0, sourArc1Val);
		Place place1=new Place(2, new Tokens(place1initialToken));
		sourceArc2=new SourceArc(3, place1, sourceArc2Val);
		Place place4=new Place(4, new Tokens(place4initialToken));
		destinationArc1= new DestinationArc(5, place4, destinationArc1Val);
		Place place5=new Place(6, new Tokens(place5initialToken));
		destinationArc2= new DestinationArc(7, place5, destinationArc2Val);
		activeArcs= new TreeSet<ActiveArc>();
		activeArcs.add(sourceArc1);
		activeArcs.add(sourceArc2);
		activeArcs.add(destinationArc1);
		activeArcs.add(destinationArc2);
	}
	@Test
	public void testSourceActivateArcBehaviour()
	{
		arcManager.activateArcs(activeArcs);
		Assert.assertEquals(place0initialToken-sourArc1Val, ((SourceArc)activeArcs.iterator().next()).get_place().get_tokens().getIntCount());
	}
	@Test
	public void testDestinationActiveArcBehaviour()
	{
		arcManager.activateArcs(activeArcs);
		ActiveArc currentActiveArc=null;
		Iterator<ActiveArc> iterator=activeArcs.iterator();
		while(iterator.hasNext())
			currentActiveArc= iterator.next();
		Assert.assertEquals(place5initialToken+destinationArc2Val,((DestinationArc)currentActiveArc).get_place().get_tokens().getIntCount());	
	}
}
