package junit;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.junit.Test;
import org.xml.sax.SAXException;

import io.PetriSerializerException;
import junit.framework.Assert;
import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.PetriNetIO;
import logic.componentManager.PetriNetManager;
import logic.componentManager.PlaceManager;
import logic.model.ArcValue;
import logic.model.DestinationArc;
import logic.model.LocationCoordinates;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Tokens;
import logic.model.Transition;
/**
 * Created by anil on Oct 13, 2017
 *
 */
public class TestPetriNetIO {


	@Test
	public void testPetriNetParser() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		assertNotNull(PetriNetIO.readPetriNet("./src/test/resources/VendingMachine.xml"));
	}

	@Test
	public void testPetriNetWrite() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException, TransformerException {
		String readPath = "./src/test/resources/VendingMachine.xml";
		String writePath = "./src/test/resources/VendingMachine-out.xml";

		PetriNet petriNet = PetriNetIO.readPetriNet(readPath);
		PetriNetIO.initializePetriNet(petriNet);
		PetriNetIO.writePetriNet(writePath, petriNet);

		assertTrue(new File(writePath).exists());
	}

	@Test
	public void testIfPetriNetInitializerWorksFine() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet=PetriNetIO.readPetriNet("./src/test/resources/VendingMachine.xml");
		PetriNetIO.initializePetriNet(petriNet);
		assertEquals(petriNet, PetriNetManager.initialize().getComponent());

	}
	@Test
	public void testIfPetriNetisValidOrNot() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		assertTrue(PetriNetIO.readPetriNet("./src/test/resources/VendingMachine.xml").validate());	
	}


	@Test
	public void testForValidPetriNetCreatedManually()
	{
		PetriNetManager petriNetManager=PetriNetManager.initialize();//initialise YourPetriNet
		Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(45, 45),new Tokens(5),null); //update the component //null is because place has nothing to attach any component
		ComponentManagerFactory.addComponent(place1, null); //add the component to the petriNet //null because place has no parent
		Place place2= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(place2,new LocationCoordinates(45, 45),new Tokens(5),null);
		ComponentManagerFactory.addComponent(place2, null);
		Transition transition1= (Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.updateComponentMetaData(transition1, new LocationCoordinates(45, 45), null, null);
		ComponentManagerFactory.addComponent(transition1, null);
		SourceArc sourceArc= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(4),place1); // arc is attach to place 
		ComponentManagerFactory.addComponent(sourceArc, transition1.get_id());// transition is parent to arc

		SourceArc sourceArc12= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc12, new LocationCoordinates(45, 45), new ArcValue(6),place1);
		ComponentManagerFactory.addComponent(sourceArc12, transition1.get_id());

		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(5),place2);
		ComponentManagerFactory.addComponent(destinationArc, transition1.get_id());

		DestinationArc destinationArc12= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc12, new LocationCoordinates(45, 45), new ArcValue(7),place1);
		ComponentManagerFactory.addComponent(destinationArc12, transition1.get_id());
		assertTrue(petriNetManager.getComponent().validate());	
	}

	@Test
	public void testForInvalidPetriNet()
	{
		PetriNetManager petriNetManager=PetriNetManager.initialize();//initialise YourPetriNet
		Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(45, 45),new Tokens(5),null); //update the component //null is because place has nothing to attach any component
		ComponentManagerFactory.addComponent(place1, null); //add the component to the petriNet //null because place has no parent
		Place place2= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(place2,new LocationCoordinates(45, 45),new Tokens(5),null);
		ComponentManagerFactory.addComponent(place2, null);
		Transition transition1= (Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.updateComponentMetaData(transition1, new LocationCoordinates(45, 45), null, null);
		ComponentManagerFactory.addComponent(transition1, null);	
		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(5),place2);
		ComponentManagerFactory.addComponent(destinationArc, transition1.get_id());

		DestinationArc destinationArc12= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc12, new LocationCoordinates(45, 45), new ArcValue(7),place1);
		ComponentManagerFactory.addComponent(destinationArc12, transition1.get_id());
		assertFalse(petriNetManager.getComponent().validate());	
	}

	@Test
	public void testForInvalidPetriNetWithNoTransition()
	{
		PetriNetManager petriNetManager=PetriNetManager.initialize();//initialise YourPetriNet
		Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(45, 45),new Tokens(5),null); //update the component //null is because place has nothing to attach any component
		ComponentManagerFactory.addComponent(place1, null); //add the component to the petriNet //null because place has no parent
		Place place2= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(place2,new LocationCoordinates(45, 45),new Tokens(5),null);
		ComponentManagerFactory.addComponent(place2, null);
		assertFalse(petriNetManager.getComponent().validate());	
	}

	@Test
	public void testValidPetriNetState()
	{
		PetriNetManager petriNetManager=PetriNetManager.initialize();//initialise YourPetriNet
		Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(45, 45),new Tokens(5),null); //update the component //null is because place has nothing to attach any component
		ComponentManagerFactory.addComponent(place1, null); //add the component to the petriNet //null because place has no parent
		Place place2= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(place2,new LocationCoordinates(45, 45),new Tokens(5),null);
		ComponentManagerFactory.addComponent(place2, null);
		Transition transition1= (Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.updateComponentMetaData(transition1, new LocationCoordinates(45, 45), null, null);
		ComponentManagerFactory.addComponent(transition1, null);
		SourceArc sourceArc= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(4),place1); // arc is attach to place 
		ComponentManagerFactory.addComponent(sourceArc, transition1.get_id());// transition is parent to arc

		SourceArc sourceArc12= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc12, new LocationCoordinates(45, 45), new ArcValue(6),place1);
		ComponentManagerFactory.addComponent(sourceArc12, transition1.get_id());

		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(5),place2);
		ComponentManagerFactory.addComponent(destinationArc, transition1.get_id());

		DestinationArc destinationArc12= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc12, new LocationCoordinates(45, 45), new ArcValue(7),place1);
		ComponentManagerFactory.addComponent(destinationArc12, transition1.get_id());

		PetriNetState petriNetState= new PetriNetState(1,petriNetManager.getComponent().get_places(),MarkingState.NEW,null,null);
		assertTrue(petriNetState.validate());

	}
	
	@Test
	public void testInValidPetriNetState()
	{
		PetriNetManager petriNetManager=PetriNetManager.initialize();//initialise YourPetriNet
		Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(45, 45),new Tokens(-5),null); //update the component //null is because place has nothing to attach any component
		ComponentManagerFactory.addComponent(place1, null); //add the component to the petriNet //null because place has no parent
		Place place2= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE);
		ComponentManagerFactory.updateComponentMetaData(place2,new LocationCoordinates(45, 45),new Tokens(5),null);
		ComponentManagerFactory.addComponent(place2, null);
		Transition transition1= (Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
		ComponentManagerFactory.updateComponentMetaData(transition1, new LocationCoordinates(45, 45), null, null);
		ComponentManagerFactory.addComponent(transition1, null);
		SourceArc sourceArc= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc, new LocationCoordinates(45, 45), new ArcValue(4),place1); // arc is attach to place 
		ComponentManagerFactory.addComponent(sourceArc, transition1.get_id());// transition is parent to arc

		SourceArc sourceArc12= (SourceArc) ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
		ComponentManagerFactory.updateComponentMetaData(sourceArc12, new LocationCoordinates(45, 45), new ArcValue(6),place1);
		ComponentManagerFactory.addComponent(sourceArc12, transition1.get_id());

		DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc, new LocationCoordinates(45, 45), new ArcValue(5),place2);
		ComponentManagerFactory.addComponent(destinationArc, transition1.get_id());

		DestinationArc destinationArc12= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
		ComponentManagerFactory.updateComponentMetaData(destinationArc12, new LocationCoordinates(45, 45), new ArcValue(7),place1);
		ComponentManagerFactory.addComponent(destinationArc12, transition1.get_id());

		PetriNetState petriNetState= new PetriNetState(1,petriNetManager.getComponent().get_places(),MarkingState.NEW,null,null);
		assertFalse(petriNetState.validate());
	}
}
