package junit;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;

import logic.model.DestinationArc;
import logic.model.Firable;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Tokens;
import logic.model.Transition;
import logic.utilities.CoverabilityTree;
import logic.utilities.DefaultArcManager;
import logic.utilities.DefaultMarkingStateManager;
import logic.utilities.DefaultTransitionFiringManager;
import logic.utilities.MarkingStateManager;
import logic.utilities.TransitionFiringManager;

/**
 * Created by anil on Sep 23, 2017
 * This class provides basic petriNet for running various test. This petri net is taken from the professor lecture notes.
 */
public abstract class DemoPetriNet {


	protected Set<PetriNetState> petriNetStates=new TreeSet<>();
	@Before
	public void initialisePetriNet()
	{
		PetriNet petriNet=this.getPetriNet();
		TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
		PetriNetState initialState= new PetriNetState(1,markingStateManager.getcurrentStateofPetriNet(petriNet.get_places()),MarkingState.NEW,null,null);
		petriNet.set_petriNetState(initialState);
		petriNetStates=coverabilityTree.generateConverabilityTree(petriNet);
	}
	public abstract PetriNet getPetriNet();
}
