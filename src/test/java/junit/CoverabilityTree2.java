package junit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JTree;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import io.PetriSerializerException;
import logic.componentManager.PetriNetIO;
import logic.model.DestinationArc;
import logic.model.Firable;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Tokens;
import logic.model.Transition;
import logic.utilities.CoverabilityTree;
import logic.utilities.CoverabilityTreeManager;
import logic.utilities.DefaultArcManager;
import logic.utilities.DefaultMarkingStateManager;
import logic.utilities.DefaultTransitionFiringManager;
import logic.utilities.MarkingStateManager;
import logic.utilities.PetriNetInferencer;
import logic.utilities.TransitionFiringManager;
import logic.utilities.exception.InvalidPetriNetStateException;

/**
 * Created by anil on Sep 23, 2017
 * The petri net used in this class is the one that was our homework on assignment.
 */
public class CoverabilityTree2 extends DemoPetriNet {

	@Override
	public PetriNet getPetriNet() {
		Place place1= new Place(1, new Tokens(1));
		Place place2= new Place(2, new Tokens(0));
		Place place3= new Place(3, new Tokens(1));
		Place place4= new Place(4, new Tokens(0));
		Set<Place> places= new TreeSet<>();
		Set<Firable> transition= new TreeSet<>();
		places.add(place1);
		places.add(place2);
		places.add(place3);
		places.add(place4);
		SourceArc sourceArc1=new SourceArc(5, place1, 1);
		SourceArc sourceArc2=new SourceArc(6, place2, 1);
		SourceArc sourceArc3=new SourceArc(7, place3, 1);

		Set<SourceArc> sourceArc= new TreeSet<>();
		sourceArc.add(sourceArc1);
		sourceArc.add(sourceArc2);
		sourceArc.add(sourceArc3);
		DestinationArc destinationArc=new DestinationArc(8, place1,1);
		Set<DestinationArc> destinationArcs= new TreeSet<>();
		destinationArcs.add(destinationArc);
		Transition transition1=new Transition(9, sourceArc, destinationArcs,"T1");
		transition.add(transition1);

		Set<SourceArc> sourceArcs1= new TreeSet<>();
		SourceArc sourceArc1_1=new SourceArc(10, place4, 1);
		sourceArcs1.add(sourceArc1_1);
		Set<DestinationArc> destinationArcs1=new TreeSet<>();
		DestinationArc destinationArc2= new DestinationArc(11, place2, 1);
		DestinationArc destinationArc3= new DestinationArc(12, place3, 1);
		destinationArcs1.add(destinationArc3);
		destinationArcs1.add(destinationArc2);
		Transition transition2= new Transition(13, sourceArcs1, destinationArcs1,"T2");
		transition.add(transition2);
		SourceArc sourceArc4=new SourceArc(14, place3, 1);
		DestinationArc destinationArc4= new DestinationArc(15, place4, 1);
		Set<SourceArc> sourceArcs3=new TreeSet<>();
		Set<DestinationArc> destinationArcs3= new TreeSet<>();
		sourceArcs3.add(sourceArc4);
		destinationArcs3.add(destinationArc4);
		Transition transition3= new Transition(16, sourceArcs3, destinationArcs3,"T3");
		transition.add(transition3);
		return new PetriNet(17, places, transition);
	}
	@Test
	public void testIfBounded() throws InvalidPetriNetStateException
	{
		PetriNetInferencer petriNetInferencer=new PetriNetInferencer(petriNetStates,getPetriNet().get_transitions().size());
		Assert.assertFalse(petriNetInferencer.isPetriNetBounded());
	}
	@Test
	public void testIfSafe() throws InvalidPetriNetStateException
	{
		PetriNetInferencer petriNetInferencer=new PetriNetInferencer(petriNetStates,getPetriNet().get_transitions().size());
		Assert.assertFalse(petriNetInferencer.isPetriNetSafe());
	}
	@Test
	public void testIfCoverabilityTreeCouldBeDone() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet=PetriNetIO.readPetriNet("./src/test/resources/VendingMachine.xml");
		TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
		int[] currentMarkingState=markingStateManager.getcurrentStateofPetriNet(petriNet.get_places());
		PetriNetState initialState= new PetriNetState(1,currentMarkingState,MarkingState.NEW,null,null);
		petriNet.set_petriNetState(initialState);		
		Set<PetriNetState> petriNetStates=coverabilityTree.generateConverabilityTree(petriNet);
		CoverabilityTreeManager coverabilityManaager= new CoverabilityTreeManager(petriNetStates);
		JTree jtree=coverabilityManaager.getDisplayableCoverabilitytree();
		assertNotNull(jtree);
	}
	@Test
	public void testPetriNetCopy() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		PetriNet petriNet=PetriNetIO.readPetriNet("./src/test/resources/VendingMachine.xml");
		PetriNet petriNetCopied= new PetriNet(petriNet);
		assertEquals(petriNetCopied.get_places(), petriNetCopied.get_places());
		assertEquals(petriNet.get_petriNetState(), petriNet.get_petriNetState());
	}
	
}
