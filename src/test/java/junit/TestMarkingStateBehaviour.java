package junit;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

import logic.model.MarkingState;
import logic.model.PetriNetState;
import logic.model.Place;
import logic.model.Tokens;
import static org.junit.Assert.*;
/**
 * Created by anil on Oct 13, 2017
 *
 */
public class TestMarkingStateBehaviour {
	
	
	
	@Test
	public void testIfPetriNetConstructorWorksfineAlternateconstructor()
	{
		PetriNetState petriNetState= new PetriNetState(1,new int[]{1,0,1,0} , MarkingState.NEW, null, null);
		assertArrayEquals(new int[]{1,0,1,0},petriNetState.getTokenCount());	
	}
	@Test
	public void testIfPetriNetConstructorWorksfine()
	{
		Place place1= new Place(1, new Tokens(1));
		Place place2= new Place(2, new Tokens(0));
		Place place3= new Place(3, new Tokens(1));
		Place place4= new Place(4, new Tokens(0));
		Set<Place> places= new TreeSet<Place>();
		places.add(place1);
		places.add(place2);
		places.add(place3);
		places.add(place4);
		PetriNetState petriNetState= new PetriNetState(1, places, MarkingState.NEW, null, null);
		assertArrayEquals(new int[]{1,0,1,0},petriNetState.getTokenCount());	
	}

}
