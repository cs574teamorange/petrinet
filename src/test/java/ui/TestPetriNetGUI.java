package ui;

import logic.componentManager.PetriNetIO;
import logic.componentManager.PetriNetManager;
import logic.model.*;
import org.junit.Test;

import java.awt.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.util.HashSet;
import java.util.Set;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by edavis on 10/17/17.
 */
public class TestPetriNetGUI {
    @Test
    public void testMenu() {
        Menu menu = new Menu();
        menu.create_menu();

        String[] captions = new String[] { "File..", "Tools" };
        String[][] subcaptions = new String[2][];
        subcaptions[0] = new String[] {"New", "Open..", "Save as.."};
        subcaptions[1] = new String[] {"Coverability", "Is bounded?", "Reachable marking.."};

        int nMenus = menu.getMenuCount();
        int nSubMenus = 3;
        assertTrue("GUI Menu Count", nMenus == captions.length);

        for (int i = 0; i < nMenus; i++) {
            JMenu submenu = (JMenu) menu.getComponent(i);
            assertTrue("GUI Menu Captions", captions[i].equals(submenu.getText()));
            int nItems = submenu.getItemCount();
            assertTrue("GUI Submenu Counts", nItems == nSubMenus);

            for (int j = 0; j < nItems; j++) {
                JMenuItem item = submenu.getItem(j);
                assertTrue("GUI Submenu Captions", subcaptions[i][j].equals(item.getText()));
                assertTrue("GUI Menu Listeners", item.getActionListeners().length > 0);
            }
        }
    }

    @Test
    public void testToolbar() {
        Toolbar toolbar = new Toolbar();
        toolbar.create_toolbar();

        Component[] components = toolbar.getComponents();
        int nComponents = 16;
        assertTrue("Toolbar Comonent Count", components.length == nComponents);

        String[] captions = new String[] {"Cut", "Copy", "Paste", "", "Undo", "Redo", "", "Select", "Connect",
                                          "Add place", "Add transition", "", "Mark", "", "Fire"};

        int i = 0;
        for (; i < 3; i++) {
            assertTrue(i + "th Component Label", ((JButton) components[i]).getText().equals(captions[i]));
        }

        for (i = 4; i < 6; i++) {
            assertTrue(i + "th Component Label", ((JButton) components[i]).getText().equals(captions[i]));
        }

        for (i = 7; i < 11; i++) {
            assertTrue(i + "th Component Label", ((JButton) components[i]).getText().equals(captions[i]));
        }

        i = 12;
        assertTrue(i + "th Component Label", ((JButton) components[i]).getText().equals(captions[i]));

        i = 14;
        assertTrue(i + "th Component Label", ((JButton) components[i]).getText().equals(captions[i]));
    }

    @Test
    public void testCanvas() {
        PaintingCanvas canvas = new PaintingCanvas();
        Set<Place> places = canvas.get_selected_places();
        Set<Transition> transitions = canvas.get_selected_transitions();

        assertTrue("GUI Canvas Selected Places", places.size() == 0);
        assertTrue("GUI Canvas Selected Transitions", transitions.size() == 0);
        assertTrue("GUI Canvas Mouse Listeners", canvas.getMouseListeners().length > 0);
        assertTrue("GUI Canvas Motion Listeners", canvas.getMouseMotionListeners().length > 0);

        // Test paintcomponent...
        PetriNetManager.initialize().clear();
        BufferedImage image = new BufferedImage(640, 480, TYPE_INT_RGB);
        canvas.paintComponent(image.getGraphics());
        assertTrue(canvas.getBackground().equals(Color.WHITE));
    }

    @Test
    public void testGUI() {
        PetriNetGUI gui = new PetriNetGUI();
        assertTrue("GUI Visible", gui.isVisible());
        gui.dispose();
        assertTrue("GUI Invisible", !gui.isVisible());

        // Test Main
        try {
            PetriNetGUI.main(new String[]{});
        } catch (Exception ex) {
            fail(ex.toString());
        }
    }

    @Test
    public void testDisplayCoverabilityTree() {
        try {
            PetriNet petriNet = PetriNetIO.readPetriNet("./src/test/resources/HW1PN1.xml");
            PetriNetManager.initialize().setComponent(petriNet);
            DisplayCoverabilityTree displayCoverabilityTree = new DisplayCoverabilityTree();
            assertTrue("Coverability Tree Visible", displayCoverabilityTree.isVisible());
            displayCoverabilityTree.dispose();
            assertTrue("Coverability Tree Invisible", !displayCoverabilityTree.isVisible());

            // Test Main
            DisplayCoverabilityTree.main(new String[] {});
        } catch (Exception ex) {
            fail(ex.toString());
        }
    }

    @Test
    public void testIconAdder() {
        JButton button = new JButton("New");
        assertTrue("Button Created", button.getLabel() == "New");
        IconAdder.add(button);
        Icon icon = button.getIcon();
        int size = 15;
        assertTrue("Icon Width", icon.getIconWidth() == size);
        assertTrue("Icon Height", icon.getIconHeight() == size);
    }

    @Test
    public void testPopMenu() {
        PopMenu popMenu = new PopMenu();
        Place p1  = new Place(1, new Tokens(1), "p1");
        Place p2  = new Place(2, new Tokens(0), "p2");

        SourceArc src = new SourceArc(3, p1, 1);
        Set<SourceArc> sourceArcs = new HashSet<>(1);
        sourceArcs.add(src);

        DestinationArc dest = new DestinationArc(4, p2, 1);
        Set<DestinationArc> destArcs = new HashSet<>(1);
        destArcs.add(dest);

        Transition t1 = new Transition(3, sourceArcs, destArcs, "t1");

        JPopupMenu editMenu = popMenu.pop_menu();
        assertTrue("Test Popup Edit Menu", editMenu.getComponents().length == 3);

        JPopupMenu placeMenu = popMenu.pop_menu(p1);
        assertTrue("Test Popup Place Menu", placeMenu.getComponents().length == 6);

        JPopupMenu transMenu = popMenu.pop_menu(t1);
        assertTrue("Test Popup Transition Menu", transMenu.getComponents().length == 5);

        JPopupMenu srcMenu = popMenu.pop_menu(src);
        assertTrue("Test Popup Source Menu", srcMenu.getComponents().length == 5);

        JPopupMenu destMenu = popMenu.pop_menu(dest);
        assertTrue("Test Popup Source Menu", destMenu.getComponents().length == 5);
    }
}
