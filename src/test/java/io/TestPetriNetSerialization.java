package io;

import logic.componentManager.PetriNetManager;
import logic.model.PetriNet;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Created by edavis on 10/2/17.
 */
public class TestPetriNetSerialization {
    @Test
    public void testSimpleNet() {
        PetriSerializationTester.run("SimplePN");
    }

    @Test
    public void testHW1PN1() {
        PetriSerializationTester.run("HW1PN1");
    }

    @Test
    public void testHW1PN2() {
        PetriSerializationTester.run("HW1PN2");
    }

    @Test
    public void testCommProtocol() {
        PetriSerializationTester.run("CommProtocol");
    }

    @Test
    public void testDiningPhilosophers() {
        PetriSerializationTester.run("DiningPhilosophers");
    }

    @Test
    public void testReadersWriters() {
        PetriSerializationTester.run("ReadersWriters");
    }

    @Test
    public void testVendingMachine() {
        PetriSerializationTester.run("VendingMachine");
    }

    @Test
    public void testToString() {
        String expected = "p1(1)\n" +
            "p2(0)\n" +
            "t1(2,4)\n" +
            "p1 -> (1) -> t1\n" +
            "p1 -> (1) -> t1\n" +
            "t1 -> (1) -> p1\n" +
            "t1 -> (1) -> p2\n" +
            "t1 -> (1) -> p1\n" +
            "t1 -> (1) -> p2\n";

        PetriNetSerializer serializer = PetriSerializationTester.run("SimplePN");
        String actual = serializer.toString();

        assertTrue("Asserting PetriNetSerializer.toString matches.", actual.length() == expected.length());
    }

    @Test
    public void testClone() {
        PetriNetSerializer serializer = PetriSerializationTester.run("SimplePN");
        PetriNet orig = serializer.get_petrinet();
        PetriNet clone = PetriNetManager.initialize().clone(orig);
        assertTrue(PetriSerializationTester.netsEqual(orig, clone));
    }

    @Test
    public void testException() {
        try {
            throw new PetriSerializerException("Testing PetriSerializerException...");
            //fail("PetriSerializerException not thrown.");
        } catch (PetriSerializerException pse) {
            assertTrue("Asserting PetriSerializerException thrown.", pse.getMessage().length() > 0);
        }
    }
}
