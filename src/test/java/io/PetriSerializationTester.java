package io;

import logic.model.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by edavis on 10/2/17.
 */
public class PetriSerializationTester {
    private static String _testPath = "./src/test/resources/";

    public static PetriNetSerializer run(String testName) {
        String testLabel = "PetriSerializationTester(" + testName + "): ";
        String xmlIn = _testPath + testName + ".xml";
        String xmlOut = _testPath + testName + "-out.xml";

        File xmlFile = new File(xmlIn);
        assertTrue(testLabel + "Asserting input file exists.", xmlFile.exists());

        PetriNetSerializer serializer = new PetriNetSerializer();

        try {
            FileInputStream fin = new FileInputStream(xmlFile);
            serializer.readObject(fin);
        } catch (Exception ex) {
            fail(testLabel + ex.toString());
        }

        PetriNet pnRead = serializer.get_petrinet();
        assertTrue(testLabel + "Asserting read net is defined.", pnRead != null);

        // Now serialize it back to a file...
        try {
            FileOutputStream fout = new FileOutputStream(xmlOut);
            serializer.writeObject(fout);
        } catch (Exception ex) {
            fail(testLabel + ex.toString());
        }

        xmlFile = new File(xmlOut);
        assertTrue(testLabel + "Asserting output file exists.", xmlFile.exists());

        try {
            FileInputStream fin = new FileInputStream(xmlFile);
            serializer.readObject(fin);
        } catch (Exception ex) {
            fail(testLabel + ex.toString());
        }

        PetriNet pnWrite = serializer.get_petrinet();
        assertTrue(testLabel + "Asserting written net is defined.", pnWrite != null);

        // Compare the two nets...
        assertTrue(testLabel + "Asserting Petri net equality.", netsEqual(pnRead, pnWrite));

        // Delete output file...
        xmlFile.delete();

        return serializer;
    }

    public static boolean netsEqual(PetriNet pn1, PetriNet pn2) {
        boolean areEqual = pn1.get_places().size() == pn2.get_places().size() &&
                           pn1.get_transitions().size() == pn2.get_transitions().size();

        // Check whether places are equal...
        if (areEqual) {
            List<Place> p1List = new ArrayList<>(pn1.get_places());
            List<Place> p2List = new ArrayList<>(pn2.get_places());

            for (int i = 0; i < p1List.size() && areEqual; i++) {
                Place p1 = p1List.get(i);
                Place p2 = p2List.get(i);

                String p1Name = p1.getPlaceName();
                String p2Name = p2.getPlaceName();

                int p1Tokens = p1.get_tokens().getIntCount();
                int p2Tokens = p2.get_tokens().getIntCount();

                int p1X = p1.get_locationCoordinates().getxPoint();
                int p1Y = p1.get_locationCoordinates().getyPoint();
                int p2X = p2.get_locationCoordinates().getxPoint();
                int p2Y = p2.get_locationCoordinates().getyPoint();

                areEqual = (p1Tokens == p2Tokens && p1X == p2X && p1Y == p2Y && p1Name.equals(p2Name));
            }
        }

        // Check whether transitions are equal
        if (areEqual) {
            List<Firable> t1List = new ArrayList<>(pn1.get_transitions());
            List<Firable> t2List = new ArrayList<>(pn2.get_transitions());

            for (int i = 0; i < t1List.size() && areEqual; i++) {
                Transition t1 = (Transition) t1List.get(i);
                Transition t2 = (Transition) t2List.get(i);

                String t1Name = t1.getTransitionName();
                String t2Name = t2.getTransitionName();

                int t1X = t1.get_locationCoordinates().getxPoint();
                int t1Y = t1.get_locationCoordinates().getyPoint();
                int t2X = t2.get_locationCoordinates().getxPoint();
                int t2Y = t2.get_locationCoordinates().getyPoint();

                List<SourceArc> t1SourceArcs = new ArrayList<>(t1.getSourceArcs());
                List<SourceArc> t2SourceArcs = new ArrayList<>(t2.getSourceArcs());
                List<DestinationArc> t1DestArcs = new ArrayList<>(t1.getDestinationArcs());
                List<DestinationArc> t2DestArcs = new ArrayList<>(t2.getDestinationArcs());

                areEqual = (t1X == t2X && t1Y == t2Y && t1Name.equals(t2Name) &&
                            t1SourceArcs.size() == t2SourceArcs.size() &&
                            t1DestArcs.size() == t2DestArcs.size());

                // Compare source arcs...
                if (areEqual) {
                    for (int j = 0; j < t1SourceArcs.size() && areEqual; j++) {
                        SourceArc t1SourceArc = t1SourceArcs.get(j);
                        SourceArc t2SourceArc = t2SourceArcs.get(j);

                        areEqual = (t1SourceArc.get_place().getPlaceName().equals(t2SourceArc.get_place().getPlaceName()) &&
                                    t1SourceArc.get_transtion().getTransitionName().equals(t2SourceArc.get_transtion().getTransitionName()) &&
                                    t1SourceArc.get_arcValue() == t2SourceArc.get_arcValue());
                    }
                }

                // Compare dest arcs...
                if (areEqual) {
                    for (int j = 0; j < t1DestArcs.size() && areEqual; j++) {
                        DestinationArc t1DestArc = t1DestArcs.get(j);
                        DestinationArc t2DestArc = t2DestArcs.get(j);

                        areEqual = (t1DestArc.get_place().getPlaceName().equals(t2DestArc.get_place().getPlaceName()) &&
                                //t1DestArc.get_transtion().getTransitionName().equals(t2DestArc.get_transtion().getTransitionName()) &&
                                t1DestArc.get_arcValue() == t2DestArc.get_arcValue());
                    }
                }
            }
        }

        return areEqual;
    }
}
