package logic.utilities;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.IntStream;

import logic.model.Firable;
import logic.model.PetriNetState;
import logic.utilities.exception.InvalidPetriNetStateException;

/**
 * 
 * defensive programming 
 * Created by anil on Sep 19, 2017
 *
 */
public class PetriNetInferencer {
	private Set<PetriNetState> _petrinetStates;
	private int _transitionCount;

	/**
	 *  A construcotor that initialized the set of PetriNetState.
	 * @param petriNetStates the petrinet all possible states
	 */
	public PetriNetInferencer(Set<PetriNetState> petriNetStates, int transitionCount)
	{
		this._petrinetStates=petriNetStates;
		this._transitionCount=transitionCount;
	}
	/**
	 * check if the return petri net is bounded or not
	 * @return true, if the petri net is bounded else return false
	 * 
	 */
	public boolean isPetriNetBounded() throws InvalidPetriNetStateException
	{
		Iterator<PetriNetState> iterators= _petrinetStates.iterator();
		while(iterators.hasNext())
		{
			PetriNetState petriNetState=iterators.next();
			if(!petriNetState.validate())
			{
				throw new InvalidPetriNetStateException("The petriNetState is invalid");

			}
			if(IntStream.of(petriNetState.getTokenCount()).anyMatch(x -> x==1000)){
				return false;
			}
		}
		return true;
	}

	/**
	 * check if the petri Net is safe or not
	 * @return true if petrinet is safe, else return false
	 */ 
	public boolean isPetriNetSafe() throws InvalidPetriNetStateException
	{
		Iterator<PetriNetState> iterators= _petrinetStates.iterator();
		while(iterators.hasNext())
		{
			PetriNetState petriNetState=iterators.next();
			if(!petriNetState.validate())
			{
				throw new InvalidPetriNetStateException("The petriNetState is invalid");

			}

			if(IntStream.of(petriNetState.getTokenCount()).anyMatch(x -> x>1)){
				return false;
			}
		}
		return true;
	}
	
	public boolean isPetriNetL1Live()
	{
		Set<Firable> firedTransition= new TreeSet<>();
		Iterator<PetriNetState> iterators= _petrinetStates.iterator();
		while(iterators.hasNext())
		{
			Firable firable=iterators.next().get_transitionFired();
			if(firable!=null)
			firedTransition.add(firable);
		}
		if(firedTransition.size()==_transitionCount)
			return true;
		return false;	
	}
	
	public boolean isPetriNetL0Live()
	{
		Set<Firable> firedTransition= new TreeSet<>();
		Iterator<PetriNetState> iterators= _petrinetStates.iterator();
		while(iterators.hasNext())
		{
			Firable firable=iterators.next().get_transitionFired();
			if(firable!=null)
			firedTransition.add(firable);
		}
		if(firedTransition.size()==_transitionCount)
			return false;
		return true;		
	}
	
	public boolean isMarkingStateReachable(int[] markingState)
	{
		Iterator<PetriNetState> iterators= _petrinetStates.iterator();
		while(iterators.hasNext())
		{
			PetriNetState petriNetState=iterators.next();
			if(petriNetState.equalsLogical(markingState))
				return true;
		}
		return false;
		}

}
