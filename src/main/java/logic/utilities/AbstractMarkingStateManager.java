package logic.utilities;

import java.util.Iterator;
import java.util.Set;

import logic.model.MarkingState;
import logic.model.PetriNetState;
import logic.model.Place;

/**
 * Created by anil on Sep 19, 2017
 * 
 * This class is a basic implementation of MarkingStateManager that abstractly implement all the exposed interface.
 *
 */
public abstract class AbstractMarkingStateManager implements MarkingStateManager {

	@Override
	public PetriNetState isGivenStateCoverabableByPreviousStates(PetriNetState petriNetState) {

		PetriNetState parentPetriNetState=petriNetState.getParentMarking();
		while(parentPetriNetState!=null)
		{
			if(parentPetriNetState.coversThisState(petriNetState.getTokenCount())){
				petriNetState=petriNetState.getAdjustedState(parentPetriNetState);
				break;
			}
			parentPetriNetState=parentPetriNetState.getParentMarking();
		}
		return petriNetState;
	}

	@Override
	public int[] getcurrentStateofPetriNet(Set<Place> places)
	{
		int[] tokenCount=new int[places.size()];
		Iterator<Place> iterator=places.iterator();
		int i=0;
		while(iterator.hasNext()){
			tokenCount[i]=iterator.next().get_tokens().getIntCount();
			i++;
		}
		return tokenCount;
	}

	@Override
	public MarkingState currentPetriNetState(PetriNetState petriNetState, Set<PetriNetState> petriNetStates) {

		Iterator<PetriNetState> states= petriNetStates.iterator();
		while(states.hasNext())
		{
			if(states.next().equalsLogical(petriNetState.getTokenCount()))
				return  MarkingState.OLD;
		}
		return MarkingState.NEW;
	}
}
