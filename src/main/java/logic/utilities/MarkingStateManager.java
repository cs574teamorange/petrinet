package logic.utilities;

import java.util.Set;

import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.model.Place;

/**
 * Created by anil on Sep 19, 2017
 * This is a utility interface that handles operation within the marking states of the coverability tree. 
 *
 */
public interface MarkingStateManager {
	/**
	 * 
	 *  This method check if the given marking state of the petri net covers any of its parent while traversing  upto the root node. If the given sate covers any of the state, then the current state
	 *   token count is adjusted, which is define by stardard petri net algorithm.
	 * @param petriNetState the current marking state of the petri net.
	 * @return the adjusted marking state  for the given  marking state.
	 * 
	 */
	public PetriNetState isGivenStateCoverabableByPreviousStates(PetriNetState petriNetState);
	
	/**
	 * @param petriNetState the current petrinet state
	 * @param petriNetStates  collection of all possible marking states of given petrinet before the given state has been reached.
	 * @return the physical state of the givne marking state i.e if the given state already appeared, then it will return OLD else it will return New.
	 */
	public MarkingState  currentPetriNetState(PetriNetState petriNetState,Set<PetriNetState> petriNetStates);
	
	/**
	 * This method iterates through all the places in the petrinet and collect the token available in an array. This array logically represent the marking state of the petrinet.
	 * @param  places set of places of a given petri net.
	 * @return the array representation of all the available token with the set of places.
	 */
	public int[] getcurrentStateofPetriNet(Set<Place> places);
}
