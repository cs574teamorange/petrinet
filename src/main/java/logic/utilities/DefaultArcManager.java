package logic.utilities;

import java.util.Set;

import logic.model.ActiveArc;

/**
 * Created by anil on Sep 19, 2017
 *
 */
public class DefaultArcManager implements ArcManager {

	@Override
	public void activateArcs(Set<ActiveArc> arcs) {
	for(ActiveArc activeArc: arcs)
		activeArc.activateArc();
	}
}
