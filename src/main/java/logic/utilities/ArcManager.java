package logic.utilities;

import java.util.Set;

import logic.model.ActiveArc;

/**
 * Created by anil on Sep 19, 2017
 * This Utility class is responsible for handlling operations on the Arc and token manipulation on the places.
 *
 */
public interface ArcManager {

	/**
	 *  this method activates the arc, which formally means calling a method of the arc which do token  manipulation on the associated places.
	 * @param arcs collection of Active arc that should be processed when a particular transition at particular state is fired.
	 */
	public void activateArcs(Set<ActiveArc> arcs);

}
