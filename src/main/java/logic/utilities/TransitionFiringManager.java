package logic.utilities;

import java.util.Set;

import logic.model.Firable;

/**
 * Created by anil on Sep 19, 2017
 *  This interface defines the method  that will handle operations for transition. It provides method to 
 *   do real world activities that could be done with the transition in the petrinet.
 * @param <T>  class that implements Firable
 */
public interface TransitionFiringManager<T extends Firable> {

	/**
	 * @param firable the Transition class
	 * @return true if the transition is firable, else return false
	 */
	public boolean isSourceArcFirable(T firable);

	/**
	 * @param firable transition to fire.
	 * this method fire the transtion and performs the necessary task that should be done, once the transition is fired. 
	 */
	public void fireTransition(T firable);

	/**
	 * @param firable set of available transitions
	 * @return this method return subset of the provided set such that each element on this set is firable.
	 */
	public Set<T> getFiriableTransitions(Set<T> firable);
}
