package logic.utilities.exception;

/**
 * Created by anil on Oct 14, 2017
 *
 */
public class InvalidPetriNetStateException extends Throwable {
	
	public InvalidPetriNetStateException(String msg)
	{
		super(msg);
	}
}
