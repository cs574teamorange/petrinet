package logic.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import logic.model.Firable;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;

/**
 * Created by anil on Nov 3, 2017
 *
 */
public class CoverabilityTreeManager {
	
	private Set<PetriNetState> _petriNetStates;
	public CoverabilityTreeManager(Set<PetriNetState> petriNetStates) {
        this._petriNetStates=petriNetStates;
	}
	public  JTree  getDisplayableCoverabilitytree( )
	{
		List<PetriNetState> states=new ArrayList<>(_petriNetStates);
		DefaultMutableTreeNode root= new DefaultMutableTreeNode(Arrays.toString(states.get(0).getTokenCount())+":"+states.get(0).getMarkingState());
		Map< PetriNetState, DefaultMutableTreeNode> mutableTreeNode= new HashMap();
		mutableTreeNode.put(states.get(0),root);
		for(int i=1;i<states.size();i++)
		{
		
			DefaultMutableTreeNode child=new DefaultMutableTreeNode(Arrays.toString(states.get(i).getTokenCount())+":"+ states.get(i).get_transitionFired().getTransitionName()+":"+states.get(i).getMarkingState());
			mutableTreeNode.get(states.get(i).getParentMarking()).add(child);
			mutableTreeNode.put(states.get(i), child);
		}
		return  new JTree(root);
	}

}
