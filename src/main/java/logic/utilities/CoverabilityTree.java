package logic.utilities;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

import logic.model.Firable;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.model.Place;
import logic.model.Tokens;

/**
 * 
 * follows design by contract
 * Created by anil on Sep 19, 2017
 * this class is responsible for returning the coverability tree. This class implements the converability tree drawing algorithm
 *
 */
public class CoverabilityTree {
	private MarkingStateManager _markingStateManger;
	private TransitionFiringManager<Firable> _transitionFiringManager;

	/**
	 * @param markingStateManger a helper class for evaulating the coverability tree algorithm
	 * @param transitionFiringManager the manager class that helps in firing the transition, if firable.
	 * @require not empty markingStateManager and not empty transitionFiringManager
	 */
	public CoverabilityTree(MarkingStateManager markingStateManger,
			TransitionFiringManager<Firable> transitionFiringManager) {
		super();
		this._markingStateManger = markingStateManger;
		this._transitionFiringManager = transitionFiringManager;
	}
	
	/**
	 * @param petriNet the petrinet instance whose coverability tree has to be generated.
	 * @return the collection of all Possible PetriNetState that the petrinet might be, when the sets of firable transition is fired.
	 * @requires a valid petriNet
	 * @ensures  all possible states of petrinet is present in the retrurned set.  
	 */
	public Set<PetriNetState> generateConverabilityTree( PetriNet petriNet)
	{
		Set<PetriNetState> petriNetStates= new TreeSet<>();
		LinkedList<PetriNetState> currentNewStates= new LinkedList<>();
		petriNetStates.add(petriNet.get_petriNetState());
		currentNewStates.add(petriNet.get_petriNetState());
		int initialMarkingId= petriNet.get_petriNetState().get_id();

		while(!currentNewStates.isEmpty()){
			PetriNetState parentMarkingState=currentNewStates.pollFirst();
			petriNet=this.initialisethecurrentPetriNetState(petriNet,parentMarkingState);
			Set<Firable> firiableTransitions= _transitionFiringManager.getFiriableTransitions(petriNet.get_transitions());
			if(firiableTransitions.isEmpty()){
				parentMarkingState.set_markingState(MarkingState.DEADLOCK);
				petriNetStates.add(parentMarkingState);
			}
			else{
				for(Firable firable: firiableTransitions)
				{
					petriNet=this.initialisethecurrentPetriNetState(petriNet,parentMarkingState);
					int[] latestToken=this.fireTransition(firable, petriNet.get_places());
					initialMarkingId++;
					PetriNetState newMarkingState= new PetriNetState(initialMarkingId, latestToken, null,parentMarkingState,firable);
					newMarkingState=_markingStateManger.isGivenStateCoverabableByPreviousStates(newMarkingState);
					newMarkingState.set_markingState(_markingStateManger.currentPetriNetState(newMarkingState, petriNetStates));
					if(newMarkingState.getMarkingState()==MarkingState.NEW)
						currentNewStates.add(newMarkingState);
					petriNetStates.add(newMarkingState);
				}
			}
		}
		return petriNetStates;
	}

	/**
	 * This method fires the firable transition and collects the token count on each possible places and return is as an array of current tokens. 
	 * @param firable a firable transition
	 * @param places collection of petriNet State
	 * @return array of current token value
	 * @requires  a  firable transition, and not empty collection of places
	 * @ensures  collection of token in all possible place after the transition is fired.
	 */
	private int[] fireTransition(Firable firable,Set<Place> places)
	{
		_transitionFiringManager.fireTransition(firable);
		return _markingStateManger.getcurrentStateofPetriNet(places);
	}
	
	/**
	 * this method initialize the given token to the current petrinet instance and return the petrinet instance. 
	 * @param petriNet the  petriNet instance
	 * @param latestNewState current petriNet State
	 * @return the petrinet with initialized set of token provided by the PetriNetstate instance
	 * @requires  not empty latestNewState and the size of token array in latestNewState== size of places in petriNet and each of the token value must be integer>=0
	 * @ensures each place of the petrinet instance is initialize with the corresponding token value of provided by the latestNewState.
	 */
	public PetriNet initialisethecurrentPetriNetState(PetriNet petriNet,PetriNetState latestNewState) {

		Iterator<Place> placeIterator=petriNet.get_places().iterator();
		int stateVariable=0;
		while(placeIterator.hasNext())
		{
			Place currentPlace= placeIterator.next();
			currentPlace.set_tokens(new Tokens(latestNewState.getTokenCount()[stateVariable]));
			stateVariable++;
		}
		return petriNet;
	}
}
