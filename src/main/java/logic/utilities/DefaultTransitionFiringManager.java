package logic.utilities;

import java.util.Set;
import java.util.TreeSet;

import logic.model.Firable;
import logic.model.Transition;

/**
 * Created by anil on Sep 19, 2017
 *
 */
public class DefaultTransitionFiringManager implements TransitionFiringManager<Firable> {

	private ArcManager _arcManager;


	public DefaultTransitionFiringManager(ArcManager arcManager) {
		super();
		this._arcManager = arcManager;
	}

	@Override
	public boolean isSourceArcFirable(Firable transition) {
		// TODO Auto-generated method stub
		return transition.isFirable();
	}

	@Override
	public void fireTransition(Firable transition) {
		transition.fire(_arcManager);
	}

	@Override
	public Set<Firable> getFiriableTransitions(Set<Firable> transions) {
		Set<Firable> firiableTransitions=new TreeSet<>();
		for(Firable transition: transions){
			if(this.isSourceArcFirable(transition))
				firiableTransitions.add(transition);
		}
		return firiableTransitions;
	}
}
