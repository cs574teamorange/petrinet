package logic.model;

import java.awt.Shape;

import logic.componentManager.ComponentType;

/**
 * Created by anil on Sep 19, 2017
 * 
 * The class is an abstract class, that  performs basic functionality that any object in java should do.
 * @param <T>  the object type on which  current instance will be compared  by the comparable interface.
 */
public abstract class AbstractComponent<T extends AbstractComponent<T>> implements Comparable<T>,Drawable {

	/**
	 * Each component is unique either it is places or the petrinet itself. This is used to uniquely identify the components by its id. 
	 */
	private int _id;
	private LocationCoordinates _locationCoordinates;
	
	public AbstractComponent(int id)
	{
		this._id=id;
	}
	public int get_id() {
		return _id;
	}
	public LocationCoordinates get_locationCoordinates() {
		return _locationCoordinates;
	}

	public void set_locationCoordinates(LocationCoordinates _locationCoordinates) {
		this._locationCoordinates = _locationCoordinates;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + _id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		T other = (T) obj;
		if (this._id!= other.get_id())
			return false;
		return true;
	}
	@Override
	public int compareTo(T o) {	
		return this._id- o.get_id();
	}

	@Override
	public String toString() {
		return _id+"";
	}
	
	
	public boolean isNameUsed(String newName)
	{
		if(newName.compareToIgnoreCase(this.getCompoentName())==0)
			return true;
		return false;
		
	}
	protected String getCompoentName() {
		
		return "";
	}
	
	
	public abstract ComponentType getComponentType();
}
