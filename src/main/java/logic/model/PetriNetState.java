package logic.model;

import java.util.Iterator;
import java.util.Set;

import logic.componentManager.ComponentType;


/**
 * Created by anil on Sep 19, 2017
 * This class represent the state of the pertinet. Each petrinet has states and the state changes once a transition is fired. 
 */
public class PetriNetState extends AbstractComponent<PetriNetState> {

	/**
	 * token count for each places represented by an array in which value at index 0 represent the token of  first place in the set places, which is sorted in ascending order by default. 
	 */
	private int[] _tokenCount;

	/**
	 * parent state of the current state. 
	 */
	private PetriNetState _parentMarking;

	/**
	 * phsical name for this markinng state. 
	 */
	private MarkingState _markingState;
	/**
	 * the transition that causes the petri net to be at this state. 
	 */
	private Firable _transitionFired;

	public PetriNetState(int id, Set<Place> places,MarkingState currentStateMarkingState ,PetriNetState parentMarking,Firable trFirable) {
		super(id);
		this._parentMarking=parentMarking;
		this._transitionFired=trFirable;
		_tokenCount= new int[places.size()];
		Iterator<Place> iterator=places.iterator();
		int i=0;
		while(iterator.hasNext()){
			_tokenCount[i]=iterator.next().get_tokens().getIntCount();
			i++;
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + _tokenCount[_tokenCount.length-1];
		return result;
	}
	public PetriNetState(int id, int[] state,MarkingState currentStateMarkingState,PetriNetState parentMarking,Firable firable) {
		super(id);
		this._tokenCount=state;
		this._parentMarking=parentMarking;
		this._markingState=currentStateMarkingState;
		this._transitionFired=firable;
	}
	public MarkingState getMarkingState() {
		return _markingState;
	}
	public void set_markingState(MarkingState _markingState) {
		this._markingState = _markingState;
	}
	public PetriNetState getParentMarking() {
		return _parentMarking;
	}
	public int[] getTokenCount() {
		return _tokenCount;
	}

	public boolean equalsLogical(int[] obj)
	{	for(int i=0;i<this._tokenCount.length;i++)
	{
		if(_tokenCount[i]!=obj[i])
				return false;
	}
		return true;
	}

	public boolean coversThisState(int[] petriNetState)
	{
		for(int i=0;i<this._tokenCount.length;i++)
		{
			if(petriNetState[i]<this._tokenCount[i])
				return false;
		}
		return true;
	}
	public PetriNetState getAdjustedState(PetriNetState parentState)
	{
		for(int i=0;i<this._tokenCount.length;i++)
		{
			if(this._tokenCount[i]>parentState.getTokenCount()[i])
				this._tokenCount[i]=1000;	
		}
		return this;
	}

	public Firable get_transitionFired() {
		return _transitionFired;
	}
	@Override
	public ComponentType getComponentType() {
		// TODO Auto-generated method stub
		return ComponentType.COVERABILITY_GRAPH;
	}

	@Override
	public Shapes getShape() {
		return Shapes.DIRECTED_GRAPH;
	}

	public  boolean validate()
	{
		for(int i=0;i<this.getTokenCount().length;i++)
		{
			if(this.getTokenCount()[i]<0)
				return false;
		}
		return true;
	}

}
