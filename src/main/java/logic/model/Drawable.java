package logic.model;

/**
 * Created by anil on Sep 30, 2017
 * An class that implement this method make it, drawable in the UI.
 */
public interface Drawable {
	
	public Shapes getShape();
	public  LocationCoordinates get_locationCoordinates();

}
