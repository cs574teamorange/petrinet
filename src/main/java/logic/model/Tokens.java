package logic.model;

/**
 * Created by anil on Sep 19, 2017
 * This class represents  actual value that any place has. Token value get updated when the transition is fired.
 */
public class Tokens extends AbstractMetaData {
	public Tokens(int initialToken){
		super(initialToken);
	}
	public Tokens(Tokens other) { this(other.getIntCount()); }
}
