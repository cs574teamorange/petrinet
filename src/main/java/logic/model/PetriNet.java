package logic.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import logic.componentManager.ComponentType;
import logic.utilities.CoverabilityTree;
import logic.utilities.DefaultArcManager;
import logic.utilities.DefaultMarkingStateManager;
import logic.utilities.DefaultTransitionFiringManager;
import logic.utilities.MarkingStateManager;
import logic.utilities.TransitionFiringManager;


/**
 * Created by anil on Sep 19, 2017
 * This class  represent actual petri net in real life. It contain set of places, transition and it has initial state.
 */
public class PetriNet extends AbstractComponent<PetriNet> {

	private Set<Place> _places;
	private Set<Firable> _transitions;
	private PetriNetState _petriNetState;
	public PetriNet(int id,Set<Place> places, Set<Firable> transitions) {
		super(id);
		this._places=places;
		this._transitions=transitions;
	}

	public PetriNet(PetriNet other) {
		super(other.get_id());
		PetriNetState otherState = other.get_petriNetState();
	
		_places = new HashSet<>(other.get_places().size());
		for (Place place : other.get_places()) {
			_places.add(new Place(place));
		}

		_transitions = new HashSet<>(other.get_transitions().size());
		for (Firable transition : other.get_transitions()) {
			Transition created=new Transition((Transition) transition,_places);
			_transitions.add(created);
			
				
		}
		TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
		int[] currentMarkingState=markingStateManager.getcurrentStateofPetriNet(this.get_places());
		PetriNetState initialState= new PetriNetState(1,currentMarkingState,MarkingState.NEW,null,null);

			LocationCoordinates otherCoords = other.get_locationCoordinates();
		if (otherCoords != null) {
			set_locationCoordinates(new LocationCoordinates(otherCoords));
		}
	}

	public void set_petriNetState(PetriNetState _petriNetState) {
		this._petriNetState = _petriNetState;
	}
	public Set<Place> get_places() {
		return _places;
	}

	public Set<Firable> get_transitions() {
		return _transitions;
	}

	public PetriNetState get_petriNetState() {
		return _petriNetState;
	}

	@Override
	public ComponentType getComponentType() {
		// TODO Auto-generated method stub
		return ComponentType.PETRINET;
	}
	@Override
	public Shapes getShape() {
		// TODO Auto-generated method stub
		return Shapes.DEFAULT;
	}
	/**
	 * Pre-conditon check method to insure if the  petrNet is valid or not
	 * @return if the petri net is  valid then return true else return false;
	 */
	public boolean validate()
	{
		if(this._places.isEmpty() || this._transitions.isEmpty())
			return false;
		else
		{
			Iterator<Firable> itr= this._transitions.iterator();
			while(itr.hasNext())
			{
				Transition transition= (Transition)itr.next();
				if(transition.getSourceArcs().isEmpty()||transition.getDestinationArcs().isEmpty())
					return false;
			}
		}
		return true;
	}
}
