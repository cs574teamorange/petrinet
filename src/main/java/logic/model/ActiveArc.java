package logic.model;


/**
 * Created by anil on Sep 19, 2017
 *
 * This interface  defines methods that make the Arc active. An arc that implements this interface 
 * is activable.
 *
 */
public interface ActiveArc {
	
	/**
	 * This method active a arc:  This method  perform  all the operations one the transition has happened: updating token count on associated places. 
	 */
	public void activateArc();

}
