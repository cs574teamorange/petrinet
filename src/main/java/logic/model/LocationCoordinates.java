package logic.model;

public class LocationCoordinates {
	
	private int _xPoint;
	private int _yPoint;
	
	
	public LocationCoordinates(int xPoint, int yPoint) {
		this._xPoint=xPoint;
		this._yPoint=yPoint;
	}

	public LocationCoordinates(LocationCoordinates other) {
		this(other.getxPoint(), other.getyPoint());
	}

	public int getxPoint() {
		return _xPoint;
	}


	public void setxPoint(int xPoint) {
		this._xPoint = xPoint;
	}


	public int getyPoint() {
		return _yPoint;
	}


	public void setyPoint(int yPoint) {
		this._yPoint = yPoint;
	}
}
