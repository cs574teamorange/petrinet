package logic.model;


/**
 * Created by anil on Sep 19, 2017
 * This enum class represent various state that any petri net could be, at particular state of time.
 */
public enum MarkingState {
	NEW,OLD,DEADLOCK,VISITED;

}
