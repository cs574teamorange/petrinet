package logic.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import logic.componentManager.ComponentType;

/**
 * Created by anil on Sep 19, 2017
 *
 */
public class Transition extends AbstractTransition<Transition>{
	public Transition(int id, Set<SourceArc> sourceArcs, Set<DestinationArc> destinationArcs,String name) {
		super(id, sourceArcs, destinationArcs,name);
	}

	public Transition(Transition other,Set<Place> places) {
		this(other.get_id(), new HashSet<>(), new HashSet<>(), other.getTransitionName());
		_sourceArcs = copySourceArcs(other.getSourceArcs(),places);
		_destinationArcs = copyDestArcs(other.getDestinationArcs(),places);
		if(other.get_locationCoordinates()!=null)
		set_locationCoordinates(new LocationCoordinates(other.get_locationCoordinates()));
	}

	private Set<SourceArc> copySourceArcs(Set<SourceArc> arcs,Set<Place> places) {
		Set<SourceArc> newArcs = new HashSet<>(arcs.size());
		for (SourceArc arc : arcs) {
			Place newPlace=null;
			Iterator<Place> iterator= places.iterator();
			while(iterator.hasNext())
			{
				newPlace= iterator.next();
				if(arc.get_place().get_id()==newPlace.get_id())
		        break;		
			}
			
			newArcs.add(new SourceArc(arc,newPlace));
		}
		return newArcs;
	}

	private Set<DestinationArc> copyDestArcs(Set<DestinationArc> arcs,Set<Place> places) {
		Set<DestinationArc> newArcs = new HashSet<>(arcs.size());
		for (DestinationArc arc : arcs) {
			Place newPlace=null;
			Iterator<Place> iterator= places.iterator();
			while(iterator.hasNext())
			{
				newPlace= iterator.next();
				if(arc.get_place().get_id()==newPlace.get_id())
		        break;		
			}
			newArcs.add(new DestinationArc(arc,newPlace));
		}
		return newArcs;
	}

	@Override
	public String getTransitionName() {
		// TODO Auto-generated method stub
		return _name;
	}
	
	public void setTransitionName(String name) {
		this._name = name;
	}

	@Override
	public ComponentType getComponentType() {
		// TODO Auto-generated method stub
		return ComponentType.TRANSITION;
	}

	@Override
	public Shapes getShape() {
		// TODO Auto-generated method stub
		return Shapes.RECTANGLE;
	}
	

}
