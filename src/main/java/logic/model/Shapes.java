package logic.model;

/**
 * Created by anil on Sep 30, 2017
 * contains all possible shapes  in the petri net
 */
public enum Shapes {
CIRCLE, SOURCE_ARROW,DESTINATION_ARROW, RECTANGLE,DEFAULT,DIRECTED_GRAPH;

}
