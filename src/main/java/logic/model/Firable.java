package logic.model;

import java.util.Set;

import logic.utilities.ArcManager;


/**
 * Created by anil on Sep 19, 2017
 * This interface hold  methods that any firable component must have. Implementing this interface make any component firiable.
 *
 */
public interface Firable {

	/** This method checks if the component at a particular state is firible or not.
	 * @return true if the component is firible, if not return false.
	 */
	public boolean isFirable();
	
	/**
	 *  This method fires the firable component using the ArcManager, to update the  tokens in associated  places.
	 * @param arcManager the handller that manages the basic operation on arcs when the component is fired. There is default implementation of  this manager.
	 */
	public void fire(ArcManager arcManager);
	
	/**
	 * the method return the actual name of transition
	 * @return displayable name of the transition
	 * 
	 */
	public String getTransitionName();

}
