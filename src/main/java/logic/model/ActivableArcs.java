package logic.model;

/**
 * Created by anil on Sep 19, 2017
 * This abstract class represents basic elements of an arc. Since Arc is also a component, hence it extends abstract component.
 * Arc, no matter source or destination, is associated with a place and each arc has a  value. A transition is firiable if all the arcs class that extends this class is 
 * active. 
 *
 * @param <T> a comparable entity that could be comparable with this class.
 */
public abstract class ActivableArcs<T extends ActivableArcs<T>> extends AbstractComponent<T> {
	/**
	 * place represent one end of the arc. It could act as a source or the destination based on the nature of the arc. 
	 */
	protected Place _place;
	protected Transition _transition;
	protected AbstractMetaData _arcValue;
	

	/**
	 * 
	 *  The value of the arc. Again this value could acts differently depending on the nature of arc: if the arc is source arc, then associated place have at least _arcValue amount of token to make this arc firable.
	 * But if the arc is Destination arc, the transition firing would result in addition of _arcValue of token in the associative place. 
	
	 * @param id the unique id of the arc
	 * @param place the associated place attach with the arc
	 * @param arcValue the  value for the arc
	 */
	public ActivableArcs(int id, Place place, int arcValue)
	{
		super(id);
		this._place=place;
		this._arcValue=new ArcValue(arcValue);
	}
	public ActivableArcs(int id)
	{
		super(id);
	}
	
	/**
	 * This method test if the arc is active or not. Active in the sense that transition could happen for this arc. 
	 * @return true if the arc is active if not, return false.
	 */
	public boolean isArcActive()
	{
		return this._place.get_tokens().getIntCount()>=this._arcValue.getIntCount()?true:false;
	}

	public Place get_place() {
		return _place;
	}
    public Transition get_transtion() { return _transition; }
	public int get_arcValue() {
		return _arcValue.getIntCount();
	}
	
	public AbstractMetaData get_arcValueObj() {
		return _arcValue;
	}
	public void set_arcValue(AbstractMetaData _arcValue) {
		this._arcValue = _arcValue;
	}
	public void set_place(Place _place) {
		this._place = _place;
	}
	public void set_transition(Transition transition) { _transition = transition; }
	
	
}
