package logic.model;

import java.util.Iterator;
import java.util.Set;

import io.PlaceSerializer;
import logic.componentManager.ComponentType;

/**
 * Created by anil on Sep 19, 2017
 * This class represent the destination arc, those  going toward places from transitions..
 * This Arc is  Active: that means it something when the corresponding transition is fired. Hence it implements the interface
 * ActiveArc. 
 */
public class DestinationArc extends AbstractComponent<DestinationArc> implements ActiveArc {

	protected Place _place;
	protected Transition _transition;
	protected AbstractMetaData _arcValue;

	public DestinationArc(int id)
	{
		super(id);
	}

	public DestinationArc(int id, Place place, int arcValue) {
		super(id);
		this._place=place;
		this._arcValue= new ArcValue(arcValue);
	}

	public DestinationArc(DestinationArc other,Place place) {
		
		
		this(other.get_id(), place, other.get_arcValue());
		_transition = other.get_transition();

		LocationCoordinates otherCoords = other.get_locationCoordinates();
		if (otherCoords != null) {
			set_locationCoordinates(new LocationCoordinates(otherCoords));
		}
	}

	@Override
	public void activateArc() {
		int tokenNewValue=_place.get_tokens().getIntCount()+this._arcValue.getIntCount();
		_place.set_tokens(new Tokens(tokenNewValue> 900? 1000: tokenNewValue));
	}

	public Place get_place() {
		return _place;
	}
	public Transition get_transition() { return _transition; }
	public int get_arcValue() {
		return _arcValue.getIntCount();
	}

	@Override
	public ComponentType getComponentType() {
		// TODO Auto-generated method stub
		return ComponentType.DESTINATION_ARROW;
	}

	public void set_place(Place place) {
		this._place = place;
	}
	public void set_transition(Transition transition) { _transition = transition; }
	public void set_arcValue(AbstractMetaData _arcValue) {
		this._arcValue = _arcValue;
	}
	
	public AbstractMetaData get_arcValueObj() {
		return _arcValue;
	}

	@Override
	public Shapes getShape() {
		// TODO Auto-generated method stub
		return Shapes.DESTINATION_ARROW;
	}
	public boolean isDuplicateComponenent(Place usedPlace, Transition usedTransition) throws NullPointerException{
		{
			if(this._place.equals(usedPlace) &&this._transition.equals(usedTransition))
				return true;
			return false;
		}
	}
	

}
