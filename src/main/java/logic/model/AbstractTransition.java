package logic.model;

import java.util.HashSet;
import java.util.Set;

import logic.utilities.ArcManager;


/**
 * Created by anil on Sep 19, 2017
 * 
 * This class contain basic operations that any transition, in general will have. It implements an iterface called Firiable that makes the transition firiable. A transition basically
 * have collection of input arc, called source arc, and output arc called Destination arc. Each of these arcs are independent entity and hence transition firing is treated differently to 
 * respective places associated to these arcs.
 *
 * @param <T> class that is comparable to this class
 */
public abstract class AbstractTransition<T extends AbstractTransition<T>> extends AbstractComponent<T> implements Firable {

	/**
	 * A  Transition consist of  incoming arcs i.e arc from places to transitions. These arc are called Source Arc. There may to many source arc, so represented by set of them. 
	 */
	protected Set<SourceArc> _sourceArcs;
	protected String _name;
	
	/**
	 *A transition consist of outgoing arcs i.e arc from transition to places. These arc are called Destination Arcs. There may be many destination arc, so represented by set of them. 
	 */
	protected Set<DestinationArc> _destinationArcs;
	protected AbstractTransition(int id, Set<SourceArc> sourceArcs, Set<DestinationArc> destinationArcs,String name) {
		super(id);
		this._sourceArcs = sourceArcs;
		this._destinationArcs = destinationArcs;
		this._name=name;
	}

	public Set<SourceArc> getSourceArcs() {
		return _sourceArcs;
	}

	public void setSourceArcs(Set<SourceArc> sourceArcs) {
		this._sourceArcs = sourceArcs;
	}

	public Set<DestinationArc> getDestinationArcs() {
		return _destinationArcs;
	}

	public void setDestinationArcs(Set<DestinationArc> destinationArcs) {
		this._destinationArcs = destinationArcs;
	}

	@Override
	public boolean isFirable() {
		if(_sourceArcs.isEmpty()||_destinationArcs.isEmpty())
			return false;
		for(SourceArc sourceArc:_sourceArcs)
		{
			if(!sourceArc.isArcActive())
				return false;
		}
		return true;
	}

	@Override
	public void fire(ArcManager arcManager) {
		Set<ActiveArc> firiblearcs=new HashSet<>();
		firiblearcs.addAll(_sourceArcs);
		firiblearcs.addAll(_destinationArcs);
		arcManager.activateArcs(firiblearcs);
	}
	
	public boolean isTransitionDead()
	{
		return this._destinationArcs.isEmpty();
		
	}
	public  String getCompoentName() {
		
		return this._name;
	}
}
