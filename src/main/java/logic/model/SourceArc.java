package logic.model;

import logic.componentManager.ComponentType;

/**
 * Created by anil on Sep 19, 2017
 * This class represent the sourceArc that moves to transition from the place. This arc is Activable because its  state determine either the 
 * transition associated with this arc is actually firiable or not. 
 *
 */
public class SourceArc extends ActivableArcs<SourceArc> implements  ActiveArc {

	public SourceArc(int id,Place place, int arcValue) {
		super(id,place,arcValue);
	}
	public SourceArc(int id)
	{
		super(id);
	}

	public SourceArc(SourceArc other,Place place) {
		this(other.get_id(), place, other.get_arcValue());
		set_transition(other.get_transtion());

		LocationCoordinates otherCoords = other.get_locationCoordinates();
		if (otherCoords != null) {
			set_locationCoordinates(new LocationCoordinates(otherCoords));
		}
	}

	@Override
	public void activateArc()
	{
		int tokenNewValue=_place.get_tokens().getIntCount()-this._arcValue.getIntCount();
		if(tokenNewValue <0)
		{

		}
		else
			_place.set_tokens(new Tokens(tokenNewValue> 900? 1000: tokenNewValue));

	}
	@Override
	public ComponentType getComponentType() {
		// TODO Auto-generated method stub
		return ComponentType.SOURCE_ARROW;
	}
	@Override
	public Shapes getShape() {
		// TODO Auto-generated method stub
		return Shapes.SOURCE_ARROW;
	}
	public boolean isDuplicateComponenent(Place usedPlace, Transition usedTransition) throws NullPointerException{
		{
			if(this._place.equals(usedPlace) &&this._transition.equals(usedTransition))
				return true;
			return false;
		}
	}
	
}
