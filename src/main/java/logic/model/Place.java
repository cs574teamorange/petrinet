package logic.model;

import logic.componentManager.ComponentType;

/**
 * Created by anil on Sep 19, 2017
 * This class represent  actual place in the petri net concept. Places has token that will be updated.
 */
public class Place extends AbstractComponent<Place>{

	private String _name;
	private AbstractMetaData _tokens;

	public Place(int id, Tokens tokens){
		this(id, tokens, "");
	}

	public Place(Place other) {
		this(other.get_id(), new Tokens((Tokens) other.get_tokens()), other.getPlaceName());
		if(other.get_locationCoordinates()!=null)
		set_locationCoordinates(new LocationCoordinates(other.get_locationCoordinates()));
	}

	public Place(int id, AbstractMetaData tokens, String name){
		super(id);
		_tokens=tokens;
		_name = name;
	}

	public AbstractMetaData get_tokens() {
		return _tokens;
	}

	public void set_tokens(AbstractMetaData _tokens) {
		this._tokens = _tokens;
	}

	public String getPlaceName() {
		return _name;
	}
	
	public void setPlaceName(String name) {
		this._name = name;
	}

	@Override
	public ComponentType getComponentType() {
		// TODO Auto-generated method stub
		return ComponentType.PLACE;
	}

	@Override
	public Shapes getShape() {
		// TODO Auto-generated method stub
		return Shapes.CIRCLE;
	}

	protected String getCompoentName() {
		
		return this._name;

	}
}
