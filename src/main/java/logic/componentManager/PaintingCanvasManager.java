package logic.componentManager;

import ui.PaintingCanvas;

public class PaintingCanvasManager {

	private  static PaintingCanvas _painting_canvas;

	private static PaintingCanvasManager _painting_canvas_manager;
	private PaintingCanvasManager(PaintingCanvas paintingCanvas){
		_painting_canvas = paintingCanvas;
	}
	public static PaintingCanvasManager getCurrentInstance()
	{
		if(_painting_canvas_manager==null)
			_painting_canvas_manager=new PaintingCanvasManager(new PaintingCanvas());
		return _painting_canvas_manager;	
	}
	
	public PaintingCanvas get_component() {
		return _painting_canvas;
	}
}

