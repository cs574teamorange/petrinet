package logic.componentManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import logic.model.AbstractComponent;
import logic.model.AbstractMetaData;
import logic.model.LocationCoordinates;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Transition;
public class SourceArcManager extends AbstractComponentManager<SourceArc> {

	private Set<SourceArc> _sources; 
	int _indexCount=0;
	private static SourceArcManager _sourceArcManager;
	private  SourceArcManager(Set<SourceArc> sources, int indexCount) {
		this._sources=sources;
		this._indexCount=indexCount;
	}
	public static SourceArcManager getCurrentInstance()
	{
		if(_sourceArcManager==null)
			_sourceArcManager= new SourceArcManager(new TreeSet<SourceArc>(), 0);
		return _sourceArcManager;
	}
	@Override
	public SourceArc getComponent() {
		_indexCount++;
		SourceArc sourceArc= new SourceArc(_indexCount);
		_sources.add(sourceArc);
		return sourceArc;
	}
	@Override
	void attachComponentToItsParentComponent(SourceArc component, int parentComponentId) {
		TransitionManager.getCurrentInstance().findComponent(parentComponentId).getSourceArcs().add(component);
		component.set_transition(TransitionManager.getCurrentInstance().findComponent(parentComponentId));
	}
	@Override
	void removeComponent(SourceArc component, int parnetComponentId) {
		this._sources.remove(component);
		TransitionManager.getCurrentInstance().findComponent(parnetComponentId).getSourceArcs().remove(component);
	}
	@Override
	public  Set<SourceArc> getComponents() {
		return _sources;
	}
	@Override
	SourceArc updateComponentMetaData(SourceArc component, LocationCoordinates locationCoordinates,
			AbstractMetaData metaData, AbstractComponent attachIt) {
		component.set_locationCoordinates(locationCoordinates);
		component.set_arcValue(metaData);
		component.set_place((Place) attachIt);
		return component;
	}
	@Override
	SourceArc copyComponent(SourceArc component) {
		SourceArc sourceArc= new SourceArc(component.get_id());
		sourceArc.set_locationCoordinates(component.get_locationCoordinates());
		sourceArc.set_arcValue(sourceArc.get_arcValueObj());
		_sources.add(sourceArc);
		return sourceArc;
	}
	@Override
	protected void updateIndexCount(int currentVal) {
		this._indexCount=currentVal;
	}

	public void removeArcsWithGivenPlace(Place place)
	{
		List<SourceArc> removalbleArcs= new ArrayList<>();
		Iterator<SourceArc> sourcesArcs= this._sources.iterator();
		while(sourcesArcs.hasNext())
		{
			SourceArc sourceArc= sourcesArcs.next();
			if(sourceArc.get_place().equals(place))
				removalbleArcs.add(sourceArc);
		}
		//remove all removable Arcs
		for(SourceArc sourceArc: removalbleArcs)
			this.removeComponent(sourceArc, sourceArc.get_transtion().get_id());
	}

	public boolean isComponentDuplicate(Place usedPlace, Transition usedTransition)
	{
		try
		{
			Iterator<SourceArc> sourcesArcs= this._sources.iterator();
			while(sourcesArcs.hasNext())
			{
				SourceArc sourceArc= sourcesArcs.next();
				if(sourceArc.isDuplicateComponenent(usedPlace,usedTransition))
					return true;
			}
			return false;
		}
		catch(NullPointerException exp)
		{
			return false;
		}
	}
}
