package logic.componentManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import logic.model.AbstractComponent;
import logic.model.AbstractMetaData;
import logic.model.ArcValue;
import logic.model.DestinationArc;
import logic.model.LocationCoordinates;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Transition;

public class DestinationArcManager extends AbstractComponentManager<DestinationArc> {

	private  Set<DestinationArc> _destinationArcs;
	private  int _indexCounter;
	private static DestinationArcManager _destinationArcManager;

	private DestinationArcManager(Set<DestinationArc> destinationArcs, int indexCounter)
	{
		this._destinationArcs=destinationArcs;
		this._indexCounter=indexCounter;
	}

	public static DestinationArcManager  getCurrentInstance()
	{
		if(_destinationArcManager==null)
			_destinationArcManager=new DestinationArcManager(new TreeSet<DestinationArc>(),0);
		return _destinationArcManager;
	}

	@Override
	DestinationArc getComponent() {
		_indexCounter++;
		DestinationArc destinationArc= new DestinationArc(_indexCounter);
		_destinationArcs.add(destinationArc);
		return destinationArc;
	}
	@Override
	void attachComponentToItsParentComponent(DestinationArc component,int idParent) {
		TransitionManager.getCurrentInstance().findComponent(idParent).getDestinationArcs().add(component);
		component.set_transition(TransitionManager.getCurrentInstance().findComponent(idParent));
	}

	@Override
	void removeComponent(DestinationArc component,int idParent) {
		this._destinationArcs.remove(component);
		TransitionManager.getCurrentInstance().findComponent(idParent).getDestinationArcs().remove(component);
	}

	@Override
	public Set<DestinationArc> getComponents() {
		// TODO Auto-generated method stub
		return _destinationArcs;
	}

	@Override
	DestinationArc updateComponentMetaData(DestinationArc component, LocationCoordinates locationCoordinates,
			AbstractMetaData metaData, AbstractComponent attachIt) {
		component.set_locationCoordinates(locationCoordinates);
		component.set_arcValue(metaData);
		component.set_place((Place) attachIt);
		return component;
	}

	@Override
	DestinationArc copyComponent(DestinationArc component) {
		_indexCounter++;
		DestinationArc destinationArc= new DestinationArc(_indexCounter);
		destinationArc.set_locationCoordinates(component.get_locationCoordinates());
		LocationCoordinates locationCoordinates= new LocationCoordinates(component.get_locationCoordinates().getxPoint(), component.get_locationCoordinates().getyPoint());
		ArcValue arcValue= new ArcValue(component.get_arcValueObj().getIntCount());
		destinationArc.set_arcValue(arcValue);
		destinationArc.set_locationCoordinates(locationCoordinates);
		_destinationArcs.add(destinationArc);
		return destinationArc;
	}
	
	
	public void removeArcsWithGivenPlace(Place place)
	{
		List<DestinationArc> removalbleArcs= new ArrayList<>();
		Iterator<DestinationArc> sourcesArcs= this._destinationArcs.iterator();
		while(sourcesArcs.hasNext())
		{
			DestinationArc sourceArc= sourcesArcs.next();
			if(sourceArc.get_place().equals(place))
				removalbleArcs.add(sourceArc);
		}
		//remove all removable Arcs

		for(DestinationArc sourceArc: removalbleArcs)
			this.removeComponent(sourceArc, sourceArc.get_transition().get_id());
	}
	@Override
	public void updateIndexCount(int currentVal) {
		this._indexCounter=currentVal;

	}
	public boolean isComponentDuplicate(Place usedPlace, Transition usedTransition)
	{
		try
		{
		Iterator<DestinationArc> destinationArcs= this._destinationArcs.iterator();
		while(destinationArcs.hasNext())
		{
			DestinationArc destinationArc=destinationArcs .next();
			if(destinationArc.isDuplicateComponenent(usedPlace, usedTransition))
				return true;
		}
		return false;
		}
		catch(NullPointerException exp)
		{
			return false;
		}
	}
}
