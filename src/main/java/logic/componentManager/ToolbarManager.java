package logic.componentManager;

import ui.Toolbar;

public class ToolbarManager {

	private  static Toolbar _toolbar;

	private static ToolbarManager _toolbar_manager = null;
	private ToolbarManager(Toolbar toolbar){
		_toolbar=toolbar;
		_toolbar.create_toolbar();
	}
	public static ToolbarManager getCurrentInstance()
	{
		if(_toolbar_manager==null)
			_toolbar_manager=new ToolbarManager(new Toolbar());
		return _toolbar_manager;	
	}
	
	public Toolbar get_component() {
		return _toolbar;
	}
}
