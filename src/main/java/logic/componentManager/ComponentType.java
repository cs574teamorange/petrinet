package logic.componentManager;

/**
 * Created by anil on Sep 30, 2017
 * contains all types of component.
 */
public enum ComponentType {
PETRINET,PLACE,TRANSITION, SOURCE_ARROW, DESTINATION_ARROW,COVERABILITY_GRAPH;
}
