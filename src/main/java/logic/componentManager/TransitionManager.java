
package logic.componentManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import logic.model.AbstractComponent;
import logic.model.AbstractMetaData;
import logic.model.DestinationArc;
import logic.model.Firable;
import logic.model.LocationCoordinates;
import logic.model.SourceArc;
import logic.model.Transition;

public class TransitionManager extends AbstractComponentManager<Transition> {

	private  Set<Transition> _transitions;
	private  int _indexCounter;
	private static TransitionManager _transitionManager;

	private TransitionManager(Set<Transition> transitions) {
		this._transitions=transitions;
	}
	public static TransitionManager getCurrentInstance()
	{
		if(_transitionManager==null)
			_transitionManager=new TransitionManager(new TreeSet<Transition>()); 
		return _transitionManager;
	}
	@Override
	Transition getComponent() {
		_indexCounter++;
		Transition firable= new Transition(_indexCounter, new TreeSet<SourceArc>(),new TreeSet<DestinationArc>(),"T"+_indexCounter);
		_transitions.add(firable);
		return (Transition) firable;
	}

	@Override
	void attachComponentToItsParentComponent(Transition component, int parentComponentId) {
		PetriNetManager.initialize().addTransition(component);
	}

	@Override
	void removeComponent(Transition component, int parnetComponentId) {
		PetriNetManager.initialize().removeTransition(component);
	}
	@Override
	public Set<Transition> getComponents() {
	return _transitions;
	}
	@Override
	Transition updateComponentMetaData(Transition component, LocationCoordinates locationCoordinates,
			AbstractMetaData metaData, AbstractComponent abstractComponent) {
		component.set_locationCoordinates(locationCoordinates);
		return component;
	}
	@Override
	Transition copyComponent(Transition component) {
		_indexCounter++;
		Transition transition= new Transition(_indexCounter, new TreeSet<SourceArc>(), new TreeSet<DestinationArc>(),"T"+_indexCounter);
		transition.set_locationCoordinates(component.get_locationCoordinates());
	//	PetriNetManager.getCurrentInstance().addTransition(transition);
		return transition;
	}
	@Override
	protected void updateIndexCount(int currentVal) {
		this._indexCounter=currentVal;
		
	}
}
