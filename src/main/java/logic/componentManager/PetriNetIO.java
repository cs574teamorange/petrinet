package logic.componentManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import io.PetriNetSerializer;
import io.PetriSerializerException;
import logic.model.PetriNet;

/**
 * Created by anil on Oct 10, 2017
 * This class acts as an adaptor betwee eddie's io code and my code. Iker should  only use this class to load and unload  the petri net. 
 */
public class PetriNetIO {
	
	
	public PetriNetIO()
	{
		
	}
	public static PetriNet readPetriNet(String xmlFileLocation) throws ClassNotFoundException, IOException,
			ParserConfigurationException, PetriSerializerException, SAXException {
		File xmlFile = new File(xmlFileLocation);
		PetriNetSerializer serializer = new PetriNetSerializer();
		FileInputStream fin = new FileInputStream(xmlFile);
		serializer.readObject(fin);
		PetriNet pnRead = serializer.get_petrinet();
		fin.close();
		return pnRead;
	}
	
	public static void initializePetriNet( PetriNet petriNet)
	{
		PetriNetManager.initialize().setComponent(petriNet);
	}
	
	public static void writePetriNet(String xmlFileLocation, PetriNet petriNet) throws IOException, TransformerException,
			PetriSerializerException, ParserConfigurationException {
		FileOutputStream fout = new FileOutputStream(xmlFileLocation);
		PetriNetSerializer serializer = new PetriNetSerializer(petriNet);
		serializer.writeObject(fout);
		fout.close();
	}
	
}
