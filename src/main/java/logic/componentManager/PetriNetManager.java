package logic.componentManager;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import logic.model.DestinationArc;
import logic.model.Firable;
import logic.model.PetriNet;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Transition;

/**
 * Created by anil on Sep 29, 2017
 *
 */
public class PetriNetManager{

	private static PetriNet _petriNet;

	private static PetriNetManager _petriNetManager;
	private PetriNetManager(PetriNet petriNet){
		this._petriNet=petriNet;
	}
	public static PetriNetManager initialize()
	{
		if(_petriNetManager==null)
			_petriNetManager=new PetriNetManager(new PetriNet(1, new TreeSet<Place>(),new TreeSet<Firable>()));
		return _petriNetManager;	
	}
	void addPlace(Place place)
	{
		_petriNet.get_places().add(place);
	}
	void removePlace(Place place)
	{
		_petriNet.get_places().remove(place);
	}
	void addTransition(Firable firable)
	{
		_petriNet.get_transitions().add(firable);
	}

	void removeTransition( Firable firable)
	{
		_petriNet.get_transitions().remove(firable);
		
	}


	public PetriNet getComponent() {
		return _petriNet;
	}

	public void setComponent(PetriNet petriNet)
	{
		this.clear();
		PlaceManager placeManager=PlaceManager.getCurrentInstance();
		DestinationArcManager destinationArcManager= DestinationArcManager.getCurrentInstance();
		SourceArcManager sourceArcManager= SourceArcManager.getCurrentInstance();
		TransitionManager transitionManager= TransitionManager.getCurrentInstance();
		_petriNet=petriNet;
		Set<Transition> transitions= new  TreeSet<>();
		Set<SourceArc> sourceArcs= new TreeSet<>();
		Set<DestinationArc> destinationArc= new TreeSet<>();

		for(Firable firable: _petriNet.get_transitions())
		{
			Transition transition=(Transition) firable; 
			transitions.add(transition);
			sourceArcs.addAll(transition.getSourceArcs());
			destinationArc.addAll(transition.getDestinationArcs());
		}
		transitionManager.setComponents(transitions);
		destinationArcManager.setComponents(destinationArc);
		sourceArcManager.setComponents(sourceArcs);
		placeManager.setComponents(_petriNet.get_places());
	}
	
	public void clear()
	{
		
		this._petriNet = new PetriNet(1, new TreeSet<Place>(),new TreeSet<Firable>());
		PlaceManager placeManager=PlaceManager.getCurrentInstance();
		DestinationArcManager destinationArcManager= DestinationArcManager.getCurrentInstance();
		SourceArcManager sourceArcManager= SourceArcManager.getCurrentInstance();
		TransitionManager transitionManager= TransitionManager.getCurrentInstance();
		placeManager.clear();
		destinationArcManager.clear();
		sourceArcManager.clear();
		transitionManager.clear();
	}

	public PetriNet clone(PetriNet petriNet) {
		return new PetriNet(petriNet);
	}
}
