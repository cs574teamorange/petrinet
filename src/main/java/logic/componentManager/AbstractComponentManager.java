package logic.componentManager;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import logic.model.AbstractComponent;
import logic.model.AbstractMetaData;
import logic.model.LocationCoordinates;
import logic.model.Transition;

/**
 * Created by anil on Sep 30, 2017
 * the specification defines basic methods that any component manager must have; It provides clear method for instantianiting components, editing and removing the component
 * and handle all subsequent processing required.
 *
 * @param <T> Component type 
 */
public abstract class AbstractComponentManager<T extends AbstractComponent<T>> {

	abstract T getComponent();
	abstract void attachComponentToItsParentComponent(T component, int parentComponentId);
	abstract T copyComponent(T component);
	abstract void removeComponent(T component, int parnetComponentId);
	abstract T updateComponentMetaData(T component, LocationCoordinates locationCoordinates, AbstractMetaData metaData, AbstractComponent attachIt);
	public  T findComponent(int id)
	{
		for(T component: this.getComponents())
		{
			if(component.get_id()==id)
				return component;
		}
		return null;
	}
	
	protected int getLastComponentId()
	{
		Iterator itr = this.getComponents().iterator();
		Object lastElement =itr.next();
		while(itr.hasNext()) {
			lastElement = itr.next();
		}
		
		return ((AbstractComponent)lastElement).get_id();
	}
	public void setComponents(Set<T> components)
	{
		this.getComponents().addAll(components);
		this.updateIndexCount(this.getLastComponentId());
	}
	
	protected abstract void updateIndexCount(int currentVal);
	public abstract Set<T> getComponents();

	public void clear()
	{
		this.updateIndexCount(0);
		this.getComponents().clear();
	}
	
	public boolean isNameUsed(String name)
	{
		Iterator<T> itr = this.getComponents().iterator();
		while(itr.hasNext()) {
		AbstractComponent lastElement = itr.next();
		if(lastElement.isNameUsed(name))
			return true;
		}	
		return false;
	}
	

}
