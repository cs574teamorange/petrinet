package logic.componentManager;

import logic.model.AbstractComponent;
import logic.model.AbstractMetaData;
import logic.model.LocationCoordinates;
import logic.model.SourceArc;

/**
 * Created by anil on Sep 30, 2017
 * this class is responsible for handlling all the ui operation regarding component creations.
 */
public class ComponentManagerFactory {

	
	public ComponentManagerFactory()
	{
		
	}
	public  static AbstractComponentManager getComponentManager(ComponentType componentType)
	{
		
		   if(componentType.equals(ComponentType.TRANSITION))
			{
				return TransitionManager.getCurrentInstance();
			}
			else if(componentType.equals(ComponentType.SOURCE_ARROW))
			{
				return SourceArcManager.getCurrentInstance();

			}
			else if(componentType.equals(ComponentType.DESTINATION_ARROW))
			{
				return DestinationArcManager.getCurrentInstance();
			}	 
				return PlaceManager.getCurrentInstance();
			 
	}

	/**
	 * @param componentType the type of the component required.
	 * This class instantiate a component required, assign an id to it, and return the component
	 * @return an instance of the component requested.
	 */
	public static AbstractComponent getComponent(ComponentType componentType)
	{
		return getComponentManager(componentType).getComponent();
	}

	/**
	 * @param component the actual component to be added.
	 * @param parentId id of the component on which this component should be attached.
	 * This  adds the component to the corresponding parent.
	 */
	public static void  addComponent(AbstractComponent component, Integer parentId)
	{
		if(parentId==null)
			parentId=new Integer(0);
		getComponentManager(component.getComponentType()).attachComponentToItsParentComponent(component, parentId);
	}
	/**
	 * This method removes the component from the parent.
	 * @param component the component to be remove
	 * @param parentId the corresponding parentId.
	 */
	public static void removeComponent(AbstractComponent component, Integer parentId)
	{
		if(parentId==null)
			parentId=new Integer(0);
		getComponentManager(component.getComponentType()).removeComponent(component, parentId);
	}

	/**
	 * @param component to be copied;
	 * @return the copied component; this method copies location information, metadata information and increment the id to get the most recent index.
	 */
	public static AbstractComponent copyComponent(AbstractComponent component)
	{
		return getComponentManager(component.getComponentType()).copyComponent(component);
	}


	public static AbstractComponent updateComponentMetaData(AbstractComponent sourceComponent, LocationCoordinates locationCoordinates, AbstractMetaData metaData, AbstractComponent attachIt)
	{
		return getComponentManager(sourceComponent.getComponentType()).updateComponentMetaData(sourceComponent, locationCoordinates, metaData, attachIt);
	}
}
