package logic.componentManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import logic.model.AbstractComponent;
import logic.model.AbstractMetaData;
import logic.model.LocationCoordinates;
import logic.model.Place;
import logic.model.Tokens;

public class PlaceManager extends AbstractComponentManager<Place> {

	private Set<Place> _places;
	private int _indexCount;
	private static PlaceManager _placeComponentManager;

	private  PlaceManager(Set<Place> places, int index) {
		this._places=places;
		this._indexCount=index;
	} 
	@Override
	public Place getComponent() {
		_indexCount++;
		Place place =new Place(_indexCount, new Tokens(0), "P"+_indexCount);
		_places.add(place);
		return place;
	}

	public static PlaceManager getCurrentInstance()
	{
		if(_placeComponentManager==null)
			_placeComponentManager= new PlaceManager(new TreeSet<Place>(), 0);
		return _placeComponentManager;
	}

	@Override
	 void attachComponentToItsParentComponent(Place component, int parentComponentId) {
		PetriNetManager.initialize().addPlace(component);
	}

	@Override
	 void removeComponent(Place component, int parnetComponentId) {
		this._places.remove(component);
		SourceArcManager.getCurrentInstance().removeArcsWithGivenPlace(component); //remove all associated source arcs
		DestinationArcManager.getCurrentInstance().removeArcsWithGivenPlace(component); //remove all associated destination arcs
		PetriNetManager.initialize().removePlace(component); //finally remove the place itself
	}

	@Override
	 public Set<Place> getComponents() {
		// TODO Auto-generated method stub
		return _places;
	}
	@Override
	 Place updateComponentMetaData(Place component, LocationCoordinates locationCoordinates,
			AbstractMetaData metaData, AbstractComponent attachIt) {
		component.set_locationCoordinates(locationCoordinates);
		component.set_tokens(metaData);
		return component;
	}
	@Override
	Place copyComponent(Place component) {
		_indexCount++;
		Tokens token= new Tokens(component.get_tokens().getIntCount());
		Place place= new Place(_indexCount, token,"P"+_indexCount);
		LocationCoordinates locationCoordinates= new LocationCoordinates(component.get_locationCoordinates().getxPoint(),component.get_locationCoordinates().getyPoint());
		place.set_locationCoordinates(locationCoordinates);
		_places.add(place);
	//PetriNetManager.getCurrentInstance().getComponent().get_places().add(place);
		return place;
	}
	@Override
	protected void updateIndexCount(int currentVal) {
	this._indexCount=currentVal;
	}
	
}
