package io;

import logic.model.LocationCoordinates;
import logic.model.Place;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by edavis on 9/22/17.
 */
public class PlaceSerializer extends PetriSerializable {
    private Place _place;

    public PlaceSerializer(Place place) {
        _place = place;
    }

    public PlaceSerializer() {
        this(null);
    }

    public void readObject(Node placeNode) throws PetriSerializerException {
        // Get place ID
        NamedNodeMap nodeAttrs = placeNode.getAttributes();
        String idStr  = nodeAttrs.getNamedItem("id").getNodeValue();

        int nTokens = 0;
        LocationCoordinates coords = new LocationCoordinates(0, 0);
        NodeList placeChildren = placeNode.getChildNodes();

        for (int i = 0; i < placeChildren.getLength(); i++) {
            Node child = placeChildren.item(i);
            String nodeName = child.getNodeName();

            if (nodeName.equals("initialMarking")) {
                NodeList markingChildren = child.getChildNodes();
                for (int j = 0; j < markingChildren.getLength(); j++) {
                    child = markingChildren.item(j);
                    nodeName = child.getNodeName();

                    if (nodeName.equals("value")) {
                        String nodeValue = child.getFirstChild().getNodeValue();
                        if (nodeValue.indexOf(',') > 0) {
                            nodeValue = nodeValue.split(",")[1];
                        }

                        nTokens = Integer.parseInt(nodeValue);
                    }
                }
            } else if (nodeName.equals("graphics")) {
                GraphicsSerializer graphicsSerializer = new GraphicsSerializer();
                graphicsSerializer.readObject(child);
                coords = graphicsSerializer.get_coords();
            }
        }

        IOComponentManager compMan = IOComponentManager.get();
        _place = compMan.getPlace(idStr, nTokens);
        _place.set_locationCoordinates(coords);
    }

    public void writeObject(Element placeNode) throws PetriSerializerException {
        XmlFactory xmlFactory = XmlFactory.get();
        String placeName = _place.getPlaceName();
        placeNode.setAttribute("id", placeName);

        Element graphicsNode = xmlFactory.createElement("graphics");
        GraphicsSerializer graphicsSerializer = new GraphicsSerializer(_place.get_locationCoordinates());
        graphicsSerializer.writeObject(graphicsNode);
        placeNode.appendChild(graphicsNode);

        Element nameNode = xmlFactory.createElement("name");
        Element valueNode = xmlFactory.createElement("value", placeName);
        nameNode.appendChild(valueNode);
        placeNode.appendChild(nameNode);

        // Name node can have (x,y) pos too, but omitting for now...

        Element markingNode = xmlFactory.createElement("initialMarking");
        valueNode = xmlFactory.createElement("value", String.format("Default,%d",
                _place.get_tokens().getIntCount()));
        markingNode.appendChild(valueNode);
        placeNode.appendChild(markingNode);
    }

    public Place get_place() {
        return _place;
    }
}
