package io;

/**
 * Created by edavis on 9/22/17.
 */
public class PetriSerializerException extends Exception {
    public PetriSerializerException(String message) {
        super(message);
    }
}
