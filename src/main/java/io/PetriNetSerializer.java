package io;

import logic.model.*;

import java.io.*;
import java.util.*;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * PetriNetSerializer
 *
 * Class for serializing a PetriNet object both to and from an XML stream.
 *
 * @author Eddie Davis
 * @version 1.0
 * @date 9/22/17
 */
public class PetriNetSerializer extends PetriSerializable {
    /**
     * PetriNet object.
     */
    protected PetriNet _petrinet;

    /**
     * XML Object Factory
     */
    protected XmlFactory _xmlFactory = XmlFactory.get();

    /**
     * Constructor with Petri net.
     *
     * @param petrinet
     * @assignable _petrinet
     * @ensures IOComponentManager is initialized.
     */
    public PetriNetSerializer(PetriNet petrinet) {
        _petrinet = petrinet;
        IOComponentManager.get().initialize();
    }

    /**
     * Default Constructor.
     */
    public PetriNetSerializer() {
        this(null);
    }

    /**
     * readObject
     *
     * Reads an XML stream and converts it to a PetriNet object.
     *
     * @param in XML input stream.
     * @requires in != null null
     * @ensures _petrinet != null
     *
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws ParserConfigurationException
     * @throws PetriSerializerException
     * @throws SAXException
     */
    public void readObject(InputStream in) throws IOException, ClassNotFoundException,
            ParserConfigurationException, PetriSerializerException, SAXException {
        XmlFactory xmlFactory = XmlFactory.get();
        xmlFactory.createDocument(in);

        Element root = xmlFactory.getElement();
        NodeList netNodes = root.getElementsByTagName("net");

        if (netNodes.getLength() < 1) {
            throw new PetriSerializerException("No Petri nets found in XML stream.");
        }

        readObject(netNodes.item(0));
    }

    /**
     * readObject
     *
     * Reads XML node and converts it to a PetriNet object.
     *
     * @param netNode XML 'net' node.
     * @requires netNode != null && netNode.getNodeName().equals("net")
     * @ensures _petrinet != null
     * @throws PetriSerializerException
     */
    public void readObject(Node netNode) throws PetriSerializerException {
        IOComponentManager compMan = IOComponentManager.get();
        NodeList childNodes = netNode.getChildNodes();

        List<Node> placeNodes = new ArrayList<>();
        List<Node> transNodes = new ArrayList<>();
        List<Node> arcNodes = new ArrayList<>();

        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            String nodeName = node.getNodeName();
            if (nodeName.equals("place")) {
                placeNodes.add(node);
            } else if (nodeName.equals("transition")) {
                transNodes.add(node);
            } else if (nodeName.equals("arc")) {
                arcNodes.add(node);
            }
        }

        for (Node placeNode : placeNodes) {
            PlaceSerializer placeSerializer = new PlaceSerializer();
            placeSerializer.readObject(placeNode);
        }

        for (Node arcNode : arcNodes) {
            NamedNodeMap nodeAttrs = arcNode.getAttributes();
            String source = nodeAttrs.getNamedItem("source").getNodeValue();
            if (compMan.isPlace(source)) {
                SourceArcSerializer sourceArcSerializer = new SourceArcSerializer();
                sourceArcSerializer.readObject(arcNode);
            } else {
                DestArcSerializer destArcSerializer = new DestArcSerializer();
                destArcSerializer.readObject(arcNode);
            }
        }

        for (Node transNode : transNodes) {
            TransitionSerializer transSerializer = new TransitionSerializer();
            transSerializer.readObject(transNode);
        }

        _petrinet = compMan.getPetriNet();
    }

    /**
     * readObjectNoData
     *
     * Implements readObjectNoData of Serializable interface.
     *
     * @throws ObjectStreamException
     */
    public void readObjectNoData() throws ObjectStreamException {
        throw new InvalidObjectException("Stream data required");
    }

    /**
     * writeObject
     *
     * Writes the PetriNet object to the XML output stream.
     *
     * @param out XML output stream.
     * @requires out != null

     * @throws IOException
     * @throws ParserConfigurationException
     * @throws PetriSerializerException
     * @throws TransformerException
     */
    public void writeObject(OutputStream out) throws IOException, ParserConfigurationException,
        PetriSerializerException, TransformerException {

        // Create document...
        Document doc = _xmlFactory.createDocument();

        // Create root...
        Element root = _xmlFactory.createElement("pnml");
        Element net = _xmlFactory.createElement("net");
        writeObject(net);
        root.appendChild(net);
        doc.appendChild(root);

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(out);
        transformer.transform(source, result);
    }

    /**
     * writeObject
     *
     * Writes the PetriNet object to an XML net node.
     *
     * @param netNode XML net node.
     * @requires netNode != null && netNode.getNodeName().equals("net")
     * @ensures netNode.getChildNodes().getLength() > 0
     * @throws PetriSerializerException
     */
    public void writeObject(Element netNode) throws PetriSerializerException {
        // Set Petri net in component manager so its components are available to serializer subclasses.
        IOComponentManager.get().setPetriNet(_petrinet);

        // Set net attributes...
        netNode.setAttribute("id", "Net-One");
        netNode.setAttribute("type", "P/T net");

        // Places...
        for (Place place : _petrinet.get_places()) {
            Element placeNode = _xmlFactory.createElement("place");
            PlaceSerializer placeSerializer = new PlaceSerializer(place);
            placeSerializer.writeObject(placeNode);
            netNode.appendChild(placeNode);
        }

        // Transitions...
        for (Firable transition : _petrinet.get_transitions()) {
            Element transNode = _xmlFactory.createElement("transition");
            TransitionSerializer transSerializer = new TransitionSerializer((Transition) transition);
            transSerializer.writeObject(transNode);
            netNode.appendChild(transNode);
        }

        // Source arcs...
        for (Firable transition : _petrinet.get_transitions()) {
            Set<SourceArc> sourceArcs = ((Transition) transition).getSourceArcs();
            for (SourceArc sourceArc : sourceArcs) {
                Element arcNode = _xmlFactory.createElement("arc");
                SourceArcSerializer arcSerializer = new SourceArcSerializer(sourceArc);
                arcSerializer.writeObject(arcNode);
                netNode.appendChild(arcNode);
            }
        }

        // Destination arcs...
        for (Firable transition : _petrinet.get_transitions()) {
            Set<DestinationArc> destArcs = ((Transition) transition).getDestinationArcs();
            for (DestinationArc destArc : destArcs) {
                Element arcNode = _xmlFactory.createElement("arc");
                DestArcSerializer arcSerializer = new DestArcSerializer(destArc);
                arcSerializer.writeObject(arcNode);
                netNode.appendChild(arcNode);
            }
        }
    }

    /**
     * get_petrinet
     *
     * @return The PetriNet object.
     */
    public PetriNet get_petrinet() {
        return _petrinet;
    }

    /**
     * toString
     *
     * @return String representation of the PetriNet object.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder(1000);
        if (_petrinet != null) {
            for (Place place : _petrinet.get_places()) {
                sb.append(String.format("%s(%d)\n", place.getPlaceName(),
                    place.get_tokens().getIntCount()));
            }

            for (Firable firable : _petrinet.get_transitions()) {
                Transition transition = (Transition) firable;
                String transName = transition.getTransitionName();

                Set<SourceArc> sourceArcs = transition.getSourceArcs();
                Set<DestinationArc> destArcs = transition.getDestinationArcs();

                sb.append(String.format("%s(%d,%d)\n", transName,
                        sourceArcs.size(), destArcs.size()));

                for (SourceArc sourceArc : sourceArcs) {
                    String placeName = sourceArc.get_place().getPlaceName();
                    int weight = sourceArc.get_arcValue();
                    sb.append(String.format("%s -> (%d) -> %s\n", placeName, weight, transName));
                }

                for (DestinationArc destArc : destArcs) {
                    String placeName = destArc.get_place().getPlaceName();
                    int weight = destArc.get_arcValue();
                    sb.append(String.format("%s -> (%d) -> %s\n", transName, weight, placeName));
                }
            }
        }

        return sb.toString();
    }
}
