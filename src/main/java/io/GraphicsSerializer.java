package io;

import logic.model.Drawable;
import logic.model.LocationCoordinates;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.stream.Location;

/**
 * Created by edavis on 9/30/17.
 */
public class GraphicsSerializer extends PetriSerializable {
    protected LocationCoordinates _coords;

    public GraphicsSerializer(LocationCoordinates coords) {
        _coords = coords;
    }

    public GraphicsSerializer() {
        this(null);
    }

    public void readObject(Node graphicsNode) throws PetriSerializerException {
        float xPos = 0.0F, yPos = 0.0F;
        NodeList children = graphicsNode.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeName().equals("position")) {
                NamedNodeMap nodeAttrs = child.getAttributes();
                xPos = Float.parseFloat(nodeAttrs.getNamedItem("x").getNodeValue());
                yPos = Float.parseFloat(nodeAttrs.getNamedItem("y").getNodeValue());
                break;
            }
        }

        _coords = new LocationCoordinates((int) xPos, (int) yPos);
    }

    public void writeObject(Element graphicsNode) throws PetriSerializerException {
        Element posNode = XmlFactory.get().createElement("position");
        int xPos = _coords.getxPoint();
        int yPos = _coords.getyPoint();
        posNode.setAttribute("x", String.format("%d", xPos));
        posNode.setAttribute("y", String.format("%d", yPos));
        graphicsNode.appendChild(posNode);
    }

    public LocationCoordinates get_coords() {
        return _coords;
    }
}
