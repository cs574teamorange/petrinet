package io;

import logic.model.*;

import java.util.*;

/**
 * Created by edavis on 9/26/17.
 */
public class IOComponentManager {
    private static IOComponentManager _instance = null;

    protected static int _nComponents = 0;
    protected int _petriNetID = 0;

    // Places & Transitions...
    protected HashMap<String, Place> _places = new HashMap<>();
    protected HashMap<String, Firable> _transitions = new HashMap<>();

    // Source & Dest Arcs...
    protected HashMap<String, List<SourceArc> > _sourceArcs = new HashMap<>();
    protected HashMap<String, List<DestinationArc> > _destArcs = new HashMap<>();

    protected IOComponentManager() {
    }

    public static IOComponentManager get() {
        if(_instance == null) {
            _instance = new IOComponentManager();
        }

        return _instance;
    }

    public void initialize() {
        _petriNetID = _nComponents;
        _places = new HashMap<>();
        _transitions = new HashMap<>();
        _sourceArcs = new HashMap<>();
        _destArcs = new HashMap<>();
    }

//    public Place getPlace(String placeName) {
//        return getPlace(placeName, 0);
//    }

    public Place getPlace(String placeName, int nTokens) {
        Place place;
        Tokens tokens = new Tokens(nTokens);

        if (_places.containsKey(placeName)) {
            place = _places.get(placeName);
            place.set_tokens(tokens);
        } else {
            _nComponents += 1;
            place = new Place(_nComponents, tokens, placeName);
            _places.put(placeName, place);
        }

        return place;
    }

//    public SourceArc getSourceArc(String placeName, String transName) {
//        return getSourceArc(placeName, transName, 1);
//    }

    public SourceArc getSourceArc(String placeName, String transName, int weight) {
        Place place = _places.get(placeName);

        _nComponents += 1;
        SourceArc sourceArc = new SourceArc(_nComponents, place, weight);

        // Transition ID is the destID
        if (!_sourceArcs.containsKey(transName)) {
            _sourceArcs.put(transName, new ArrayList<>());
        }

        _sourceArcs.get(transName).add(sourceArc);

        return sourceArc;
    }

//    public DestinationArc getDestinationArc(String placeName, String transName) {
//        return getDestinationArc(placeName, transName, 1);
//    }

    public DestinationArc getDestinationArc(String placeName, String transName, int weight) {
        Place place = _places.get(placeName);

        _nComponents += 1;
        DestinationArc destArc = new DestinationArc(_nComponents, place, weight);

        // Transition ID is the destID
        if (!_destArcs.containsKey(transName)) {
            _destArcs.put(transName, new ArrayList<>());
        }

        _destArcs.get(transName).add(destArc);

        return destArc;
    }

    public List<SourceArc> getSourceArcs(String transName) throws PetriSerializerException {
        List<SourceArc> arcs;
        if (_sourceArcs.containsKey(transName)) {
            arcs = _sourceArcs.get(transName);
        } else {
            throw new PetriSerializerException("Transition '" + transName + "' has no source arcs.");
        }
        return arcs;
    }

    public List<DestinationArc> getDestinationArcs(String transName) throws PetriSerializerException {
        List<DestinationArc> arcs;
        if (_destArcs.containsKey(transName)) {
            arcs = _destArcs.get(transName);
        } else {
            throw new PetriSerializerException("Transition '" + transName + "' has no destination arcs.");
        }
        return arcs;
    }

    public Transition getTransition(String transName) throws PetriSerializerException {
        Set<SourceArc> sourceArcs = new HashSet<>(getSourceArcs(transName));
        Set<DestinationArc> destArcs = new HashSet<>(getDestinationArcs(transName));

        return getTransition(transName, sourceArcs, destArcs);
    }

    public Transition getTransition(String transName, Set<SourceArc> sourceArcs, Set<DestinationArc> destArcs) {
        Transition transition;
        if (_transitions.containsKey(transName)) {
            transition = (Transition) _transitions.get(transName);
            transition.setSourceArcs(sourceArcs);
            transition.setDestinationArcs(destArcs);
        } else {
            _nComponents += 1;
            transition = new Transition(_nComponents, sourceArcs, destArcs, transName);
            _transitions.put(transName, transition);
        }

        for (SourceArc sourceArc : sourceArcs) {
            sourceArc.set_transition(transition);
        }

        for (DestinationArc destArc : destArcs) {
            destArc.set_transition(transition);
        }

        return transition;
    }

    public boolean isPlace(String id) {
        return _places.containsKey(id);
    }

    public boolean isTransition(String id) {
        return _transitions.containsKey(id);
    }

    public Set<Place> getPlaces() {
        return new HashSet<>(_places.values());
    }

    public Set<Firable> getTransitions() {
        return new HashSet<>(_transitions.values());
    }

    public PetriNet getPetriNet() {
        return new PetriNet(_petriNetID, getPlaces(), getTransitions());
    }

    public void setPetriNet(PetriNet petriNet) {
        initialize();

        _places.clear();
        for (Place place : petriNet.get_places()) {
            _places.put(place.getPlaceName(), place);
        }

        _transitions.clear();
        _sourceArcs.clear();
        _destArcs.clear();

        for (Firable firable : petriNet.get_transitions()) {
            Transition transition = (Transition) firable;
            String transName = transition.getTransitionName();
            _transitions.put(transName, transition);

            _sourceArcs.put(transName, new ArrayList<>(transition.getSourceArcs()));
            _destArcs.put(transName, new ArrayList<>(transition.getDestinationArcs()));
        }
    }
}
