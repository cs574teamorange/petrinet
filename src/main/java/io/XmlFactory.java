package io;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by edavis on 9/27/17.
 */
public class XmlFactory {
    private static XmlFactory _instance = null;

    protected DocumentBuilderFactory _dbFactory = DocumentBuilderFactory.newInstance();
    protected DocumentBuilder _docBuilder = null;
    protected Document _currDoc = null;

    protected XmlFactory() {
    }

    public static XmlFactory get() {
        if(_instance == null) {
            _instance = new XmlFactory();
        }

        return _instance;
    }

    public DocumentBuilder getBuilder() throws ParserConfigurationException {
        if (_docBuilder == null) {
            _docBuilder = _dbFactory.newDocumentBuilder();
        }

        return _docBuilder;
    }

//    public Document getDocument() {
//        return _currDoc;
//    }

    public Element getElement() {
        return getElement(_currDoc);
    }

    public Element getElement(Document document) {
        return document.getDocumentElement();
    }

    public Document createDocument() throws ParserConfigurationException {
        _currDoc = getBuilder().newDocument();

        return _currDoc;
    }

//    public Document createDocument(String filePath) throws IOException, ParserConfigurationException, SAXException {
//        return createDocument(new FileInputStream(filePath));
//    }

    public Document createDocument(InputStream in) throws IOException, ParserConfigurationException, SAXException {
        _currDoc = getBuilder().parse(in);
        _currDoc.getDocumentElement().normalize();

        return _currDoc;
    }

    public Element createElement(String tag) {
        return createElement(tag, "");
    }

    public Element createElement(String tag, String innerText) {
        return createElement(tag, innerText, new HashMap<>());
    }

    public Element createElement(String tag, String innerText, Map<String, String> attributes) {
        return createElement(tag, innerText, attributes, _currDoc);
    }

    public Element createElement(String tag, String innerText, Map<String, String> attributes, Document document) {
        Element elem = document.createElement(tag);

        for (String attrName : attributes.keySet()) {
            String attrValue = attributes.get(attrName);
            elem.setAttribute(attrName, attrValue);
        }

        if (innerText.length() > 0) {
            elem.appendChild(document.createTextNode(innerText));
        }

        return elem;
    }
}
