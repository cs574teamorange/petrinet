package io;

import logic.model.LocationCoordinates;
import logic.model.Transition;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by edavis on 9/22/17.
 */
public class TransitionSerializer extends PetriSerializable {
    private Transition _transition;

    public TransitionSerializer(Transition place) {
        _transition = place;
    }

    public TransitionSerializer() {
        this(null);
    }

    public void readObject(Node transNode) throws PetriSerializerException {
        NamedNodeMap nodeAttrs = transNode.getAttributes();
        String idStr  = nodeAttrs.getNamedItem("id").getNodeValue();
        LocationCoordinates coords = new LocationCoordinates(0, 0);

        NodeList children = transNode.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeName().equals("graphics")) {
                GraphicsSerializer graphicsSerializer = new GraphicsSerializer();
                graphicsSerializer.readObject(child);
                coords = graphicsSerializer.get_coords();
            }
        }

        IOComponentManager compMan = IOComponentManager.get();
        _transition = compMan.getTransition(idStr);
        _transition.set_locationCoordinates(coords);
    }

    public void writeObject(Element transNode) throws PetriSerializerException {
        XmlFactory xmlFactory = XmlFactory.get();
        String transName = _transition.getTransitionName();
        transNode.setAttribute("id", transName);

        Element graphicsNode = xmlFactory.createElement("graphics");
        GraphicsSerializer graphicsSerializer = new GraphicsSerializer(_transition.get_locationCoordinates());
        graphicsSerializer.writeObject(graphicsNode);
        transNode.appendChild(graphicsNode);

        // We are not using these, but including for compatibility...
        String[] keys = new String[] {"name", "orientation", "rate", "timed", "infiniteServer", "priority"};
        String[] vals = new String[] {transName, "0", "1.0", "false", "false", "1"};

        for (int i = 0; i < keys.length; i++) {
            String key = keys[i];
            String val = vals[i];

            Element keyNode = xmlFactory.createElement(key);
            Element valNode = xmlFactory.createElement("value", val);
            keyNode.appendChild(valNode);
            transNode.appendChild(keyNode);
        }
    }
}
