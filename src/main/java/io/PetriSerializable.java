package io;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.*;

/**
 * PetriNetSerializer
 *
 * Class for serializing a PetriNet object both to and from an XML stream.
 *
 * @author Eddie Davis
 * @version 1.0
 * @date 9/26/17
 */
public abstract class PetriSerializable implements Serializable {
    /**
     * readObject
     *
     * Reads XML node and converts it to a PetriNet object.
     *
     * @param node XML node.
     * @requires node != null
     * @throws PetriSerializerException
     */
	public abstract void readObject(Node node) throws PetriSerializerException;

    /**
     * writeObject
     *
     * Writes the PetriNet object to an XML net node.
     *
     * @param elem XML element.
     * @requires elem != null
     * @throws PetriSerializerException
     */
	public abstract void writeObject(Element elem) throws PetriSerializerException;
}
