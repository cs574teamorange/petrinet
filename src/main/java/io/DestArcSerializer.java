package io;

import logic.model.DestinationArc;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Created by edavis on 9/22/17.
 */
public class DestArcSerializer extends PetriSerializable {
    private DestinationArc _arc;

    public DestArcSerializer(DestinationArc arc) {
        _arc = arc;
    }

    public DestArcSerializer() {
        this(null);
    }

    public void readObject(Node arcNode) throws PetriSerializerException {
        NamedNodeMap nodeAttrs = arcNode.getAttributes();
        String source = nodeAttrs.getNamedItem("source").getNodeValue();
        String dest = nodeAttrs.getNamedItem("target").getNodeValue();

        ArcWeightSerializer weightSerializer = new ArcWeightSerializer();
        weightSerializer.readObject(arcNode);
        int weight = weightSerializer.get_weight();

        IOComponentManager compMan = IOComponentManager.get();
        _arc = compMan.getDestinationArc(dest, source, weight);
    }

    public void writeObject(Element arcNode) throws PetriSerializerException {
        XmlFactory xmlFactory = XmlFactory.get();

        String source = _arc.get_transition().getTransitionName();
        String target = _arc.get_place().getPlaceName();
        String arcName = String.format("%s to %s", source, target);

        arcNode.setAttribute("id", arcName);
        arcNode.setAttribute("source", source);
        arcNode.setAttribute("target", target);

        Element weightNode = xmlFactory.createElement("inscription");
        ArcWeightSerializer weightSerializer = new ArcWeightSerializer(_arc.get_arcValue());
        weightSerializer.writeObject(weightNode);
        arcNode.appendChild(weightNode);

        Element tagNode = xmlFactory.createElement("tagged");
        tagNode.appendChild(xmlFactory.createElement("value", "false"));
        arcNode.appendChild(tagNode);

        Element typeNode = xmlFactory.createElement("type");
        typeNode.setAttribute("value", "normal");
        arcNode.appendChild(tagNode);
    }
}
