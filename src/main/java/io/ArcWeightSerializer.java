package io;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by edavis on 9/26/17.
 */
public class ArcWeightSerializer extends PetriSerializable {
    private int _weight;

    public ArcWeightSerializer(int weight) {
        _weight = weight;
    }

    public ArcWeightSerializer() {
        this(1);
    }

    public void readObject(Node arcNode) throws PetriSerializerException {
        // Parse children to get arc weight...
        _weight = 0;
        NodeList arcChildren = arcNode.getChildNodes();
        for (int i = 0; i < arcChildren.getLength() && _weight < 1; i++) {
            Node child = arcChildren.item(i);
            String nodeName = child.getNodeName();
            if (nodeName.equals("inscription")) {
                NodeList insChildren = child.getChildNodes();
                for (int j = 0; j < insChildren.getLength() && _weight < 1; j++) {
                    child = insChildren.item(j);
                    if (child.getNodeName().equals("value")) {
                        String nodeValue = child.getFirstChild().getNodeValue();
                        if (nodeValue.indexOf(',') > 0) {
                            nodeValue = nodeValue.substring(nodeValue.indexOf(',') + 1);
                        }
                        _weight = Integer.parseInt(nodeValue);
                    }
                }
            }
        }
    }

    public void writeObject(Element weightNode) throws PetriSerializerException {
        XmlFactory xmlFactory = XmlFactory.get();

        Element valueNode = xmlFactory.createElement("value",
                String.format("Default,%d", _weight));
        weightNode.appendChild(valueNode);

        Element graphicsNode = xmlFactory.createElement("graphics");
        weightNode.appendChild(graphicsNode);
    }

    public int get_weight() {
        return _weight;
    }
}
