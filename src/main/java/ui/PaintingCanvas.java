package ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.DestinationArcManager;
import logic.componentManager.PetriNetManager;
import logic.componentManager.SourceArcManager;
import logic.componentManager.ToolbarManager;
import logic.model.DestinationArc;
import logic.model.Firable;
import logic.model.LocationCoordinates;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Tokens;
import logic.model.Transition;

public class PaintingCanvas extends JPanel{

	private int _place_radius = 100;
	private int _transition_width = 40;
	private int _transition_heigth = 100;
	private int _arrow_size = 15;
	private double _arrow_angle = (30*Math.PI/180);
	
	private double _arc_eps = 20;
	 
	
	private boolean _msPressed = false;
	private Point _msPoint;
	private Point _prev_msPoint;
	
	private Set<Place> _selected_places;
	private Set<Transition> _selected_transitions;
	private Set<SourceArc> _selected_src_arcs;
	private Set<DestinationArc> _selected_dst_arcs;
	
	private JPopupMenu _popup;
	private String _rename;
	private int _tokens;
	private int _weight;
	
	public PaintingCanvas() {
		this.addMouseListener(new MouseHandler());
		this.addMouseMotionListener(new MouseMotionHandler());
		
		_selected_places = new TreeSet<Place>();
		_selected_transitions = new TreeSet<Transition>();
		_selected_src_arcs = new TreeSet<SourceArc>();
		_selected_dst_arcs = new TreeSet<DestinationArc>();
		
		_popup = new JPopupMenu();
		
		
	}
	
	/**
	 * Paints the GUI component.
	 * 
	 * @param g is the Graphics component from Swing.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		paint_petrinet_places(PetriNetManager.initialize().getComponent().get_places(), g);
		paint_petrinet_transitions(PetriNetManager.initialize().getComponent().get_transitions(), g);
		paint_petrinet_arcs(PetriNetManager.initialize().getComponent().get_transitions(), g);
		
		if (!_selected_places.isEmpty())
			paint_selected_places(this._selected_places, g);
		if (!_selected_transitions.isEmpty())
			paint_selected_transitions(this._selected_transitions, g);
		
		this.setBackground(Color.WHITE);
	}
	
	/**
	 * Paints the places of the petrinet on the canvas
	 * 
	 * @param places a set of petrinet Places.
	 * @param g is the Graphics component from Swing.
	 */
	private void paint_petrinet_places(Set<Place> places, Graphics g) {
		g.setColor(Color.BLACK);
		Iterator<Place>  it = places.iterator();
		while(it.hasNext()) {
			Place p = it.next();
			int x = p.get_locationCoordinates().getxPoint();
			int y = p.get_locationCoordinates().getyPoint();
			g.drawOval(x, y, _place_radius, _place_radius);
			g.drawString(p.getPlaceName(), x+((int)this._place_radius/2), y+this._place_radius+11);
			
			Tokens tok = (Tokens) p.get_tokens();
			if (tok.getIntCount() > 0) {
				g.setColor(Color.BLUE);
				g.fillOval(x+((int)8*this._place_radius/20), y+((int)8*this._place_radius/20), (int) _place_radius/10, (int) _place_radius/10);
				g.setColor(Color.BLACK);
				g.drawString(Integer.toString(tok.getIntCount()), x+(int)(7*this._place_radius/20), y+(int)(7*this._place_radius/20));
			}
		}
	}
	
	
	/**
	 * Paints the transitions of the petrinet on the canvas
	 * 
	 * @param transitions a set of petrinet Transitions.
	 * @param g is the Graphics component from Swing.
	 */
	private void paint_petrinet_transitions(Set<Firable> transitions, Graphics g) {
		g.setColor(Color.BLACK);
		Iterator<Firable>  it = transitions.iterator();
		
		while(it.hasNext()) {
			Transition t = (Transition) it.next();
			int x = t.get_locationCoordinates().getxPoint();
			int y = t.get_locationCoordinates().getyPoint();
			if (t.isFirable()) {
				g.setColor(Color.ORANGE);
				g.fillRect(x, y, this._transition_width, this._transition_heigth);
				g.setColor(Color.BLACK);
				g.drawRect(x, y, this._transition_width, this._transition_heigth);
			} else if (t.isTransitionDead()) {
				g.setColor(Color.RED);
				g.fillRect(x, y, this._transition_width, this._transition_heigth);
				g.setColor(Color.BLACK);
				g.drawRect(x, y, this._transition_width, this._transition_heigth);
			} else
				g.drawRect(x, y, this._transition_width, this._transition_heigth);
			g.drawString(t.getTransitionName(), x+((int)this._transition_width/2), y+this._transition_heigth+11);
		}
	}
	
	
	private void paint_petrinet_arcs(Set<Firable> transitions, Graphics g) {
		g.setColor(Color.BLACK);
		Iterator<Firable> it = transitions.iterator();
		
		while(it.hasNext()) {
			Transition t = (Transition) it.next();
			paint_source_arcs(t.getSourceArcs(), g,t);
			
			paint_destination_arcs(t.getDestinationArcs(), g,t);
			
			
		}
	}
	
	private void paint_source_arcs(Set<SourceArc> srcs, Graphics g, Transition t) {
		Iterator<SourceArc> it_src = srcs.iterator();
		while(it_src.hasNext()) {
			SourceArc arc = it_src.next();
			if (_selected_src_arcs.contains(arc))
				g.setColor(Color.LIGHT_GRAY);
			else
				g.setColor(Color.BLACK);
			Place src_place = arc.get_place();
			int src_x = src_place.get_locationCoordinates().getxPoint() + ((int) _place_radius/2);
			int src_y = src_place.get_locationCoordinates().getyPoint() + ((int) _place_radius/2);
			int dst_x = t.get_locationCoordinates().getxPoint() + ((int) _transition_width/2);
			int dst_y = t.get_locationCoordinates().getyPoint() + ((int) _transition_heigth/2);
			Point2D dst = intersect_arc_transition(new Point(src_x, src_y), new Point(dst_x, dst_y));
			dst_x = (int) dst.getX();
			dst_y = (int) dst.getY();
			
			double v_ab_x = dst_x - src_x;
			double v_ab_y = dst_y - src_y;
			double d = Math.sqrt(v_ab_x*v_ab_x + v_ab_y*v_ab_y);
			v_ab_x = (v_ab_x / d) * _place_radius/2;
			v_ab_y = (v_ab_y / d) * _place_radius/2;
			g.drawLine(src_x + (int) v_ab_x, src_y + (int) v_ab_y, dst_x, dst_y);
			draw_arrow(src_x, src_y, dst_x, dst_y, g);
			int x = (int) (src_x + dst_x)/2;
			int y = (int) (src_y + dst_y)/2;
			g.drawString(String.valueOf(arc.get_arcValue()), x, y+11);
		}
	}
	
	private Point2D intersect_arc_transition(Point2D a, Point2D transition_center) {
		Vector<Line2D> lines = new Vector<Line2D>();
		for (int i=0; i<4; i++) {
			lines.add(new Line2D.Double());
		}
		
		Vector<Point2D> points = new Vector<Point2D>();
		points.add(new Point2D.Double(transition_center.getX()-_transition_width/2, transition_center.getY()-_transition_heigth/2));
		points.add(new Point2D.Double(transition_center.getX()+_transition_width/2, transition_center.getY()-_transition_heigth/2));
		points.add(new Point2D.Double(transition_center.getX()+_transition_width/2, transition_center.getY()+_transition_heigth/2));
		points.add(new Point2D.Double(transition_center.getX()-_transition_width/2, transition_center.getY()+_transition_heigth/2));
		
		for (int i=0; i<lines.size(); i++) {
			lines.get(i).setLine(points.get(i%lines.size()), points.get((i+1)%lines.size()));
		}
		
		Line2D arc = new Line2D.Double(a, transition_center);
		Line2D intersection_line = new Line2D.Double();
		for (int i=0; i<lines.size(); i++) {
			if (arc.intersectsLine(lines.get(i))) {
				intersection_line = lines.get(i);
				break;
			}
		}
		
		double x = 0; double y = 0;
		if (intersection_line.getX2() - intersection_line.getX1() == 0) {
			double m0 = (arc.getY2() - arc.getY1()) / (arc.getX2() - arc.getX1());
			double b0 = arc.getY1() - m0*arc.getX1();
			x = intersection_line.getX1();
			y = m0*x + b0;
		} else if (arc.getX2() - arc.getX1() == 0) {
			double m1 = (intersection_line.getY2() - intersection_line.getY1()) / (intersection_line.getX2() - intersection_line.getX1());
			double b1 = intersection_line.getY1() - m1*intersection_line.getX1();
			x = intersection_line.getX1();
			y = m1*x + b1;
		} else {
			double m0 = (arc.getY2() - arc.getY1()) / (arc.getX2() - arc.getX1());
			double m1 = (intersection_line.getY2() - intersection_line.getY1()) / (intersection_line.getX2() - intersection_line.getX1());
			
			double b0 = arc.getY1() - m0*arc.getX1();
			double b1 = intersection_line.getY1() - m1*intersection_line.getX1();
			
			x = (b1-b0) / (m0-m1);
			y = m0*x + b0;
		}
		
		Point2D p = new Point2D.Double(x,y);
		
		return p;
	}
	
	
	
	private void paint_destination_arcs(Set<DestinationArc> dsts, Graphics g, Transition t) {
		Iterator<DestinationArc> it_dst = dsts.iterator();
		while(it_dst.hasNext()){
			DestinationArc arc = it_dst.next();
			if (_selected_dst_arcs.contains(arc))
				g.setColor(Color.LIGHT_GRAY);
			else
				g.setColor(Color.BLACK);
			Place dst_place = arc.get_place();
			int src_x = t.get_locationCoordinates().getxPoint() + ((int) _transition_width/2);
			int src_y = t.get_locationCoordinates().getyPoint() + ((int) _transition_heigth/2);
			int dst_x = dst_place.get_locationCoordinates().getxPoint() + ((int) _place_radius/2);
			int dst_y = dst_place.get_locationCoordinates().getyPoint() + ((int) _place_radius/2);
			
			Point2D src = intersect_arc_transition(new Point(dst_x, dst_y), new Point(src_x, src_y));
			src_x = (int) src.getX();
			src_y = (int) src.getY();
			
			double v_ab_x = dst_x - src_x;
			double v_ab_y = dst_y - src_y;
			double d = Math.sqrt(v_ab_x*v_ab_x + v_ab_y*v_ab_y);
			v_ab_x = (v_ab_x / d) * _place_radius/2;
			v_ab_y = (v_ab_y / d) * _place_radius/2;
			
			Rectangle r1 = new Rectangle(src_x-(int) _transition_width/2, src_y-(int) _transition_width/2,
										 src_x+(int) _transition_width/2, src_y+(int) _transition_width/2);
			Line2D l1 = new Line2D.Float(src_x, src_y, dst_x, dst_y);
			
			g.drawLine(src_x, src_y, dst_x - (int) v_ab_x, dst_y - (int) v_ab_y);
			draw_arrow(src_x, src_y, dst_x - (int) v_ab_x, dst_y - (int) v_ab_y, g);
			int x = (int) (src_x + dst_x)/2;
			int y = (int) (src_y + dst_y)/2;
			g.drawString(String.valueOf(arc.get_arcValue()), x, y+11);
		}
	}
	
	private Point line_transition_intersect(Point line_src, Point line_dst, Point trans_ctr) {
		
		
		
		return null;
	}
	
	
	
	/**
	 * Paints the selected elements.
	 * 
	 * @param places a set of petrinet Places.
	 * @param g is the Graphics componen from Swing.
	 */
	private void paint_selected_places(Set<Place> places, Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		Iterator<Place>  it = places.iterator();
		while(it.hasNext()) {
			Place p = it.next();
			int x = p.get_locationCoordinates().getxPoint();
			int y = p.get_locationCoordinates().getyPoint();
			g.drawRect(x, y, this._transition_heigth, this._transition_heigth);
		}
	}
	

	/**
	 * Detects when the mouse has clicked over a place.
	 * 
	 * @param places a set of petrinet Places.
	 * @param x the x coordinate of the mouse when clicked.
	 * @param y the y coordinate of the mouse when clicked.
	 * @return a petrinet Place if the x and y coordinates fall in it, null otherwise.
	 */
	private Place get_selected_place(Set<Place> places, int x, int y) {
		Iterator<Place>  it = places.iterator();
		while(it.hasNext()) {
			Place p = it.next();
			int px = p.get_locationCoordinates().getxPoint() + this._place_radius/2;
			int py = p.get_locationCoordinates().getyPoint() + this._place_radius/2;
			if (px-this._place_radius/2<= x && x <= px+this._place_radius/2 &&
				py-this._place_radius/2<= y && y <= py+this._place_radius/2)
				return p;
		}
		return null;
	}
	
	
	/**
	 * Paints the selected transitions.
	 * 
	 * @param transitions a set of petrinet Transitions.
	 * @param g is the Graphics component from Swing.
	 */
	private void paint_selected_transitions(Set<Transition> transitions, Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		Iterator<Transition>  it = transitions.iterator();
		while(it.hasNext()) {
			Transition t =  it.next();
			int x = t.get_locationCoordinates().getxPoint();
			int y = t.get_locationCoordinates().getyPoint();
			g.drawRect(x-((int) _transition_width/2), y, this._transition_heigth, this._transition_heigth);
		}
	}
	
	
	/**
	 * Detects when the mouse has clicked over a Transition.
	 * 
	 * @param transitions a set of petrinet Transitions.
	 * @param x the x coordinate of the mouse when clicked.
	 * @param y the y coordinate of the mouse when clicked.
	 * @return a petrinet Transition if the x and y coordinates fall in it, null otherwise.
	 */	 
	private Transition get_selected_transition(Set<Firable> transitions, int x, int y) {
		Iterator<Firable>  it = transitions.iterator();
		while(it.hasNext()) {
			Transition t = (Transition) it.next();
			int px = t.get_locationCoordinates().getxPoint() + this._transition_width/2;
			int py = t.get_locationCoordinates().getyPoint() + this._transition_heigth/2;
			if (px-this._transition_width/2<= x && x <= px+this._transition_width/2 &&
				py-this._transition_heigth/2<= y && y <= py+this._transition_heigth/2)
				return t;
		}
		return null;
	}
	
	
	/**
	 * Detects when the mouse has clicked over a SourceArc.
	 * 
	 * @param src_arcs a set of petrinet SourceArcs.
	 * @param x the x coordinate of the mouse when clicked.
	 * @param y the y coordinate of the mouse when clicked.
	 * @return a petrinet SourceArc if the x and y coordinates fall in it, null otherwise.
	 */	 
	private SourceArc get_selected_sourceArc(Set<SourceArc> src_arcs, int x, int y) {
		Point2D point = new Point2D.Double(x, y);
		Iterator<SourceArc> it = src_arcs.iterator();
		while(it.hasNext()) {
			SourceArc arc = it.next();
			Place p = arc.get_place();
			Transition t = arc.get_transtion();
			Line2D line = new Line2D.Double(p.get_locationCoordinates().getxPoint()+_place_radius/2,p.get_locationCoordinates().getyPoint()+_place_radius/2,
											t.get_locationCoordinates().getxPoint()+_transition_width/2,t.get_locationCoordinates().getyPoint()+_transition_heigth/2);
			if (line.ptSegDist(point) < _arc_eps)
				return arc;
		}
		
		return null;
	}
	
	/**
	 * Detects when the mouse has clicked over a DestinationArc.
	 * 
	 * @param dst_arcs a set of petrinet DestinationArc.
	 * @param x the x coordinate of the mouse when clicked.
	 * @param y the y coordinate of the mouse when clicked.
	 * @return a petrinet DestinationArc if the x and y coordinates fall in it, null otherwise.
	 */	 
	private DestinationArc get_selected_destinationArc(Set<DestinationArc> dst_arcs, int x, int y) {
		Point2D point = new Point2D.Double(x, y);
		Iterator<DestinationArc> it = dst_arcs.iterator();
		while(it.hasNext()) {
			DestinationArc arc = it.next();
			Place p = arc.get_place();
			Transition t = arc.get_transition();
			Line2D line = new Line2D.Double(p.get_locationCoordinates().getxPoint()+_place_radius/2,p.get_locationCoordinates().getyPoint()+_place_radius/2,
											t.get_locationCoordinates().getxPoint()+_transition_width/2,t.get_locationCoordinates().getyPoint()+_transition_heigth/2);
			if (line.ptSegDist(point) < _arc_eps)
				return arc;
		}
		
		return null;
	}
	
	
	

	
	
	/**
	 * A mouse handler to detect when the mouse has been pressed
	 * or released, and perform the corresponding actions. 
	 */
	private class MouseHandler extends MouseAdapter {

		
        @Override
        public void mouseReleased(MouseEvent e) {
        	_msPressed = false;
        }

        @Override
        public void mousePressed(MouseEvent e) {
        	_msPressed = true;
            _msPoint = e.getPoint();
            _prev_msPoint = _msPoint;
            
            
            if(SwingUtilities.isRightMouseButton(e)) {
            	Place p = get_selected_place(PetriNetManager.initialize().getComponent().get_places(), e.getX(), e.getY());
            	Transition t = get_selected_transition(PetriNetManager.initialize().getComponent().get_transitions(), e.getX(), e.getY());
            	SourceArc src_arc = get_selected_sourceArc(SourceArcManager.getCurrentInstance().getComponents(), e.getX(), e.getY());
            	DestinationArc dst_arc = get_selected_destinationArc(DestinationArcManager.getCurrentInstance().getComponents(), e.getX(), e.getY());
            	PopMenu pop_menu =  new PopMenu();
            	if (p != null) {
            		_popup = pop_menu.pop_menu(p);
            	} else if (t != null) {
            		_popup = pop_menu.pop_menu(t);
            	} else if (src_arc != null ) {
            		_popup = pop_menu.pop_menu(src_arc);
            	} else if (dst_arc != null) {
            		_popup = pop_menu.pop_menu(dst_arc);
            	} else {
            		_popup = pop_menu.pop_menu();
            	}
            	
            	_popup.show(e.getComponent(), e.getX(), e.getY());
            }
            
            
            if (ToolbarManager.getCurrentInstance().get_component().get_toolbar_mode() == Toolbar.ADD_PLACE) {
            	ToolbarManager.getCurrentInstance().get_component().execute();
            	Place place1= (Place) ComponentManagerFactory.getComponent(ComponentType.PLACE); //Get the component
        		ComponentManagerFactory.updateComponentMetaData(place1,new LocationCoordinates(e.getX()-((int) _place_radius/2), e.getY()-((int) _place_radius/2)),new Tokens(0),null); //update the component //null is because place has nothing to attach any component
        		ComponentManagerFactory.addComponent(place1, 0); //add the component to the petriNet //null because place has no parent
            }
            
            if (ToolbarManager.getCurrentInstance().get_component().get_toolbar_mode() == Toolbar.ADD_TRANSITION) {
            	ToolbarManager.getCurrentInstance().get_component().execute();
            	Transition transition1= (Transition) ComponentManagerFactory.getComponent(ComponentType.TRANSITION);
        		ComponentManagerFactory.updateComponentMetaData(transition1, new LocationCoordinates(e.getX()-((int) _transition_width/2), e.getY()-((int) _transition_heigth/2)), null, null);
        		ComponentManagerFactory.addComponent(transition1, 0);
            }
            
            if (ToolbarManager.getCurrentInstance().get_component().get_toolbar_mode() == Toolbar.SELECT) {
            	Place p = get_selected_place(PetriNetManager.initialize().getComponent().get_places(), e.getX(), e.getY());
            	Transition t = get_selected_transition(PetriNetManager.initialize().getComponent().get_transitions(), e.getX(), e.getY());
            	SourceArc src_arc = get_selected_sourceArc(SourceArcManager.getCurrentInstance().getComponents(), e.getX(), e.getY());
            	DestinationArc dst_arc = get_selected_destinationArc(DestinationArcManager.getCurrentInstance().getComponents(), e.getX(), e.getY());
            	if (p != null && !_selected_places.contains(p)) {
            		_selected_places.add(p);
            		ToolbarManager.getCurrentInstance().get_component().add_connection_element(p);
            	} else if (t != null && !_selected_transitions.contains(t)) {
            		_selected_transitions.add(t);
            		ToolbarManager.getCurrentInstance().get_component().add_connection_element(t);
            	} else if (src_arc != null && !_selected_src_arcs.contains(src_arc)) {
            		_selected_src_arcs.add(src_arc);
            	} else if (dst_arc != null && !_selected_dst_arcs.contains(dst_arc)) {
            		_selected_dst_arcs.add(dst_arc);
            	} else {
            		clear_selected_elements();
            	}
            	
            }
            
            
            if (ToolbarManager.getCurrentInstance().get_component().get_toolbar_mode() == Toolbar.MARK) {
            	ToolbarManager.getCurrentInstance().get_component().execute();
            	Place p = get_selected_place(PetriNetManager.initialize().getComponent().get_places(), e.getX(), e.getY());
            	if (p != null) {
            		Tokens tok = (Tokens) p.get_tokens();
            		tok.setIntCount(tok.getIntCount()+1);
            		ComponentManagerFactory.updateComponentMetaData(p, p.get_locationCoordinates(), tok, null);
            	}
            }
            
            ToolbarManager.getCurrentInstance().get_component().updateToolbar();
            e.getComponent().repaint();
        }
    }
	
	
	/**
	 * A mouse handler to detect when the mouse is moving
	 * and perform the corresponding actions. 
	 */
	 private class MouseMotionHandler extends MouseMotionAdapter {

	        @Override
	        public void mouseDragged(MouseEvent e) {
	            if (_msPressed) {
	                
	                int delta_x = _prev_msPoint.x - e.getX();
	                int delta_y = _prev_msPoint.y - e.getY();
	                
	                Iterator<Place> it_places = _selected_places.iterator();
	                Iterator<Transition> it_trans = _selected_transitions.iterator();
	                
	                while(it_places.hasNext()) {
	                	Place p = it_places.next();
	                	int new_x = p.get_locationCoordinates().getxPoint() - delta_x;
	                	int new_y = p.get_locationCoordinates().getyPoint() - delta_y;
	                	ComponentManagerFactory.updateComponentMetaData(p, new LocationCoordinates(new_x, new_y), p.get_tokens(), null);
	                }
	                while(it_trans.hasNext()) {
	                	Transition t = it_trans.next();
	                	int new_x = t.get_locationCoordinates().getxPoint() -  delta_x;
	                	int new_y = t.get_locationCoordinates().getyPoint() -  delta_y;
	                	ComponentManagerFactory.updateComponentMetaData(t, new LocationCoordinates(new_x, new_y), null, null);
	                }
	                
	                _prev_msPoint.x = _msPoint.x -  delta_x;
	                _prev_msPoint.y = _msPoint.y -  delta_y;
	                
	            }
	            e.getComponent().repaint();
	        }
	    }
	 
	 
	 private void draw_arrow(int src_x, int src_y, int dst_x, int dst_y, Graphics g) {
			Point src = new Point(src_x, src_y);
			Point dst = new Point(dst_x, dst_y);
			
			Point2D.Double vec = new Point.Double(src.x-dst.x, src.y-dst.y);
			double h = Math.sqrt(vec.x*vec.x + vec.y*vec.y);
			vec.x = vec.x / h;
			vec.y = vec.y/ h;
			
			Point2D.Double int_p = new Point2D.Double(dst.x + vec.x*_arrow_size, dst.y + vec.y*_arrow_size);
			int_p.x = int_p.x - dst.x;
			int_p.y = int_p.y - dst.y;
			Point.Double tp1 = rotate_point(int_p, 1);
			Point.Double tp2 = rotate_point(int_p, -1);
			int[] x = {dst.x, (int) (tp1.x+dst.x), (int) (tp2.x+dst.x)};
			int[] y = {dst.y, (int) (tp1.y+dst.y), (int) (tp2.y+dst.y)};
			g.fillPolygon(x, y, 3);
		}
		
		private Point2D.Double rotate_point(Point2D.Double p, int direction) {
			int x = (int) (p.x*Math.cos(direction*_arrow_angle) + p.y*Math.sin(direction*_arrow_angle));
			int y = (int) (p.y*Math.cos(direction*_arrow_angle) - p.x*Math.sin(direction*_arrow_angle));
			return new Point2D.Double(x,y);
		}
		
		
	 
	 /**
		 * Returns a the set of selected places.
		 * 
		 * @return Set<Place> 
		 */
	 public Set<Place> get_selected_places() {
		 return this._selected_places;
	 }
	 
	 /**
		 * Returns a the set of selected transitions.
		 * 
		 * @return Set<Transition> 
		 */
	 public Set<Transition> get_selected_transitions() {
		 return this._selected_transitions;
	 }
	 
	 /**
		 * Returns a the set of selected source arcs.
		 * 
		 * @return Set<Arc> 
		 */
	 public Set<SourceArc> get_selected_src_arcs() {
		 return this._selected_src_arcs;
	 }
	 
	 /**
		 * Returns a the set of selected destination arcs.
		 * 
		 * @return Set<Arc> 
		 */
	 public Set<DestinationArc> get_selected_dst_arcs() {
		 return this._selected_dst_arcs;
	 }
	 
	 public void clear_selected_elements() {
		 this._selected_places.clear();
		 this._selected_transitions.clear();
		 this._selected_src_arcs.clear();
		 this._selected_dst_arcs.clear();
	 }
	 
}
