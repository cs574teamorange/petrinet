package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import logic.componentManager.PaintingCanvasManager;
import logic.componentManager.PlaceManager;
import logic.componentManager.ToolbarManager;
import logic.componentManager.TransitionManager;
import logic.model.DestinationArc;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Transition;

public class PopMenu{

	private JMenuItem pop_cut;
	private JMenuItem pop_copy;
	private JMenuItem pop_paste;
	
	public PopMenu() {
		pop_cut = new JMenuItem("Cut");
		pop_copy = new JMenuItem("Copy");
		pop_paste = new JMenuItem("Paste");
		
		pop_cut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ToolbarManager.getCurrentInstance().get_component().cut_elements();
			}
		});
		
		pop_copy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ToolbarManager.getCurrentInstance().get_component().copy_elements();
			}
		});
		
		pop_paste.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ToolbarManager.getCurrentInstance().get_component().paste_elements();
			}
		});
	}
	
	public JPopupMenu pop_menu(Place p) {
		JPopupMenu pup = new JPopupMenu();
		JMenuItem name = new JMenuItem("Rename...");
		JMenuItem tokens = new JMenuItem("Tokens...");
		
		name.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {	
				String s = (String) JOptionPane.showInputDialog(
	                    (JComponent) e.getSource(), "Rename\n", "Rename", 
	                    JOptionPane.PLAIN_MESSAGE, null, null, p.getPlaceName());
				if (s == null)
					return;
				if (!s.equals(""))
					if (!PlaceManager.getCurrentInstance().isNameUsed(s))
						p.setPlaceName(s);
					else 
						JOptionPane.showMessageDialog((JComponent) e.getSource(), "The new name is already used!");
				else
					JOptionPane.showMessageDialog((JComponent) e.getSource(), "The new name is null!");
				PaintingCanvasManager.getCurrentInstance().get_component().repaint();
			}
		});
		
		tokens.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String s = (String) JOptionPane.showInputDialog(
	                    (JComponent) e.getSource(), "Tokens\n", "Tokens", 
	                    JOptionPane.PLAIN_MESSAGE, null, null, p.get_tokens().getIntCount());
				if (s == null)
					return;
				if (!s.equals(""))
					p.get_tokens().setIntCount(Integer.parseInt(s));
				else
					JOptionPane.showMessageDialog((JComponent) e.getSource(), "The new tokens are null!");
				PaintingCanvasManager.getCurrentInstance().get_component().repaint();
			}
		});
		
		pup.add(pop_cut);
		pup.add(pop_copy);
		pup.add(pop_paste);
		pup.addSeparator();
		pup.add(name);
		pup.add(tokens);
		return pup;
	}
	
	public JPopupMenu pop_menu(Transition t) {
		JPopupMenu pup = new JPopupMenu();
		JMenuItem name = new JMenuItem("Rename...");
		
		name.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {	
				String s = (String) JOptionPane.showInputDialog(
	                    (JComponent) e.getSource(), "Rename\n", "Rename", 
	                    JOptionPane.PLAIN_MESSAGE, null, null, t.getTransitionName());
				if (s == null)
					return;
				if (!s.equals(""))
					if (!TransitionManager.getCurrentInstance().isNameUsed(s))
						t.setTransitionName(s);
					else 
						JOptionPane.showMessageDialog((JComponent) e.getSource(), "The new name is already used!");
				else
					JOptionPane.showMessageDialog((JComponent) e.getSource(), "The new name is null!");
				PaintingCanvasManager.getCurrentInstance().get_component().repaint();
			}
		});
		
		pup.add(pop_cut);
		pup.add(pop_copy);
		pup.add(pop_paste);
		pup.addSeparator();
		pup.add(name);
		return pup;
	}
	
	public JPopupMenu pop_menu(SourceArc arc) {
		JPopupMenu pup = new JPopupMenu();
		JMenuItem weight = new JMenuItem("Weight...");
		
		weight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {	
				String s = (String) JOptionPane.showInputDialog(
	                    (JComponent) e.getSource(), "Weight\n", "Weight", 
	                    JOptionPane.PLAIN_MESSAGE, null, null, arc.get_arcValue());
				if (s == null)
					return;
				if (!s.equals(""))
					arc.get_arcValueObj().setIntCount(Integer.parseInt(s));
				else
					JOptionPane.showMessageDialog((JComponent) e.getSource(), "The weight is null!");
				PaintingCanvasManager.getCurrentInstance().get_component().repaint();
			}
		});
		
		pup.add(pop_cut);
		pup.add(pop_copy);
		pup.add(pop_paste);
		pup.addSeparator();
		pup.add(weight);
		return pup;
	}
	
	public JPopupMenu pop_menu(DestinationArc arc) {
		JPopupMenu pup = new JPopupMenu();
		JMenuItem weight = new JMenuItem("Weight...");
		
		weight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {	
				String s = (String) JOptionPane.showInputDialog(
	                    (JComponent) e.getSource(), "Weight\n", "Weight", 
	                    JOptionPane.PLAIN_MESSAGE, null, null, arc.get_arcValue());
				if (s == null)
					return;
				if (!s.equals(""))
					arc.get_arcValueObj().setIntCount(Integer.parseInt(s));
				else
					JOptionPane.showMessageDialog((JComponent) e.getSource(), "The weight is null!");
				PaintingCanvasManager.getCurrentInstance().get_component().repaint();
			}
		});
		
		pup.add(pop_cut);
		pup.add(pop_copy);
		pup.add(pop_paste);
		pup.addSeparator();
		pup.add(weight);
		return pup;
	}
	
	public JPopupMenu pop_menu() {
		JPopupMenu pup = new JPopupMenu();
		pup.add(pop_cut);
		pup.add(pop_copy);
		pup.add(pop_paste);
		return pup;
	}
	
}
