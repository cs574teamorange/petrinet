package ui;

import java.awt.Dimension;
import java.io.IOException;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import io.PetriSerializerException;
import logic.componentManager.PetriNetIO;
import logic.componentManager.PetriNetManager;
import logic.model.Firable;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.utilities.CoverabilityTree;
import logic.utilities.CoverabilityTreeManager;
import logic.utilities.DefaultArcManager;
import logic.utilities.DefaultMarkingStateManager;
import logic.utilities.DefaultTransitionFiringManager;
import logic.utilities.MarkingStateManager;
import logic.utilities.TransitionFiringManager;


public class DisplayCoverabilityTree extends JFrame {
	
	public DisplayCoverabilityTree() throws ClassNotFoundException, IOException, ParserConfigurationException, PetriSerializerException, SAXException
	{
		//PetriNet petriNet=PetriNetIO.readPetriNet("./src/test/resources/HW1PN1.xml");
		PetriNet petriNet = PetriNetManager.initialize().getComponent();
		TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
		MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
		CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
		int[] currentMarkingState=markingStateManager.getcurrentStateofPetriNet(petriNet.get_places());
		PetriNetState initialState= new PetriNetState(1,currentMarkingState,MarkingState.NEW,null,null);
		petriNet.set_petriNetState(initialState);		
		
		Set<PetriNetState> petriNetStates=coverabilityTree.generateConverabilityTree(petriNet);
		CoverabilityTreeManager coverabilityTreeManager= new CoverabilityTreeManager(petriNetStates);
          JTree tree=coverabilityTreeManager.getDisplayableCoverabilitytree();
        add(tree);
        setPreferredSize(new Dimension(400, 400));	
        pack();
        setVisible(true);
	}
	
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					new DisplayCoverabilityTree();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (PetriSerializerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SAXException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

}
