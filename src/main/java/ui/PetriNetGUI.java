package ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import io.PetriSerializerException;
import logic.componentManager.PaintingCanvasManager;
import logic.componentManager.PetriNetIO;
import logic.componentManager.PetriNetManager;
import logic.componentManager.ToolbarManager;
import logic.model.PetriNet;

/**
 * Created by edavis on 9/18/17.
 */
public class PetriNetGUI extends JFrame {
	
	private Menu _menu;
	private PetriNetGUI _gui;
	private String _wd_filepath = "./wd.xml";
	
	public static void main(String[] args) {
		try {
			PetriNet petriNet=PetriNetIO.readPetriNet("./wd.xml");//provide complete path of the petri net xml file
			PetriNetIO.initializePetriNet(petriNet);// initialize the petriNet to PetriNetManager
				
				// do your UI stuff
			} catch (ClassNotFoundException e) { 
				//handle this exception
			
			} catch (IOException e) {
				//handle this exception
			} catch (ParserConfigurationException e) {
				//handle this exception
			} catch (PetriSerializerException e) {
				//handle this exception
			} catch (SAXException e) {
				//handle this exception
			} 
		
		PetriNetGUI p = new PetriNetGUI();
	}
	
	
	public PetriNetGUI() {
		
		ToolbarManager toolbar_manager = ToolbarManager.getCurrentInstance();
	
		
		
		this.setTitle("PetriNet");
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowListener() {
			@Override
			public void windowOpened(java.awt.event.WindowEvent e) {}
			@Override
			public void windowIconified(java.awt.event.WindowEvent e) {	}
			@Override
			public void windowDeiconified(java.awt.event.WindowEvent e) {}
			@Override
			public void windowDeactivated(java.awt.event.WindowEvent e) {}
			@Override
			public void windowClosing(java.awt.event.WindowEvent e) {
				int option = JOptionPane.showConfirmDialog(_menu, "Do you want to save the workspace?");
				
				switch(option) {
				case JOptionPane.YES_OPTION: // Clear the petrinet
					// Save to _actual_filename_path
					try {
						PetriNetIO.writePetriNet(_wd_filepath, PetriNetManager.initialize().getComponent());
					} 
					catch (IOException exception) {System.out.println(exception.getMessage());} 
					catch (ParserConfigurationException exception) {System.out.println(exception.getMessage());} 
					catch (PetriSerializerException exception) {System.out.println(exception.getMessage());} 
					catch (TransformerException exception) {System.out.println(exception.getMessage());} 
				
					System.exit(0);
				case JOptionPane.NO_OPTION:
					System.exit(0);
				}
				
			}
			@Override
			public void windowClosed(java.awt.event.WindowEvent e) {
				
			}
			@Override
			public void windowActivated(java.awt.event.WindowEvent e) {}
		});
		
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
		this.setMinimumSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height));
		
		Menu menu = new Menu();
		PaintingCanvasManager painting_canvas_manager = PaintingCanvasManager.getCurrentInstance();
		
		menu.create_menu();
		
		this._menu = menu;
		
		this.setJMenuBar(menu);
		this.add(toolbar_manager.get_component(), BorderLayout.WEST);
		this.add(painting_canvas_manager.get_component());
		
		
		
		this.pack();
		this.setVisible(true);
		
	}
}
