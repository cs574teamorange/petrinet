package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import io.PetriSerializerException;
import logic.componentManager.PetriNetIO;
import logic.componentManager.PetriNetManager;
import logic.componentManager.PlaceManager;
import logic.componentManager.ToolbarManager;
import logic.model.Firable;
import logic.model.MarkingState;
import logic.model.PetriNet;
import logic.model.PetriNetState;
import logic.utilities.CoverabilityTree;
import logic.utilities.DefaultArcManager;
import logic.utilities.DefaultMarkingStateManager;
import logic.utilities.DefaultTransitionFiringManager;
import logic.utilities.MarkingStateManager;
import logic.utilities.PetriNetInferencer;
import logic.utilities.TransitionFiringManager;
import logic.utilities.exception.InvalidPetriNetStateException;

public class Menu extends JMenuBar {
	
	private PetriNet petrinet;
	private String _actual_filename_path;
	
	
	public Menu() {
		
	}
	
	
	/**
	 * This method creates the Menu, generating the options inside of it and assigns the 
	 * corresponding actions when the options are selected.
	 */
	public void create_menu() {
		JMenu menu_file = new JMenu("File..");
		JMenuItem menu_new = new JMenuItem("New");
		JMenuItem menu_open = new JMenuItem("Open..");
		JMenuItem menu_saveas = new JMenuItem("Save as..");
		menu_new.addActionListener(menu_new_listener);
		menu_open.addActionListener(menu_open_listener);
		menu_saveas.addActionListener(menu_saveas_listener);
		IconAdder.add(menu_new);
		IconAdder.add(menu_open);
		IconAdder.add(menu_saveas);
		menu_file.add(menu_new);
		menu_file.add(menu_open);
		menu_file.add(menu_saveas);


		JMenu menu_tools = new JMenu ("Tools");
		JMenuItem menu_coverability = new JMenuItem("Coverability");
		JMenuItem menu_isBounded = new JMenuItem("Is bounded?");
		JMenuItem menu_reachableMarking = new JMenuItem("Reachable marking..");
		IconAdder.add(menu_coverability);
		IconAdder.add(menu_isBounded);
		IconAdder.add(menu_reachableMarking);
		menu_reachableMarking.addActionListener(menu_reachableMarking_listener);
		menu_isBounded.addActionListener(menu_isBounded_listener);
		menu_coverability.addActionListener(menu_coverability_listener);
		menu_tools.add(menu_coverability);
		menu_tools.add(menu_isBounded);
		menu_tools.add(menu_reachableMarking);
		

		
		

		
		this.add(menu_file);
		this.add(menu_tools);
	}

	
	/**
	 * This ActionListener is assigned to the menu button "new" and clears the
	 * petrinet components to give as a result a blank petrinet in the GUI.
	 */
	private ActionListener menu_new_listener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			int option = JOptionPane.showConfirmDialog(getParent().getParent(), "Do you want to create a new petrinet?");
			
			switch(option) {
			case JOptionPane.YES_OPTION: // Clear the petrinet
				PetriNetManager petrinet_manager = PetriNetManager.initialize();
				petrinet_manager.clear();
			case JOptionPane.NO_OPTION:
				break;
			case JOptionPane.CANCEL_OPTION:
				break;
			}
			
			// Repaint GUI
			ToolbarManager.getCurrentInstance().get_component().updateToolbar();
			getParent().getParent().repaint();
		}
	};
	
	/**
	 * This ActionListener is assigned to the menu button "open" to open an already
	 * saved petrinet in an XML format. As a result, the petrinet is loaded and displayed
	 * in the GUI.
	 */
	private ActionListener menu_open_listener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			// Open file browser
			JFileChooser browser = new JFileChooser();
			browser.setCurrentDirectory(new File(System.getProperty("user.dir")));
			int result = browser.showOpenDialog(getParent().getParent());
			
			if (result == JFileChooser.APPROVE_OPTION) {
				// Read input file
				File selectedFile = browser.getSelectedFile();
				String filename = selectedFile.getAbsolutePath();
				_actual_filename_path = filename;
				try {
					PetriNet petriNet=PetriNetIO.readPetriNet(_actual_filename_path);//provide complete path of the petri net xml file
					PetriNetIO.initializePetriNet(petriNet);// initialize the petriNet to PetriNetManager
				} 
				catch(ClassNotFoundException exception) {} 
				catch (IOException exception) {} 
				catch (ParserConfigurationException exception) {} 
				catch (PetriSerializerException exception) {} 
				catch (SAXException exception) {} 
			}
			
			//Repaint GUI
			ToolbarManager.getCurrentInstance().get_component().updateToolbar();
			getParent().getParent().repaint();
		}
	};
	
	/**
	 * Thise ActionListener is assigned to the menu button "save" and it saves 
	 * the current loaded petrinet into a XML file.
	 */
	private ActionListener menu_save_listener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			// Save to _actual_filename_path
			try {
				if (_actual_filename_path != null)
					PetriNetIO.writePetriNet(_actual_filename_path, PetriNetManager.initialize().getComponent());
			} 
			catch (IOException exception) {System.out.println(exception.getMessage());} 
			catch (ParserConfigurationException exception) {System.out.println(exception.getMessage());} 
			catch (PetriSerializerException exception) {System.out.println(exception.getMessage());} 
			catch (TransformerException exception) {System.out.println(exception.getMessage());} 
		}
	};
	
	private ActionListener menu_saveas_listener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			// Open file browser
			JFileChooser browser = new JFileChooser();
			browser.setCurrentDirectory(new File(System.getProperty("user.dir/nets")));
			int result = browser.showOpenDialog(getParent().getParent());
			
			if (result == JFileChooser.APPROVE_OPTION) {
				// Save input file
				File selectedFile = browser.getSelectedFile();
				String filename = selectedFile.getAbsolutePath();
				_actual_filename_path = filename;
				try {
					PetriNetIO.writePetriNet(_actual_filename_path, PetriNetManager.initialize().getComponent());
				} 
				catch (IOException exception) {System.out.println(exception.getMessage());} 
				catch (ParserConfigurationException exception) {System.out.println(exception.getMessage());} 
				catch (PetriSerializerException exception) {System.out.println(exception.getMessage());} 
				catch (TransformerException exception) {System.out.println(exception.getMessage());} 
			
			}
		}
	};
	
	private ActionListener menu_coverability_listener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
	
			try {
				DisplayCoverabilityTree cov_tree = new DisplayCoverabilityTree();
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (ParserConfigurationException e1) {
				e1.printStackTrace();
			} catch (PetriSerializerException e1) {
				e1.printStackTrace();
			} catch (SAXException e1) {
				e1.printStackTrace();
			}
		
		}
	};
	
	private ActionListener menu_isBounded_listener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			PetriNet petrinet = PetriNetManager.initialize().getComponent();
			TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
			MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
			CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
			
			PetriNetState initialState= new PetriNetState(1,markingStateManager.getcurrentStateofPetriNet(petrinet.get_places()),MarkingState.NEW,null,null);
			petrinet.set_petriNetState(initialState);
			
			Set<PetriNetState> states = coverabilityTree.generateConverabilityTree(petrinet);
			PetriNetInferencer petriNetInferencer=new PetriNetInferencer(states,petrinet.get_transitions().size());
			try {
				if (petriNetInferencer.isPetriNetBounded()){
					JOptionPane.showMessageDialog(null, "The PetriNet is bounded.");
				} else {
					JOptionPane.showMessageDialog(null, "The PetriNet is not bounded!!");
				}
			} catch (InvalidPetriNetStateException e1) {
				e1.printStackTrace();
			}
			
		}
	};
	
	private ActionListener menu_reachableMarking_listener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			String s = (String) JOptionPane.showInputDialog(
                    (JComponent) e.getSource(), "Insert the marking state to check for reachability:\n"
                    		+ "e.g. 1,0,0,1", "Reachability", 
                    JOptionPane.PLAIN_MESSAGE, null, null, "");
			
			String[] s_tokens = s.split(",");
			int[] state = new int[s_tokens.length];
			
			for (int i=0; i<s_tokens.length; i++) {
				state[i] = Integer.parseInt(s_tokens[i]);
			}
			
			PetriNet petrinet = PetriNetManager.initialize().getComponent();
			TransitionFiringManager<Firable> transitionFiringManager= new DefaultTransitionFiringManager(new DefaultArcManager()); 
			MarkingStateManager markingStateManager= new DefaultMarkingStateManager();
			CoverabilityTree coverabilityTree= new CoverabilityTree(markingStateManager, transitionFiringManager);
			
			PetriNetState initialState= new PetriNetState(1,markingStateManager.getcurrentStateofPetriNet(petrinet.get_places()),MarkingState.NEW,null,null);
			petrinet.set_petriNetState(initialState);
			
			Set<PetriNetState> states = coverabilityTree.generateConverabilityTree(petrinet);
			PetriNetInferencer petriNetInferencer=new PetriNetInferencer(states,petrinet.get_transitions().size());
			
			if (petriNetInferencer.isMarkingStateReachable(state)) {
				JOptionPane.showMessageDialog(null, "The input state is reachable.");
			} else {
				JOptionPane.showMessageDialog(null, "The input state is not reachable!");
			}
			
		}
	};
}
