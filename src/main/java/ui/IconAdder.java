package ui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by edavis on 12/2/17.
 */
public class IconAdder {
    public static void add(AbstractButton button) {
        String[] words = button.getText().split(" ");
        String lastWord = words[words.length - 1].replace("..", "").replace("?", "");
        String imgName = lastWord.toLowerCase();
        String imgPath = "./src/main/resources/" + imgName + ".png";

        try {
            Image img = ImageIO.read(new File(imgPath));
            button.setIcon(new ImageIcon(img));
        } catch (IOException ioe) {
            System.out.println("ERROR: Could not read image file '" + imgPath + "'.");
        }
    }
}
