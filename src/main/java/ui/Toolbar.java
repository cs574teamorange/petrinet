package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

import logic.componentManager.ComponentManagerFactory;
import logic.componentManager.ComponentType;
import logic.componentManager.DestinationArcManager;
import logic.componentManager.PaintingCanvasManager;
import logic.componentManager.PetriNetManager;
import logic.componentManager.SourceArcManager;
import logic.model.AbstractComponent;
import logic.model.ArcValue;
import logic.model.DestinationArc;
import logic.model.Firable;
import logic.model.PetriNet;
import logic.model.Place;
import logic.model.SourceArc;
import logic.model.Transition;
import logic.utilities.DefaultArcManager;
import logic.utilities.DefaultTransitionFiringManager;
import logic.utilities.TransitionFiringManager;

public class Toolbar extends JToolBar{

	private int mode;
	
	public static final int SELECT = 1;
	public static final int CONNECT = 2;
	public static final int ADD_PLACE = 3;
	public static final int ADD_TRANSITION = 4; 
	public static final int MARK = 5;
	
	private Set<Place> _copy_places;
	private Set<Transition>  _copy_transitions;
	
	private Stack<PetriNet> _stack_undo;
	private Stack<PetriNet> _stack_redo;
	
	private Vector<Transition> _firable_transitions = new Vector<Transition>();
	private JComboBox<String> _combo_box_firable_transitions;
	
	private Vector<AbstractComponent> _connection_order = new Vector<AbstractComponent>();
	
	private PetriNet _actual_petrinet;
	
	
	public Toolbar() {
		this.mode = Toolbar.SELECT;
		
		this._stack_undo = new Stack<PetriNet>();
		this._stack_redo = new Stack<PetriNet>();
		
		this._copy_places = new TreeSet<Place>();
		this._copy_transitions = new TreeSet<Transition>();
		
		this._actual_petrinet = null;
		
	}
	
	/**
	 * This method creates the toolbar generating the buttons of it and assigns the 
	 * corresponding actions when the buttons are pressed.
	 */
	public void create_toolbar() {
		JButton button_cut = new JButton("Cut");
		JButton button_copy = new JButton("Copy");
		JButton button_paste = new JButton("Paste");
		
		JButton button_undo = new JButton("Undo");
		JButton button_redo = new JButton("Redo");
		
		JButton button_select = new JButton("Select");
		JButton button_connect = new JButton("Connect");
		JButton button_add_place = new JButton("Add place");
		JButton button_add_transition = new JButton("Add transition");
		
		JButton button_mark = new JButton("Mark");
		
		JButton button_fire = new JButton("Fire");
		button_fire.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				execute();
		        int index = _combo_box_firable_transitions.getSelectedIndex();
		        if (index < 0) return;
		        TransitionFiringManager<Firable> tr= new DefaultTransitionFiringManager(new DefaultArcManager());
		        tr.fireTransition(_firable_transitions.get(index));
		        _combo_box_firable_transitions.removeAllItems();
		        _firable_transitions.clear();
		        fill_comboBox(_combo_box_firable_transitions);
		        _combo_box_firable_transitions.getParent().getParent().repaint();
			}
		});
		
		_combo_box_firable_transitions = new JComboBox<String>();
		_combo_box_firable_transitions.setMaximumSize(button_connect.getMaximumSize());
		fill_comboBox(_combo_box_firable_transitions);
		
		
		button_cut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cut_elements();
				updateToolbar();
			}
		});
		
		button_copy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				copy_elements();
				updateToolbar();
			}
		});
		
		button_paste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				paste_elements();
				updateToolbar();
			}
		});
		
		
		button_undo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				undo();
				updateToolbar();
			}
		});
		
		
		button_redo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				redo();
				updateToolbar();
			}
		});
		
		
		
		button_select.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PaintingCanvasManager.getCurrentInstance().get_component().clear_selected_elements();
				_connection_order.clear();
				change_toolbar_mode(SELECT);
				PaintingCanvasManager.getCurrentInstance().get_component().repaint();
			}
		});
		
		button_connect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connect_elements();
				clear_connection_list();
				PaintingCanvasManager.getCurrentInstance().get_component().get_selected_places().clear();
				PaintingCanvasManager.getCurrentInstance().get_component().get_selected_transitions().clear();
				updateToolbar();
			}
		});
		
		button_add_place.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				change_toolbar_mode(ADD_PLACE);
			}
		});
		
		button_add_transition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				change_toolbar_mode(ADD_TRANSITION);
			}
		});
		
		button_mark.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				change_toolbar_mode(MARK);
			}
		});

		JButton[] buttons = new JButton[] {button_cut, button_copy, button_paste, button_undo, button_redo,
				button_select, button_connect, button_add_place, button_add_transition, button_mark, button_fire};
		for (int i = 0; i < buttons.length; i++) {
			IconAdder.add(buttons[i]);
		}

		this.add(button_cut);
		this.add(button_copy);
		this.add(button_paste);
		
		this.addSeparator();
		
		this.add(button_undo);
		this.add(button_redo);
		
		this.addSeparator();
		
		this.add(button_select);
		this.add(button_connect);
		this.add(button_add_place);
		this.add(button_add_transition); 
		
		this.addSeparator();
		
		this.add(button_mark);
		
		this.addSeparator();
		
		this.add(button_fire);
		this.add(_combo_box_firable_transitions);
		
		
		this.setRollover(false);
		
		this.setVisible(true);
	}

	/**
	 * Stores the selected elements in the canvas to be ready to copy them.
	 */
	public void copy_elements() {
		execute();
		Set<Place> tmp_places = PaintingCanvasManager.getCurrentInstance().get_component().get_selected_places();
		Set<Transition> tmp_transitions = PaintingCanvasManager.getCurrentInstance().get_component().get_selected_transitions();
		Iterator<Place> it_place = tmp_places.iterator();
		Iterator<Transition> it_transition = tmp_transitions.iterator();
		this._copy_places.clear();
		this._copy_transitions.clear();
		while (it_place.hasNext()) 
			this._copy_places.add(it_place.next());
		while (it_transition.hasNext())
			this._copy_transitions.add(it_transition.next());
	}
	
	/**
	 * Pastes all the elements selected for copying and creates new components
	 * in the petrinet.
	 */
	public void paste_elements() {
		execute();
		Iterator<Place> it_place = this._copy_places.iterator(); 
		Iterator<Transition> it_transition = this._copy_transitions.iterator();
		
		
		while (it_place.hasNext()) {
			Place p = (Place) ComponentManagerFactory.copyComponent(it_place.next());
			ComponentManagerFactory.addComponent(p, 0);
		}
		while (it_transition.hasNext()) {
			Transition t = (Transition) ComponentManagerFactory.copyComponent(it_transition.next());
			ComponentManagerFactory.addComponent(t, 0);
		}
		PaintingCanvasManager.getCurrentInstance().get_component().clear_selected_elements();
		getParent().repaint();
	}
	
	
	/**
	 * Stores the selected elements in the canvas to be ready to copy them and
	 * removes them from the petrinet.
	 */
	public void cut_elements() {
		copy_elements();
		
		Iterator<Place> it_place = this._copy_places.iterator(); 
		Iterator<Transition> it_transition = this._copy_transitions.iterator();
		
		while (it_place.hasNext()) 
			ComponentManagerFactory.removeComponent(it_place.next(), 0); 
		while (it_transition.hasNext())
			ComponentManagerFactory.removeComponent(it_transition.next(), 0);
		
		PaintingCanvasManager.getCurrentInstance().get_component().clear_selected_elements();
		getParent().repaint();
	}
	
	
	public void execute() {
		PetriNet p = PetriNetManager.initialize().getComponent();
		PetriNet p_clone = PetriNetManager.initialize().clone(p);
		this._stack_undo.push(p_clone);
		this._actual_petrinet = p_clone;
	}
	
	public void undo() {
		
		if (!this._stack_undo.isEmpty()) {
			PaintingCanvasManager.getCurrentInstance().get_component().clear_selected_elements();
			PetriNet p = this._stack_undo.pop();
			this._stack_redo.push(PetriNetManager.initialize().clone(this._actual_petrinet));
			this._actual_petrinet = PetriNetManager.initialize().clone(p);
			PetriNetManager.initialize().setComponent(this._actual_petrinet);
			PaintingCanvasManager.getCurrentInstance().get_component().repaint();
		}
		System.out.println("Undo: " + this._stack_undo.size() + " || Redo: " + this._stack_redo.size());
	}
	
	
	public void redo() {
		
		if(!this._stack_redo.isEmpty()) {
			PaintingCanvasManager.getCurrentInstance().get_component().clear_selected_elements();
			PetriNet p = this._stack_redo.pop();
			this._stack_undo.push(PetriNetManager.initialize().clone(p));
			PetriNetManager.initialize().setComponent(p);
			PaintingCanvasManager.getCurrentInstance().get_component().repaint();
		}
		System.out.println("Undo: " + this._stack_undo.size() + " || Redo: " + this._stack_redo.size());
	}
	
	
	public void updateToolbar() {
		_combo_box_firable_transitions.removeAllItems();
		fill_comboBox(this._combo_box_firable_transitions);
	}
	
	
	/**
	 * Adds an AbstractComponent to the component list to connect the elements in order.
	 * 
	 * @param comp
	 */
	public void add_connection_element(AbstractComponent comp) {
		_connection_order.addElement(comp);
	}
	
	public void clear_connection_list() {
		_connection_order.clear();
	}
	
	public void connect_elements() {
		execute();
		for (int i=0; i<_connection_order.size()-1; i++) {
			connect(_connection_order.get(i), _connection_order.get(i+1));
		}
	}
	
	private void connect(AbstractComponent c1, AbstractComponent c2) {
		
		if (c1.getComponentType() == c2.getComponentType()) {
			JOptionPane.showMessageDialog(this.getParent(), "Connection of two elements with the same type not allowed!");
			return;
		}
		
		if (c1.getComponentType() == ComponentType.PLACE) {
			SourceArc sourceArc= (SourceArc)ComponentManagerFactory.getComponent(ComponentType.SOURCE_ARROW);
			ComponentManagerFactory.updateComponentMetaData(sourceArc, c1.get_locationCoordinates(), new ArcValue(1),  c1);
			if (!SourceArcManager.getCurrentInstance().isComponentDuplicate((Place) c1, (Transition) c2))
				ComponentManagerFactory.addComponent(sourceArc, ((Transition) c2).get_id());
			else
				JOptionPane.showMessageDialog(this.getParent(), "Duplicated arc!");
		}
		if (c1.getComponentType() == ComponentType.TRANSITION) {
			DestinationArc destinationArc= (DestinationArc) ComponentManagerFactory.getComponent(ComponentType.DESTINATION_ARROW);
			ComponentManagerFactory.updateComponentMetaData(destinationArc, destinationArc.get_locationCoordinates(), new ArcValue(1), c2);
			if (!DestinationArcManager.getCurrentInstance().isComponentDuplicate((Place) c2, (Transition) c1))
				ComponentManagerFactory.addComponent(destinationArc, c1.get_id());
			else
				JOptionPane.showMessageDialog(this.getParent(), "Duplicated arc!");
			
		}
		this.getParent().repaint();
	}
	
	private void fill_comboBox(JComboBox<String> cb) {
		Iterator<Firable> it_trans = PetriNetManager.initialize().getComponent().get_transitions().iterator();
		while (it_trans.hasNext()) {
			Transition t = (Transition) it_trans.next();
			if (t.isFirable()) {
				cb.addItem(t.getTransitionName());
				_firable_transitions.add(t);
			}
		}
	}
	
	/**
	 * This method changes the toolbar mode to the mode defined by @param m .
	 * 
	 * @param m an integer value from the set {SELECT, CONNECT, ADD_PLACE, ADD_TRANSITION}.
	 */
	private void change_toolbar_mode(int m) {
		this.mode = m;
	}
	
	/**
	 * This method returns the toolbar mode.
	 * 
	 * @return an integer value from the set {SELECT, CONNECT, ADD_PLACE, ADD_TRANSITION}.
	 */
	public int get_toolbar_mode() {
		return this.mode;
	}
}
