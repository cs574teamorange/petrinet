all:
	mvn install

clean:
	mvn clean; rm -f *.s output.log 

skip:
	mvn package -DskipTests

